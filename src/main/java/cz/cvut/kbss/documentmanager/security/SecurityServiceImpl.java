package cz.cvut.kbss.documentmanager.security;

import cz.cvut.kbss.documentmanager.exception.SecurityClientErrorException;
import cz.cvut.kbss.documentmanager.exception.SecurityServerErrorException;
import cz.cvut.kbss.documentmanager.model.security.AuthenticationToken;
import cz.cvut.kbss.documentmanager.model.security.UserAccount;
import cz.cvut.kbss.documentmanager.model.security.DocManUserDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

@Service
public class SecurityServiceImpl implements SecurityService {
    
    private final RestTemplate restTemplate;

    @Value("${security.authorization-point}")
    private String authorizationPoint;

    public SecurityServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public boolean isTokenValid(HttpServletRequest request) throws SecurityClientErrorException {
        String authHeader = request.getHeader("authorization");
        HttpHeaders headers = new HttpHeaders();
        headers.set("authorization", authHeader);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        UserAccount userAccount;
        try {
            userAccount = restTemplate.exchange(authorizationPoint, HttpMethod.GET, entity, UserAccount.class).getBody();
        } catch (HttpClientErrorException clientException) {
            throw new SecurityClientErrorException("You are not authorized to do this operation. \n" + clientException.toString());
        } catch (HttpServerErrorException serverException) {
            throw new SecurityServerErrorException("Something went wrong on the KBSS server. \n" + serverException.toString());
        }
        
        DocManUserDetails details = new DocManUserDetails(userAccount);
        setCurrentUser(details);        
        return true;
    }

    public AuthenticationToken setCurrentUser(DocManUserDetails userDetails) {
        final AuthenticationToken token = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        token.setAuthenticated(true);

        final SecurityContext context = new SecurityContextImpl();
        context.setAuthentication(token);
        SecurityContextHolder.setContext(context);
        return token;
    }
    
    public static DocManUserDetails getCurrentUserDetails() {
        return (DocManUserDetails)SecurityContextHolder.getContext().getAuthentication().getDetails();        
    }
    
}

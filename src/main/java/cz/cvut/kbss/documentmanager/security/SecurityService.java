package cz.cvut.kbss.documentmanager.security;

import javax.servlet.http.HttpServletRequest;

public interface SecurityService {
    public boolean isTokenValid(HttpServletRequest request);
}

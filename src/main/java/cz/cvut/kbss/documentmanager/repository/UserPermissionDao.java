package cz.cvut.kbss.documentmanager.repository;

import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import java.util.List;
import java.util.Optional;

public interface UserPermissionDao {

    public Optional<UserPermission> findById(String id);
    
    public boolean existsById(String id);

    public List<UserPermission> findAll();

    public UserPermission save(UserPermission permission);

    public void deleteById(String id);
    
}

package cz.cvut.kbss.documentmanager.repository.jpa;

import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.repository.FolderDao;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface FolderRepository extends JpaRepository<Folder, String>, FolderDao {
    
}

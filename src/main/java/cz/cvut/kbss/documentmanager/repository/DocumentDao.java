package cz.cvut.kbss.documentmanager.repository;

import cz.cvut.kbss.documentmanager.model.Document;
import java.util.List;
import java.util.Optional;

public interface DocumentDao {

    public Optional<Document> findById(String id);
    
    public boolean existsById(String id);

    public List<Document> findAll();

    public Document save(Document document);

    public void deleteById(String id);
}

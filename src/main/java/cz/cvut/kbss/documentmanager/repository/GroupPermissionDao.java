package cz.cvut.kbss.documentmanager.repository;

import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import java.util.List;
import java.util.Optional;

public interface GroupPermissionDao {

    public Optional<GroupPermission> findById(String id);
    
    public boolean existsById(String id);

    public List<GroupPermission> findAll();

    public GroupPermission save(GroupPermission permission);

    public void deleteById(String id);
    
}

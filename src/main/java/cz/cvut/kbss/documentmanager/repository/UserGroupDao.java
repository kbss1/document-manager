package cz.cvut.kbss.documentmanager.repository;

import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import java.util.List;
import java.util.Optional;

public interface UserGroupDao {

    public Optional<UserGroup> findById(String id);
    
    public boolean existsById(String id);

    public List<UserGroup> findAll();

    public UserGroup save(UserGroup group);

    public void deleteById(String id);
}

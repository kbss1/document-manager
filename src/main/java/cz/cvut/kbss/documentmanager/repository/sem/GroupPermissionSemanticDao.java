package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.jopa.model.EntityManager;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.exception.PersistenceException;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.repository.GroupPermissionDao;

import cz.cvut.kbss.jopa.exceptions.NoResultException;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile("sem")
public class GroupPermissionSemanticDao implements GroupPermissionDao {

    @Autowired
    EntityManager em;

    public GroupPermissionSemanticDao() {
    }
    
    @Override
    public List<GroupPermission> findAll() {
        try {
            return em.createNativeQuery("SELECT ?x WHERE { ?x a ?type . }", GroupPermission.class)
                .setParameter("type", URI.create(Vocabulary.c_GroupPermission)).getResultList();
        } catch (Exception e)  {
            throw new PersistenceException("Could not find all permissions.", e);
        }
    }

    @Override
    public boolean existsById(String uri) {
        if (uri == null) {
            return false;
        }
        return em.createNativeQuery("ASK { ?individual a ?type . }", Boolean.class)
            .setParameter("individual", URI.create(uri))
            .setParameter("type", URI.create(Vocabulary.c_GroupPermission)).getSingleResult();
    }

    @Override
    public Optional<GroupPermission> findById(String uri) {
        try {
            GroupPermission ret = em.find(GroupPermission.class, URI.create(uri));
            if (ret == null) {
                return Optional.empty();
            }
            return Optional.of(ret);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }


    @Override
    public GroupPermission save(GroupPermission entity) {
        Objects.requireNonNull(entity);
        try {
            return em.merge(entity);
        } catch (Exception e) {
            throw new PersistenceException("Could not persist an entity. " + e.toString(), e);
        }
    }

    @Override
    public void deleteById(String uri) {
        GroupPermission entity = findById(uri).get();
        Objects.requireNonNull(entity);
        try {
            em.remove(entity);
        } catch (Exception e) {
            throw new PersistenceException("Could not delete an entity. ", e);
        }
    }

}

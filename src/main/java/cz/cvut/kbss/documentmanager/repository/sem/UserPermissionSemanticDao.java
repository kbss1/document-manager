package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.jopa.model.EntityManager;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.exception.PersistenceException;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;

import cz.cvut.kbss.jopa.exceptions.NoResultException;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import cz.cvut.kbss.documentmanager.repository.UserPermissionDao;

@Repository
@Profile("sem")
public class UserPermissionSemanticDao implements UserPermissionDao {

    @Autowired
    EntityManager em;

    public UserPermissionSemanticDao() {
    }
    
    @Override
    public List<UserPermission> findAll() {
        try {
            return em.createNativeQuery("SELECT ?x WHERE { ?x a ?type . }", UserPermission.class)
                .setParameter("type", URI.create(Vocabulary.c_UserPermission)).getResultList();
        } catch (Exception e)  {
            throw new PersistenceException("Could not find all permissions.", e);
        }
    }

    @Override
    public boolean existsById(String uri) {
        if (uri == null) {
            return false;
        }
        return em.createNativeQuery("ASK { ?individual a ?type . }", Boolean.class)
            .setParameter("individual", URI.create(uri))
            .setParameter("type", URI.create(Vocabulary.c_UserPermission)).getSingleResult();
    }

    @Override
    public Optional<UserPermission> findById(String uri) {
        try {
            UserPermission ret = em.find(UserPermission.class, URI.create(uri));
            if (ret == null) {
                return Optional.empty();
            }
            return Optional.of(ret);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }


    @Override
    public UserPermission save(UserPermission entity) {
        Objects.requireNonNull(entity);
        try {
            return em.merge(entity);
        } catch (Exception e) {
            throw new PersistenceException("Could not persist an entity." + e.toString(), e);
        }
    }

    @Override
    public void deleteById(String uri) {
        UserPermission entity = findById(uri).get();
        Objects.requireNonNull(entity);
        try {
            em.remove(entity);
        } catch (Exception e) {
            throw new PersistenceException("Could not delete an entity.", e);
        }
    }

}

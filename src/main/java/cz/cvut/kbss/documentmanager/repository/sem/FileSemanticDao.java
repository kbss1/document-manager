package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.documentmanager.exception.PersistenceException;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.jopa.exceptions.NoResultException;
import cz.cvut.kbss.jopa.model.EntityManager;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.repository.FileDao;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile("sem")
public class FileSemanticDao implements FileDao {

    @Autowired
    EntityManager em;

    public FileSemanticDao() {
    }

    @Override
    public List<File> findAll() {
        try {
            return em.createNativeQuery("SELECT ?x WHERE { ?x a ?type . }", File.class)
                .setParameter("type", URI.create(Vocabulary.c_File)).getResultList();
        } catch (NoResultException e)  {
            throw new PersistenceException("Could not find all files.", e);
        }
    }

    @Override
    public boolean existsById(String uri) {
        if (uri == null) {
            return false;
        }
        return em.createNativeQuery("ASK { ?individual a ?type . }", Boolean.class)
            .setParameter("individual", URI.create(uri))
            .setParameter("type", URI.create(Vocabulary.c_File)).getSingleResult();
    }


    @Override
    public Optional<File> findById(String uri) {
        try {
            File file = em.find(File.class, URI.create(uri));
            if (file == null) {
                return Optional.empty();
            }
            return Optional.of(file);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public File save(File entity) {
        Objects.requireNonNull(entity);
        try {
            return em.merge(entity);
        } catch (RuntimeException e) {
            throw new PersistenceException("Could not persist an entity.", e);
        }
    }

    @Override
    public void deleteById(String uri) {
        File file = this.findById(uri).get();
        Objects.requireNonNull(file);
        try {
            em.remove(em.merge(file));
        } catch (Exception e) {
            throw new PersistenceException("Could not delete an entity.", e);
        }
    }
}

package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.documentmanager.exception.PersistenceException;
import cz.cvut.kbss.jopa.model.EntityManager;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.repository.FolderDao;
import cz.cvut.kbss.jopa.exceptions.NoResultException;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile("sem")
public class FolderSemanticDao implements FolderDao {

    @Autowired
    EntityManager em;

    public FolderSemanticDao() {
    }

    @Override
    public List<Folder> findAll() {
        try {
            return em.createNativeQuery("SELECT ?x WHERE { ?x a ?type . }", Folder.class)
                .setParameter("type", URI.create(Vocabulary.c_Folder)).getResultList();
        } catch (Exception e)  {
            throw new PersistenceException("Could not find all files.", e);
        }
    }
    
    @Override
    public boolean existsById(String uri) {
        if (uri == null) {
            return false;
        }
        return em.createNativeQuery("ASK { ?individual a ?type . }", Boolean.class)
            .setParameter("individual", URI.create(uri))
            .setParameter("type", URI.create(Vocabulary.c_Folder)).getSingleResult();
    }

    @Override
    public Optional<Folder> findById(String uri) {
        try {
            Folder folder = em.find(Folder.class, URI.create(uri));
            if (folder == null) {
                return Optional.empty();
            }
            return Optional.of(folder);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public Folder save(Folder entity) {
        Objects.requireNonNull(entity);
        try {
            return em.merge(entity);
        } catch (Exception e) {
            throw new PersistenceException("Could not persist an entity. ", e);
        }
    }

    @Override
    public void deleteById(String uri) {
        Folder folder = this.findById(uri).get();
        Objects.requireNonNull(folder);
        try {
            em.remove(folder);
        } catch (Exception e) {
            throw new PersistenceException("Could not delete an entity. ", e);
        }
    }

}

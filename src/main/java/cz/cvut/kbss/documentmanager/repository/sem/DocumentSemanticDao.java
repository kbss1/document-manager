package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.jopa.exceptions.NoResultException;
import cz.cvut.kbss.jopa.model.EntityManager;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.exception.PersistenceException;

import cz.cvut.kbss.documentmanager.repository.DocumentDao;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile("sem")
public class DocumentSemanticDao implements DocumentDao {

    @Autowired
    EntityManager em;

    public DocumentSemanticDao() {
    }

    @Override
    public List<Document> findAll() {
        try {
            return em.createNativeQuery("SELECT ?x WHERE { ?x a ?type . }", Document.class)
                .setParameter("type", URI.create(Vocabulary.c_Document)).getResultList();
        } catch (Exception e)  {
            throw new PersistenceException("Could not find all documents.", e);
        }
    }

    @Override
    public boolean existsById(String uri) {
        if (uri == null) {
            return false;
        }
        return em.createNativeQuery("ASK { ?individual a ?type . }", Boolean.class)
            .setParameter("individual", URI.create(uri))
            .setParameter("type", URI.create(Vocabulary.c_Document)).getSingleResult();
    }

    @Override
    public Optional<Document> findById(String uri) {
        try {
            Document doc = em.find(Document.class, URI.create(uri));
            if (doc == null) {
                return Optional.empty();
            }
            return Optional.of(doc);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }


    @Override
    public Document save(Document entity) {
        Objects.requireNonNull(entity);
        try {
            return em.merge(entity);
        } catch (Exception e) {
            throw new PersistenceException("Could not persist an entity.", e);
        }
    }

    @Override
    public void deleteById(String uri) {
        Document doc = findById(uri).get();
        Objects.requireNonNull(doc);
        try {
            em.remove(doc);
        } catch (Exception e) {
            throw new PersistenceException("Could not delete an entity.", e);
        }
    }

}

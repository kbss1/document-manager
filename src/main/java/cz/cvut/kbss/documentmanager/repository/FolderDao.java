package cz.cvut.kbss.documentmanager.repository;

import cz.cvut.kbss.documentmanager.model.Folder;
import java.util.List;
import java.util.Optional;

public interface FolderDao {

    public Optional<Folder> findById(String id);

    public boolean existsById(String id);
    
    public List<Folder> findAll();

    public Folder save(Folder folder);    
    
    public void deleteById(String id);
    
}

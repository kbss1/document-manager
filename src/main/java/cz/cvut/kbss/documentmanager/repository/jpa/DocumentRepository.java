package cz.cvut.kbss.documentmanager.repository.jpa;

import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.repository.DocumentDao;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface DocumentRepository extends JpaRepository<Document, String>, DocumentDao {

}

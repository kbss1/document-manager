package cz.cvut.kbss.documentmanager.repository.jpa;

import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import cz.cvut.kbss.documentmanager.repository.UserPermissionDao;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface UserPermissionRepository extends JpaRepository<UserPermission, String>, UserPermissionDao {
    
}

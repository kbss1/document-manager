package cz.cvut.kbss.documentmanager.repository;

import java.nio.file.Path;
import java.util.List;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageDao {
    
    public Resource loadFileAsResource(Path path);
    
    public void storeFileInPath(MultipartFile file, Path path);
    
    public void deleteFileByFolderPath(Path path);
    
    public boolean fileFolderExists(Path path);
    
    public List<String> getStorageFileFolders();
    
}

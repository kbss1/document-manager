package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.jopa.exceptions.NoResultException;
import cz.cvut.kbss.jopa.model.EntityManager;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.exception.PersistenceException;
import cz.cvut.kbss.documentmanager.model.security.UserGroup;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import cz.cvut.kbss.documentmanager.repository.UserGroupDao;

@Repository
@Profile("sem")
public class UserGroupSemanticDao implements UserGroupDao {

    @Autowired
    EntityManager em;

    public UserGroupSemanticDao() {
    }

    @Override
    public List<UserGroup> findAll() {
        try {
            return em.createNativeQuery("SELECT ?x WHERE { ?x a ?type . }", UserGroup.class)
                .setParameter("type", URI.create(Vocabulary.c_UserGroup)).getResultList();
        } catch (Exception e)  {
            throw new PersistenceException("Could not find all groups.", e);
        }
    }

    @Override
    public boolean existsById(String uri) {
        if (uri == null) {
            return false;
        }
        return em.createNativeQuery("ASK { ?individual a ?type . }", Boolean.class)
            .setParameter("individual", URI.create(uri))
            .setParameter("type", URI.create(Vocabulary.c_UserGroup)).getSingleResult();
    }

    @Override
    public Optional<UserGroup> findById(String uri) {
        try {
            UserGroup entity = em.find(UserGroup.class, URI.create(uri));
            if (entity == null) {
                return Optional.empty();
            }
            return Optional.of(entity);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public UserGroup save(UserGroup entity) {
        Objects.requireNonNull(entity);
        try {
            return em.merge(entity);
        } catch (Exception e) {
            throw new PersistenceException("Could not persist an entity.", e);
        }
    }

    @Override
    public void deleteById(String uri) {
        UserGroup entity = findById(uri).get();
        Objects.requireNonNull(entity);
        try {
            em.remove(entity);
        } catch (Exception e) {
            throw new PersistenceException("Could not delete an entity.", e);
        }
    }

}

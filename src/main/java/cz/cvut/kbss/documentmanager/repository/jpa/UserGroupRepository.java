package cz.cvut.kbss.documentmanager.repository.jpa;

import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import cz.cvut.kbss.documentmanager.repository.UserGroupDao;

@Repository
@Profile("jpa")
public interface UserGroupRepository extends JpaRepository<UserGroup, String>, UserGroupDao {

}

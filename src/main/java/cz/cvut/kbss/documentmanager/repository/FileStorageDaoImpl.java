package cz.cvut.kbss.documentmanager.repository;

import cz.cvut.kbss.documentmanager.conf.FileStorageConfig;
import cz.cvut.kbss.documentmanager.exception.FileStorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileStorageDaoImpl implements FileStorageDao {

    private final Path fileStorageLocation;
    
    @Autowired
    public FileStorageDaoImpl(FileStorageConfig fileStorageConfig) {
        fileStorageLocation = Paths.get(fileStorageConfig.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(fileStorageLocation);
        } catch (IOException ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }
    
    @Override
    public Resource loadFileAsResource(Path path) {
        Path filePath = fileStorageLocation.resolve(path).normalize();
        try {
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileStorageException("File not found: " + filePath.toString());
            }
        } catch (MalformedURLException ex) {
            throw new FileStorageException("File not found: " + filePath.toString(), ex);
        }
    }
    
    @Override
    public void storeFileInPath(MultipartFile file, Path path) {
        try {
            Path targetLocation = Files.createDirectories(fileStorageLocation.resolve(path));
            Files.copy(file.getInputStream(), targetLocation.resolve(file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new FileStorageException("Could not store file: " + file.getOriginalFilename() + ". Please try again! ", e);
        }
    }
    
    @Override
    public void deleteFileByFolderPath(Path path) {
        Path fileFolderPath = fileStorageLocation.resolve(path).normalize();
        try {
            Files.walkFileTree(fileFolderPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new FileStorageException("Could not delete file: " + e.toString());
        }
    }
    
    @Override
    public boolean fileFolderExists(Path path) {
        return Files.exists(fileStorageLocation.resolve(path).normalize());
    }
    
    @Override
    public List<String> getStorageFileFolders() {
        try {
            List <String> subfolders = Files.walk(fileStorageLocation, 1)
                    .filter(Files::isDirectory).map(f -> f.getFileName().toString())
                    .collect(Collectors.toList());
            subfolders.remove(0);
            return subfolders;
        } catch (IOException e) {
            throw new FileStorageException("Could not browse folders in file storage.");
        }
    }
    
}

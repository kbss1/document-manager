package cz.cvut.kbss.documentmanager.repository.jpa;

import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.repository.GroupPermissionDao;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface GroupPermissionRepository extends JpaRepository<GroupPermission, String>, GroupPermissionDao {
    
}

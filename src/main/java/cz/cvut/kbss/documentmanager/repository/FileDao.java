package cz.cvut.kbss.documentmanager.repository;

import cz.cvut.kbss.documentmanager.model.File;
import java.util.List;
import java.util.Optional;

public interface FileDao {

    public Optional<File> findById(String id);
    
    public boolean existsById(String id);
    
    public List<File> findAll();

    public File save(File file);
    
    public void deleteById(String id);
    
}

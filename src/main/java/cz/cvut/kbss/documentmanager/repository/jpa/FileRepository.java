package cz.cvut.kbss.documentmanager.repository.jpa;

import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.repository.FileDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.context.annotation.Profile;

@Repository
@Profile("jpa")
public interface FileRepository extends JpaRepository<File, String>, FileDao {
    
}

package cz.cvut.kbss.documentmanager.conf;

import cz.cvut.kbss.jopa.Persistence;
import cz.cvut.kbss.jopa.model.EntityManagerFactory;
import cz.cvut.kbss.jopa.model.JOPAPersistenceProperties;
import cz.cvut.kbss.jopa.model.JOPAPersistenceProvider;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class PersistenceFactory {
    
    @Autowired
    private Environment environment;
    private EntityManagerFactory emf;
    
    @Bean
    public EntityManagerFactory getEntityManagerFactory() {
        return emf;
    }

    @PostConstruct
    private void init() {
        final Map<String, String> properties = new HashMap<>();
        properties.put(JOPAPersistenceProperties.SCAN_PACKAGE, "cz.cvut.kbss.documentmanager.model");
        properties.put(JOPAPersistenceProperties.JPA_PERSISTENCE_PROVIDER, JOPAPersistenceProvider.class.getName());
        properties.put(JOPAPersistenceProperties.ONTOLOGY_PHYSICAL_URI_KEY, environment.getProperty("repositoryUrl"));
        properties.put(JOPAPersistenceProperties.DATA_SOURCE_CLASS, environment.getProperty("driver"));
        this.emf = Persistence.createEntityManagerFactory("DocManagerPU", properties);
    }
    
    @PreDestroy
    private void close() {
        if (emf.isOpen()) {
            emf.close();
        }
    }
    
}

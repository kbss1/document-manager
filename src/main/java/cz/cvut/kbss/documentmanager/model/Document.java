package cz.cvut.kbss.documentmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;
import cz.cvut.kbss.jopa.model.annotations.CascadeType;
import cz.cvut.kbss.jopa.model.annotations.FetchType;
import cz.cvut.kbss.jopa.model.annotations.OWLAnnotationProperty;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import cz.cvut.kbss.jopa.model.annotations.OWLObjectProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.generateUserPermissionURI;

@JsonIgnoreProperties({"persistenceContext", "fragment"})
@Entity
@OWLClass(iri = Vocabulary.c_Document)
public class Document extends Node implements Serializable {
    
    @Column(name = "description")
    @OWLAnnotationProperty(iri = Vocabulary.p_description)
    private String description;
    
    @OneToOne(cascade = javax.persistence.CascadeType.ALL)
    @JoinColumn(name = "fk_rootfolder")
    @OWLObjectProperty(iri = Vocabulary.p_rootFolder, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Folder rootFolder;
    
    @JsonIgnore
    @OneToMany(mappedBy = "document", cascade = {javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.REMOVE})
    @OWLObjectProperty(iri = Vocabulary.p_hasUserPermission, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserPermission> userPermissions;
    
    @JsonIgnore
    @OneToMany(mappedBy = "document", cascade = {javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.REMOVE})
    @OWLObjectProperty(iri = Vocabulary.p_hasGroupPermission, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<GroupPermission> groupPermissions;
    
    //Functions
    public PermissionLevel getPermissionLevelForUser(String userUri) {
        String permUri = generateUserPermissionURI(getFragment(), userUri);
        UserPermission userPerm = getUserPermissionWithUri(permUri);
        if (userPerm != null) {
            return userPerm.getPermissionLevel();
        }
        if (groupPermissions == null) {
            return PermissionLevel.NONE;
        }
        PermissionLevel maxPermLevel = PermissionLevel.NONE;
        for (GroupPermission groupPerm : groupPermissions) {
            if (groupPerm.getGroup().hasUser(userUri)) {
                PermissionLevel groupPermLevel = groupPerm.getPermissionLevel();
                if (groupPermLevel.isBigger(maxPermLevel)) {
                    maxPermLevel = groupPermLevel;
                }
            }
        }
        return maxPermLevel;
    }    
    
    public UserPermission getUserPermissionWithUri(String permURI) {
        if (userPermissions == null) return null;
        for (UserPermission perm : userPermissions) {
            if (perm.getUri().equals(permURI)) {
                return perm;
            }
        }
        return null;
    }
    
    public void addUserPermission(UserPermission permission) {
        Objects.requireNonNull(permission);
        if (userPermissions == null) {
            userPermissions = new HashSet<>();
        }
        userPermissions.add(permission);
    }
    
    public void removeUserPermissionByUri(String uri) {
        if (userPermissions == null) return;
        userPermissions.removeIf(perm -> perm.getUri().equals(uri));
    }
    
    public GroupPermission getGroupPermissionWithUri(String permURI) {
        if (groupPermissions == null) return null;
        for (GroupPermission perm : groupPermissions) {
            if (perm.getUri().equals(permURI)) {
                return perm;
            }
        }
        return null;
    }
    
    public void addGroupPermission(GroupPermission permission) {
        Objects.requireNonNull(permission);
        if (groupPermissions == null) {
            groupPermissions = new HashSet<>();
        }
        groupPermissions.add(permission);
    }
    
    public void removeGroupPermissionByUri(String uri) {
        if (groupPermissions == null) return;
        groupPermissions.removeIf(perm -> perm.getUri().equals(uri));
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Document)) {
            return false;
        }
        Document entity = (Document)o;
        return Objects.equals(getUri(), entity.getUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri());
    }
    
    //Getters + Setters

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Folder getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(Folder rootFolder) {
        this.rootFolder = rootFolder;
    }

    public Set<UserPermission> getUserPermissions() {
        return userPermissions;
    }

    public void setUserPermissions(Set<UserPermission> userPermissions) {
        this.userPermissions = userPermissions;
    }

    public Set<GroupPermission> getGroupPermissions() {
        return groupPermissions;
    }

    public void setGroupPermissions(Set<GroupPermission> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }
    
}

package cz.cvut.kbss.documentmanager.model.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.jopa.model.annotations.CascadeType;
import cz.cvut.kbss.jopa.model.annotations.FetchType;
import cz.cvut.kbss.jopa.model.annotations.OWLAnnotationProperty;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import cz.cvut.kbss.jopa.model.annotations.OWLObjectProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@JsonIgnoreProperties({"persistenceContext"})
@OWLClass(iri = Vocabulary.c_UserGroup)
@Entity
public class UserGroup implements Serializable {

    @Id
    @cz.cvut.kbss.jopa.model.annotations.Id
    private String uri;
    
    @Column
    @OWLAnnotationProperty(iri = Vocabulary.p_name)
    private String name;
    
    @ElementCollection
    @OWLAnnotationProperty(iri = Vocabulary.p_hasUser, simpleLiteral = true)
    private Set<String> users;
    
    @JsonIgnore
    @OneToMany(mappedBy = "group", cascade = {javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.REMOVE})
    @OWLObjectProperty(iri = Vocabulary.p_hasGroupPermission, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<GroupPermission> permissions;
    
    //Functions
    public boolean hasUser(String uri) {
        if (users == null) {
            return false;
        }
        return users.contains(uri);
    }
    
    public void addUser(String uri) {
        Objects.requireNonNull(uri);
        if (users == null) {
            users = new HashSet<>();
        }
        users.add(uri);
    }
    
    public void removeUser(String uri) {
        Objects.requireNonNull(uri);
        if (users == null) return;
        users.remove(uri);
    }
    
    public GroupPermission getGroupPermissionWithUri(String permURI) {
        if (permissions == null) return null;
        for (GroupPermission perm : permissions) {
            if (perm.getUri().equals(permURI)) {
                return perm;
            }
        }
        return null;
    }
    
    public void addPermission(GroupPermission permission) {
        Objects.requireNonNull(permission);
        if (permissions == null) {
            permissions = new HashSet<>();
        }
        permissions.add(permission);
    }
    
    public void removePermissionByUri(String uri) {
        if (permissions == null) return;
        permissions.removeIf(perm -> perm.getUri().equals(uri));
    }
    
    public String getFragment() {
        return uri.substring(uri.lastIndexOf("/")+1);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserGroup)) {
            return false;
        }
        UserGroup entity = (UserGroup)o;
        return Objects.equals(getUri(), entity.getUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri());
    }
    
    //Getters + Setters
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getUsers() {
        return users;
    }

    public void setUsers(Set<String> userURIs) {
        this.users = userURIs;
    }

    public Set<GroupPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<GroupPermission> permissions) {
        this.permissions = permissions;
    }
    
}

package cz.cvut.kbss.documentmanager.model;

public class Vocabulary {
    
    public static final String BASE = "http://example.cz/";
    
    //Classes
    public static final String c_Node = BASE + "Node";
    public static final String c_Document = BASE + "Document";
    public static final String c_Folder = BASE + "Folder";
    public static final String c_File = BASE + "File";
    public static final String c_FileMemento = BASE + "FileMemento";
    public static final String c_Permission = BASE + "Permission";
    public static final String c_UserPermission = BASE + "UserPermission";
    public static final String c_GroupPermission = BASE + "GroupPermission";
    public static final String c_UserGroup = BASE + "UserGroup";
    
    //Node
    public static final String p_name = BASE + "name";
    public static final String p_created = BASE + "created";
    public static final String p_lastModified = BASE + "lastModified";
    
    //Document + Folder shared
    public static final String p_description = BASE + "description";
    public static final String p_belongsToDocument = BASE + "belongsToDocument";
    public static final String p_files = BASE + "files";
    
    //Document unique
    public static final String p_rootFolder = BASE + "rootFolder";
    public static final String p_hasUserPermission = BASE + "hasUserPermission";
    public static final String p_hasGroupPermission = BASE + "hasGroupPermission";
    
    //Folder unique
    public static final String p_isRoot = BASE + "isRoot";
    public static final String p_parentFolder = BASE + "parentFolder";
    public static final String p_subfolders = BASE + "subfolders";
    
    //File + FileVersionMemento shared
    public static final String p_fileName = BASE + "fileName";
    public static final String p_fileSize = BASE + "fileSize";
    public static final String p_fileType = BASE + "fileType";
    public static final String p_location = BASE + "location";
    public static final String p_version = BASE + "version";
    
    //File unique
    public static final String p_mementos = BASE + "mementos";
        
    //User
    public static final String p_firstName = BASE + "firstName";
    public static final String p_lastName = BASE + "lastName";
    public static final String p_username = BASE + "username";
    public static final String p_types = BASE + "types";
    
    //Permissions
    public static final String p_permissionLevel = BASE + "level";
    public static final String p_userUri = BASE + "userUri";
    public static final String p_hasGroup = BASE + "hasGroup";
    
    //Group
    public static final String p_hasUser = BASE + "hasUser";
    
    private Vocabulary() {
        throw new AssertionError();
    }
    
}

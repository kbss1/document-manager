package cz.cvut.kbss.documentmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.cvut.kbss.jopa.model.annotations.OWLAnnotationProperty;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import cz.cvut.kbss.jopa.model.annotations.ParticipationConstraints;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@JsonIgnoreProperties({"persistenceContext", "fragment"})
@Entity
@OWLClass(iri = Vocabulary.c_FileMemento)
public class FileMemento implements Serializable {
    
    @Id
    @cz.cvut.kbss.jopa.model.annotations.Id
    private String uri;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @ParticipationConstraints(nonEmpty = true)
    @OWLAnnotationProperty(iri = Vocabulary.p_created)
    private Date created;

    @Column(name = "fileName")
    @OWLAnnotationProperty(iri = Vocabulary.p_fileName)
    private String fileName;

    @Column(name = "filesize")
    @OWLAnnotationProperty(iri = Vocabulary.p_fileSize)
    private Long fileSize;

    @Column(name = "filetype")
    @OWLAnnotationProperty(iri = Vocabulary.p_fileType)
    private String fileType;

    @Column(name = "version")
    @OWLAnnotationProperty(iri = Vocabulary.p_version)
    private Long version;

    //Functions
    public void setFromFile(File file) {
        this.fileSize = file.getFileSize();
        this.fileType = file.getFileType();
        this.fileName = file.getFileName();
        this.version = file.getVersion();
        this.created = file.getLastModified();
    }
    
    public void setDate() {
        this.created = new Date(System.currentTimeMillis());
    }
    
    public void generateUriFromFragment(String fragment) {
        this.uri = Vocabulary.c_FileMemento + "/" + fragment + "_" + version;
    }
    
    public String getFragment() {
        return uri.substring(uri.lastIndexOf("/")+1);
    }
    
    //Getters + Setters    
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    
    
}

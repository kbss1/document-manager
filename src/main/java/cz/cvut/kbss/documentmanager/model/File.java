 package cz.cvut.kbss.documentmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.cleanseString;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.createDJB2Hash;
import cz.cvut.kbss.jopa.model.annotations.OWLAnnotationProperty;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import cz.cvut.kbss.jopa.model.annotations.FetchType;
import cz.cvut.kbss.jopa.model.annotations.CascadeType;
import cz.cvut.kbss.jopa.model.annotations.OWLObjectProperty;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;

@JsonIgnoreProperties({"persistenceContext", "fragment", "storagePath"})
@Entity
@OWLClass(iri = Vocabulary.c_File)
public class File extends Node implements Serializable {
    
    @Column(name = "filename")
    @OWLAnnotationProperty(iri = Vocabulary.p_fileName)
    private String fileName;
    
    @Column(name = "filesize")
    @OWLAnnotationProperty(iri = Vocabulary.p_fileSize)
    private Long fileSize;

    @Column(name = "filetype")
    @OWLAnnotationProperty(iri = Vocabulary.p_fileType)
    private String fileType;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_location")
    @OWLObjectProperty(iri = Vocabulary.p_location, fetch = FetchType.EAGER)
    private Folder location;
    
    @Column(name = "version")
    @OWLAnnotationProperty(iri = Vocabulary.p_version)
    private Long version;

    @JsonIgnore
    @OneToMany(cascade = javax.persistence.CascadeType.ALL)
    @JoinColumn(name = "fk_file")
    @OWLObjectProperty(iri = Vocabulary.p_mementos, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<FileMemento> versions;

    //Functions    
    public void incrementVersion() {
        if (this.version == null) {
            this.version = 0L;
        }
        this.version++;
    }
    
    public Path getStoragePath() {
        String hashedPart = getUri().substring(0,getUri().lastIndexOf("/"));
        Path storingPath = Paths.get(cleanseString(getFragment()) + "_" + createDJB2Hash(hashedPart));
        return storingPath;
    }
    
    public Path getStoragePath(long version) {
        Path storingPath = getStoragePath();
        storingPath = storingPath.resolve(Long.toString(version));
        return storingPath;
    }
    
    public FileMemento getMementoVersion(long version) {
        if (versions == null) return null;
        for (FileMemento memento : versions) {
            if (Objects.equals(memento.getVersion(), version)) {
                return memento;
            }
        }
        return null;
    }
    
    public void saveMemento(FileMemento memento) {
        if (versions == null) {
            versions = new HashSet<>();
        }
        versions.add(memento);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Folder)) {
            return false;
        }
        File entity = (File)o;
        return Objects.equals(getUri(), entity.getUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri());
    }
    
    //Getters + Setters
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Folder getLocation() {
        return location;
    }

    public void setLocation(Folder location) {
        this.location = location;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Set<FileMemento> getVersions() {
        return versions;
    }
    
}

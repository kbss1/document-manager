package cz.cvut.kbss.documentmanager.model.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.jopa.model.annotations.FetchType;
import cz.cvut.kbss.jopa.model.annotations.OWLAnnotationProperty;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import cz.cvut.kbss.jopa.model.annotations.OWLObjectProperty;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@OWLClass(iri = Vocabulary.c_Permission)
@cz.cvut.kbss.jopa.model.annotations.MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Permission {

    @Id
    @cz.cvut.kbss.jopa.model.annotations.Id
    private String uri;
    
    @Column(nullable = false)
    @OWLAnnotationProperty(iri = Vocabulary.p_permissionLevel)
    private PermissionLevel permissionLevel;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_document")
    @OWLObjectProperty(iri = Vocabulary.p_belongsToDocument, fetch = FetchType.EAGER)
    private Document document;
    
    //Functions
    public String getFragment() {
        return uri.substring(uri.lastIndexOf("/")+1);
    }
    
    //Getters + Setters
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public PermissionLevel getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(PermissionLevel permissionLevel) {
        this.permissionLevel = permissionLevel;
    }
    
    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        Objects.requireNonNull(document);
        this.document = document;
    }
    
}

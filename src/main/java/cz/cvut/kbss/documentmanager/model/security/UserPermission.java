package cz.cvut.kbss.documentmanager.model.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.jopa.model.annotations.OWLAnnotationProperty;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;

@JsonIgnoreProperties({"persistenceContext"})
@OWLClass(iri = Vocabulary.c_UserPermission)
@Entity
public class UserPermission extends Permission implements Serializable {

    @Column
    @OWLAnnotationProperty(iri = Vocabulary.p_userUri)
    private String userURI;
    
    //Functions    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserPermission)) {
            return false;
        }
        UserPermission entity = (UserPermission)o;
        return Objects.equals(getUri(), entity.getUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri());
    }
    
    //Getters + Setters
    public String getUserURI() {
        return userURI;
    }

    public void setUserURI(String userURI) {
        Objects.requireNonNull(userURI);
        this.userURI = userURI;
    }
    
}

package cz.cvut.kbss.documentmanager.model.security;

public enum PermissionLevel {
    NONE(0),
    READ(1),
    WRITE(2),
    SECURITY(3);
    
    private final int level;
    
    PermissionLevel(int level) {
        this.level = level;
    }
    
    PermissionLevel(String strLevel) {
        for (PermissionLevel perm : values()) {
            if (perm.toString().equals(strLevel)) {
                this.level = perm.level;
                return;
            }
        }
        throw new IllegalArgumentException("No permission level found for " + strLevel + ".");
    }
    
    public boolean isPermitted(PermissionLevel other) {
        return this.level >= other.level;
    }
    
    public boolean isBigger(PermissionLevel other) {
        return this.level > other.level;
    }
    
}
package cz.cvut.kbss.documentmanager.model.security;

import java.security.Principal;
import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class AuthenticationToken extends AbstractAuthenticationToken implements Principal {

    private final DocManUserDetails userDetails;

    public AuthenticationToken(Collection<? extends GrantedAuthority> authorities, DocManUserDetails userDetails) {
        super(authorities);
        this.userDetails = userDetails;
        super.setAuthenticated(true);
        super.setDetails(userDetails);
    }

    @Override
    public Object getCredentials() {
        return userDetails.getPassword();
    }

    @Override
    public Object getPrincipal() {
        return userDetails;
    }

    @Override
    public DocManUserDetails getDetails() {
        return userDetails;
    }
}
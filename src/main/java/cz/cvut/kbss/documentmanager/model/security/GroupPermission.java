package cz.cvut.kbss.documentmanager.model.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.jopa.model.annotations.FetchType;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import cz.cvut.kbss.jopa.model.annotations.OWLObjectProperty;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@JsonIgnoreProperties({"persistenceContext"})
@OWLClass(iri = Vocabulary.c_GroupPermission)
@Entity
public class GroupPermission extends Permission implements Serializable {
    
    @ManyToOne
    @JoinColumn(name = "fk_group")
    @OWLObjectProperty(iri = Vocabulary.p_hasGroup, fetch = FetchType.EAGER)
    private UserGroup group;
    
    //Functions    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GroupPermission)) {
            return false;
        }
        GroupPermission entity = (GroupPermission)o;
        return Objects.equals(getUri(), entity.getUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri());
    }
    
    //Getters + Setters
    public UserGroup getGroup() {
        return group;
    }

    public void setGroup(UserGroup group) {
        Objects.requireNonNull(group);
        this.group = group;
    }
    
}

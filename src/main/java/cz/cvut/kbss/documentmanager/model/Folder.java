package cz.cvut.kbss.documentmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.cvut.kbss.jopa.model.annotations.OWLAnnotationProperty;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import cz.cvut.kbss.jopa.model.annotations.FetchType;
import cz.cvut.kbss.jopa.model.annotations.CascadeType;
import cz.cvut.kbss.jopa.model.annotations.OWLObjectProperty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;

@OWLClass(iri = Vocabulary.c_Folder)
@JsonIgnoreProperties({"persistenceContext", "folderPath", "fragment"})
@Entity
public class Folder extends Node implements Serializable {
    
    @Column(name = "description")
    @OWLAnnotationProperty(iri = Vocabulary.p_description)
    private String description;
    
    @Column(name = "isRoot")
    @OWLAnnotationProperty(iri = Vocabulary.p_isRoot)
    private Boolean isRoot;
    
    @ManyToOne
    @JoinColumn(name = "subfolderId")
    @OWLObjectProperty(iri = Vocabulary.p_parentFolder, fetch = FetchType.EAGER)
    private Folder parentFolder;
    
    @JsonIgnore
    @OneToMany(mappedBy = "parentFolder", cascade = {javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.REMOVE})
    @OWLObjectProperty(iri = Vocabulary.p_subfolders, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Folder> folders;
    
    @JsonIgnore
    @OneToMany(mappedBy = "location", cascade = {javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.REMOVE})
    @OWLObjectProperty(iri = Vocabulary.p_files, cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private Set<File> files;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_document")
    @OWLObjectProperty(iri = Vocabulary.p_belongsToDocument, fetch = FetchType.EAGER)
    private Document document;

    //Functions    
    public void addSubfolder(Folder folder) {
        Objects.requireNonNull(folder);
        if (folders == null) {
            this.folders = new HashSet<>();
        }
        folders.add(folder);
    }
    
    public void removeSubfolder(Folder folder) {
        Objects.requireNonNull(folder);
        if (folders == null) {
            return;
        }
        folders.remove(folder);
    }

    public void addFile(File file) {
        Objects.requireNonNull(file);
        if (files == null) {
            this.files = new HashSet<>();
        }
        files.add(file);
    }
    
    public void removeFile(File file) {
        Objects.requireNonNull(file);
        if (files == null) {
            return;
        }
        files.remove(file);
    }
    
    public String getFolderPath() {
        StringBuilder sb = new StringBuilder("/" + getName());
        Folder folder = this;
        while (!folder.isRoot()) {
            folder = folder.getParentFolder();
            sb.insert(0,"/" + folder.getName());
        }
        sb.deleteCharAt(0);
        return sb.toString().replace(" ", "_");
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Folder)) {
            return false;
        }
        Folder entity = (Folder)o;
        return Objects.equals(getUri(), entity.getUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri());
    }
    
    //Getters + Setters
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isRoot() {
        return isRoot;
    }

    public void setIsRoot(Boolean isRoot) {
        this.isRoot = isRoot;
    }

    public Folder getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(Folder parentFolder) {
        this.parentFolder = parentFolder;
    }

    public Set<Folder> getFolders() {
        return folders;
    }

    public void setFolders(Set<Folder> folders) {
        this.folders = folders;
    }

    public Set<File> getFiles() {
        return files;
    }

    public void setFiles(Set<File> files) {
        this.files = files;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

}

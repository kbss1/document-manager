package cz.cvut.kbss.documentmanager.model;

import cz.cvut.kbss.jopa.model.annotations.OWLAnnotationProperty;
import cz.cvut.kbss.jopa.model.annotations.OWLClass;
import cz.cvut.kbss.jopa.model.annotations.ParticipationConstraints;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@OWLClass(iri = Vocabulary.c_Node)
@cz.cvut.kbss.jopa.model.annotations.MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Node {

    @Id
    @cz.cvut.kbss.jopa.model.annotations.Id
    private String uri;
    
    @Column(name = "name", nullable = false)
    @ParticipationConstraints(nonEmpty = true)
    @OWLAnnotationProperty(iri = Vocabulary.p_name)
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @ParticipationConstraints(nonEmpty = true)
    @OWLAnnotationProperty(iri = Vocabulary.p_created)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    @ParticipationConstraints(nonEmpty = true)
    @OWLAnnotationProperty(iri = Vocabulary.p_lastModified)
    private Date lastModified;

    //Functions
    public void updateLastModified() {
        lastModified.setTime(System.currentTimeMillis());
    }
    
    public void initDates() {
        created = new Date(System.currentTimeMillis());
        lastModified = new Date(System.currentTimeMillis());
    }
    
    public String getFragment() {
        return uri.substring(uri.lastIndexOf("/")+1);
    }
    
    //Getters + setters
    public void setUri(String uri) {
        this.uri = uri;
    }
    
    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public Date getLastModified() {
        return lastModified;
    }
    
}


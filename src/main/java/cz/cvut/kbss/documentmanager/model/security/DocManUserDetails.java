package cz.cvut.kbss.documentmanager.model.security;

import cz.cvut.kbss.documentmanager.util.Constants;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class DocManUserDetails implements UserDetails {

    public static final GrantedAuthority DEFAULT_AUTHORITY = new SimpleGrantedAuthority("ROLE_USER");
    private final Set<GrantedAuthority> authorities;
    private final UserAccount user;

    public DocManUserDetails(UserAccount user) {
        Objects.requireNonNull(user);
        this.user = user;
        this.authorities = resolveAuthorities(user);
    }

    private static Set<GrantedAuthority> resolveAuthorities(UserAccount user) {
        Set<GrantedAuthority> authorities = new HashSet<>(4);
        authorities.add(DEFAULT_AUTHORITY);
        if (user.hasType(Constants.ADMIN_ROLE)) {
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }
        return authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.unmodifiableCollection(authorities);
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return user.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getUserUri() {
        return user.uri;
    }

    public String getUserFirstName() {
        return user.firstName;
    }

    public String getUserLastName() {
        return user.lastName;
    }
}

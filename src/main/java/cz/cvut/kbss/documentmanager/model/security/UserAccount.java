package cz.cvut.kbss.documentmanager.model.security;

import java.io.Serializable;
import java.util.Set;

public class UserAccount implements Serializable {
    
    public String uri;
    
    public String firstName;
    
    public String lastName;
    
    public String username;
    
    public Set<String> types;

    public boolean hasType(String type) {
        return types != null && types.contains(type);
    }
}
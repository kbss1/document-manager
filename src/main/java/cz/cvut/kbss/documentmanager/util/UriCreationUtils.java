package cz.cvut.kbss.documentmanager.util;

import cz.cvut.kbss.documentmanager.model.Vocabulary;
import java.net.URL;
import java.util.Arrays;

public class UriCreationUtils {
    
    private static final char REPLACEMENT_CHARACTER = '_';
    private static final int[] ILLEGAL_FILENAME_CHARS = {34, 60, 62, 124, 0, 1,
        2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
        22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 58, 42, 63, 92, 47};

    static {
        Arrays.sort(ILLEGAL_FILENAME_CHARS);
    }    
    
    public static String ResolveUri(String fragment, String namespace) {
        if (!namespace.endsWith("/")) {
            namespace += "/";
        }
        return namespace + cleanseString(fragment);
    }
    
    public static boolean isUriValid(String uri) {
        try {
            new URL(uri).toURI();
            return true; 
        }
        catch (Exception e) {
            return false; 
        } 
    }
    
    public static String cleanseString(String string) {
        StringBuilder cleanName = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            int c = string.charAt(i);
            if (Arrays.binarySearch(ILLEGAL_FILENAME_CHARS, c) < 0) {
                cleanName.append((char) c);
            } else {
                cleanName.append(REPLACEMENT_CHARACTER);
            }
        }
        return cleanName.toString().trim();
    }
    
    public static String createDJB2Hash(String hashedPart) {
        long hash = 5381;
        for (int i = 0; i < hashedPart.length(); i++) {
            hash = ((hash << 5) + hash) + hashedPart.charAt(i);
        }
        if (hash < 0) {
            hash = -hash;
        }
        return Long.toString(hash,36);
    }
    
    public static String generateRandomHash() {
        return Long.toString(System.currentTimeMillis(), 36);
    }
    
    public static String generateDocumentURI() {
        return Vocabulary.c_Document + "/Document_" + UriCreationUtils.generateRandomHash();
    }
    
    public static String generateFolderURI() {
        return Vocabulary.c_Folder + "/Folder_" + UriCreationUtils.generateRandomHash();
    }
    
    public static String generateFileURI() {
        return Vocabulary.c_File + "/File_" + UriCreationUtils.generateRandomHash();
    }
    
    public static String generateUserGroupURI() {
        return Vocabulary.c_UserGroup + "/UserGroup_" + UriCreationUtils.generateRandomHash();
    }
    
    public static String generateUserPermissionURI(String docFragment, String userURI) {
        return Vocabulary.c_UserPermission + "/" + docFragment + "_" + createDJB2Hash(userURI);
    }
    
    public static String generateGroupPermissionURI(String docFragment, String groupURI) {
        return Vocabulary.c_GroupPermission + "/" + docFragment + "_" + createDJB2Hash(groupURI);
    }
}

package cz.cvut.kbss.documentmanager.util;

import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;

public class Constants {    
    
    /**
     * Permission level Assigned to a document author
     */
    public static final PermissionLevel MAX_PERM_LEVEL = PermissionLevel.SECURITY;
    
    /**
     * Administrator role term
     */
    public static final String ADMIN_ROLE = "http://onto.fel.cvut.cz/ontologies/application/termit/pojem/administrátor-termitu";
    
}
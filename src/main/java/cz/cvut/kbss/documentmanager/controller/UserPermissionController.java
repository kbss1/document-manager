package cz.cvut.kbss.documentmanager.controller;

import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.ResolveUri;
import cz.cvut.kbss.jsonld.JsonLd;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import cz.cvut.kbss.documentmanager.service.security.UserPermissionService;

@RestController
@RequestMapping("/permissions/user")
public class UserPermissionController {
    private final UserPermissionService permissionService;

    public UserPermissionController(UserPermissionService permissionService) {
        this.permissionService = permissionService;
    }
    
    //GET
    @GetMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForUserPermission(#fragment, #namespace, 'READ')")
    public UserPermission getUserPermission(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return permissionService.findByURI(ResolveUri(fragment, namespace));
    }

    //PUT
    @PutMapping(path = "/{fragment}", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForUserPermission(#fragment, #namespace, 'SECURITY')")
    public UserPermission updateUserPermission(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody UserPermission permission) {
        return permissionService.update(permission, ResolveUri(fragment, namespace));
    }
    
    //DELETE
    @DeleteMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForUserPermission(#fragment, #namespace, 'SECURITY')")
    public void deleteUserPermission(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        permissionService.deleteByURI(ResolveUri(fragment, namespace));
    }
    
}

package cz.cvut.kbss.documentmanager.controller;

import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.ResolveUri;
import cz.cvut.kbss.jsonld.JsonLd;
import org.springframework.web.bind.annotation.*;
import java.util.Set;
import org.springframework.http.MediaType;
import cz.cvut.kbss.documentmanager.service.security.UserGroupService;
import org.springframework.security.access.prepost.PreAuthorize;

@RestController
@RequestMapping("/groups")
public class UserGroupController {
    private final UserGroupService groupService;

    public UserGroupController(UserGroupService groupService) {
        this.groupService = groupService;
    }
    
    //GET
    @GetMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserGroup getUserGroup(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return groupService.findByURI(ResolveUri(fragment, namespace));
    }
    
    @GetMapping(path = "/{fragment}/users")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Set<String> getUserGroupUsers(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return groupService.getUsers(ResolveUri(fragment, namespace));
    }
    
    //POST
    @PostMapping(path = "/{fragment}", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserGroup createUserGroup(@RequestBody UserGroup group) {
        return groupService.create(group);
    }
    
    @PostMapping(path = "/{fragment}/users")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void addUserToUserGroup(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody String useruri) {
        groupService.addUser(ResolveUri(fragment, namespace), useruri);
    }
    
    //PUT
    @PutMapping(path = "/{fragment}", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserGroup updateUserGroup(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody UserGroup group) {
        return groupService.update(group, ResolveUri(fragment, namespace));
    }

    //DELETE
    @DeleteMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteUserGroup(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        groupService.deleteByURI(ResolveUri(fragment, namespace));
    }
    
    @DeleteMapping(path = "/{fragment}/users")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void removeUserFromUserGroup(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody String useruri) {
        groupService.removeUser(ResolveUri(fragment, namespace), useruri);
    }
    
}

package cz.cvut.kbss.documentmanager.controller;

import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import cz.cvut.kbss.documentmanager.service.DocumentService;
import cz.cvut.kbss.documentmanager.service.FileService;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.ResolveUri;
import cz.cvut.kbss.jsonld.JsonLd;
import java.util.Set;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/documents")
public class DocumentController {
    private final DocumentService documentService;
    private final FileService fileService;

    public DocumentController(DocumentService documentService, FileService fileService) {
        this.documentService = documentService;
        this.fileService = fileService;
    }
    
    //GET
    @GetMapping
    public Set<Document> getDocumentList() {
        return documentService.getDocumentList();
    }
    
    @GetMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'READ')")
    public Document getDocument(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return documentService.findByURI(ResolveUri(fragment, namespace));
    }
    
    @GetMapping(path = "/{fragment}/permissions/user")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'READ')")
    public Set<UserPermission> getDocumentUserPermissions(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return documentService.getUserPermissionsByDocumentURI(ResolveUri(fragment, namespace));
    }
    
    @GetMapping(path = "/{fragment}/permissions/group")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'READ')")
    public Set<GroupPermission> getDocumentGroupPermissions(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return documentService.getGroupPermissionsByDocumentURI(ResolveUri(fragment, namespace));
    }

    //POST
    @PostMapping(consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    public Document createDocument(@RequestBody Document document) {
        return documentService.create(document);
    }
    
    @PostMapping(path = "/{fragment}/permissions/user", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'SECURITY')")
    public UserPermission createUserPermission(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody UserPermission permission) {
        return documentService.createUserPermission(permission, ResolveUri(fragment, namespace));
    }
    
    @PostMapping(path = "/{fragment}/permissions/group", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'SECURITY')")
    public GroupPermission createGroupPermission(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody GroupPermission permission) {
        return documentService.createGroupPermission(permission, ResolveUri(fragment, namespace));
    }
    
    @PostMapping(path = "/{fragment}/files")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'SECURITY')")
    public File uploadFileToDocument(@PathVariable String fragment,
                                  @RequestParam(name = "namespace", required = true) String namespace,
                                  @RequestParam("file") MultipartFile file,
                                  @RequestParam(name = "uri", required = false) String uri,
                                  @RequestParam(name = "name", required = true) String name) {
        Document doc = documentService.findByURI(UriCreationUtils.ResolveUri(fragment, namespace));
        return fileService.uploadFile(file, uri, name, doc.getRootFolder().getUri());
    }
    
    //PUT
    @PutMapping(path = "/{fragment}", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'SECURITY')")
    public Document updateDocument(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody Document document) {
        return documentService.update(document, ResolveUri(fragment, namespace));
    }
    
    //DELETE
    @DeleteMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'SECURITY')")
    public void deleteDocument(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        documentService.deleteByURI(ResolveUri(fragment, namespace));
    }
    
}

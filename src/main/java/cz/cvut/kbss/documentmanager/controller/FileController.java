package cz.cvut.kbss.documentmanager.controller;

import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.FileMemento;
import cz.cvut.kbss.documentmanager.service.DocumentService;
import cz.cvut.kbss.documentmanager.service.FileService;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.ResolveUri;
import cz.cvut.kbss.jsonld.JsonLd;
import java.io.IOException;
import org.springframework.web.bind.annotation.*;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/files")
public class FileController {
    private final FileService fileService;
    private final DocumentService documentService;

    public FileController(FileService fileService, DocumentService documentService) {
        this.fileService = fileService;
        this.documentService = documentService;
    }
    
    //GET    
    @GetMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFile(#fragment, #namespace, 'READ')")
    public File getFile(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return fileService.findByURI(ResolveUri(fragment, namespace));
    }
    
    @GetMapping(path = "/{fragment}/versions")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFile(#fragment, #namespace, 'READ')")
    public Set<FileMemento> getFileVersions(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return fileService.getAllFileMementos(ResolveUri(fragment, namespace));
    }
    
    @GetMapping(path = "/{fragment}/versions/{version}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFile(#fragment, #namespace, 'READ')")
    public FileMemento getFileVersion(@PathVariable String fragment, @PathVariable Long version,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return fileService.getFileMemento(ResolveUri(fragment, namespace), version);
    }
    
    @GetMapping(path = "/{fragment}/content")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFile(#fragment, #namespace, 'READ')")
    public ResponseEntity<Resource> getFileContent(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            HttpServletRequest request) {
        Resource resource = fileService.getFileContent(ResolveUri(fragment, namespace), null);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(getContentType(request, resource)))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    
    @GetMapping(path = "/{fragment}/content/{version}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFile(#fragment, #namespace, 'READ')")
    public ResponseEntity<Resource> getFileContentCertainVersion(@PathVariable String fragment,
            @PathVariable Long version, HttpServletRequest request,
            @RequestParam(name = "namespace", required = true) String namespace) {
        Resource resource = fileService.getFileContent(ResolveUri(fragment, namespace), version);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(getContentType(request, resource)))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    
    @PostMapping(path = "/{fragment}/document")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForDocument(#fragment, #namespace, 'SECURITY')")
    public File uploadFileToDocumentRootFolder(@PathVariable String fragment,
                                  @RequestParam(name = "namespace", required = true) String namespace,
                                  @RequestParam("file") MultipartFile file,
                                  @RequestParam(name = "uri", required = false) String uri,
                                  @RequestParam(name = "name", required = true) String name) {
        Document doc = documentService.findByURI(uri);
        return fileService.uploadFile(file, uri, name, doc.getRootFolder().getUri());
    }
    
    //PUT
    @PutMapping(path = "/{fragment}", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFile(#fragment, #namespace, 'SECURITY')")
    public File updateFileInfo(@PathVariable String fragment,
                                  @RequestParam(name = "namespace", required = true) String namespace,
                                  @RequestBody File fileInfo) {
        return fileService.updateFileInfo(fileInfo, ResolveUri(fragment, namespace));
    }
    
    @PutMapping(path = "/{fragment}/content")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFile(#fragment, #namespace, 'WRITE')")
    public File updateFileContent(@PathVariable String fragment,
                                  @RequestParam(name = "namespace", required = true) String namespace,
                                  @RequestParam(name = "file", required = true) MultipartFile file) {
        return fileService.updateFileContent(file, ResolveUri(fragment, namespace));
    }

    //DELETE
    @DeleteMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFile(#fragment, #namespace, 'SECURITY')")
    public ResponseEntity deleteFile(@PathVariable String fragment, @RequestParam(name = "namespace", required = true) String namespace) {
        fileService.deleteByURI(ResolveUri(fragment, namespace));
        return ResponseEntity.ok().build();
    }

    //UTIL
    private static String getContentType(HttpServletRequest request, Resource resource) {
        String contentType;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException e) { contentType = "application/octet-stream"; }
        return contentType == null ? "application/octet-stream" : contentType;
    }
}

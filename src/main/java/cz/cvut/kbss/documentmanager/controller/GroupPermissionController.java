package cz.cvut.kbss.documentmanager.controller;

import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.service.security.GroupPermissionService;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.ResolveUri;
import cz.cvut.kbss.jsonld.JsonLd;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;

@RestController
@RequestMapping("/permissions/group")
public class GroupPermissionController {
    private final GroupPermissionService permissionService;

    public GroupPermissionController(GroupPermissionService permissionService) {
        this.permissionService = permissionService;
    }
    
    //GET
    @GetMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForGroupPermission(#fragment, #namespace, 'READ')")
    public GroupPermission getPermission(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        GroupPermission ret = permissionService.findByURI(ResolveUri(fragment, namespace));
        return ret;
    }

    //PUT
    @PutMapping(path = "/{fragment}", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForGroupPermission(#fragment, #namespace, 'SECURITY')")
    public GroupPermission updatePermission(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody GroupPermission permission) {
        return permissionService.update(permission, ResolveUri(fragment, namespace));
    }
    
    //DELETE
    @DeleteMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForGroupPermission(#fragment, #namespace, 'SECURITY')")
    public void deletePermission(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        permissionService.deleteByURI(ResolveUri(fragment, namespace));
    }
    
}

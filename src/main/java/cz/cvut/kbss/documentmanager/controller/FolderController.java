package cz.cvut.kbss.documentmanager.controller;

import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.service.FileService;
import cz.cvut.kbss.documentmanager.service.FolderService;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.ResolveUri;
import cz.cvut.kbss.jsonld.JsonLd;
import org.springframework.web.bind.annotation.*;
import java.util.Set;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/folders")
public class FolderController {
    private final FolderService folderService;
    private final FileService fileService;

    public FolderController(FolderService folderService, FileService fileService) {
        this.folderService = folderService;
        this.fileService = fileService;
    }
    
    //GET
    @GetMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFolder(#fragment, #namespace, 'READ')")
    public Folder getFolder(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return folderService.findByURI(ResolveUri(fragment, namespace));
    }
    
    @GetMapping(path = "/{fragment}/files")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFolder(#fragment, #namespace, 'READ')")
    public Set<File> getFolderFiles(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return folderService.getAllFolderFiles(ResolveUri(fragment, namespace));
    }
    
    @GetMapping(path = "/{fragment}/subfolders")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFolder(#fragment, #namespace, 'READ')")
    public Set<Folder> getFolderSubfolders(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        return folderService.getAllFolderSubfolders(ResolveUri(fragment, namespace));
    }

    //POST
    @PostMapping(path = "/{fragment}/subfolders", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFolder(#fragment, #namespace, 'SECURITY')")
    public Folder createSubfolder(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody Folder folder) {
        return folderService.createSubFolder(folder, ResolveUri(fragment, namespace));
    }
    
    @PostMapping(path = "/{fragment}/files")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFolder(#fragment, #namespace, 'SECURITY')")
    public File uploadFileToFolder(@PathVariable String fragment,
                                  @RequestParam(name = "namespace", required = true) String namespace,
                                  @RequestParam("file") MultipartFile file,
                                  @RequestParam(name = "uri", required = false) String uri,
                                  @RequestParam(name = "name", required = true) String name) {
        String folderURI = ResolveUri(fragment, namespace);
        return fileService.uploadFile(file, uri, name, folderURI);        
    }
    
    //PUT    
    @PutMapping(path = "/{fragment}", consumes = {JsonLd.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFolder(#fragment, #namespace, 'SECURITY')")
    public Folder updateFolder(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace,
            @RequestBody Folder folder) {
        return folderService.update(folder, ResolveUri(fragment, namespace));
    }

    //DELETE
    @DeleteMapping(path = "/{fragment}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @authorizationService.isAuthorizedForFolder(#fragment, #namespace, 'SECURITY')")
    public void deleteFolder(@PathVariable String fragment,
            @RequestParam(name = "namespace", required = true) String namespace) {
        folderService.deleteByURI(ResolveUri(fragment, namespace));
    }
}

package cz.cvut.kbss.documentmanager;

import cz.cvut.kbss.documentmanager.conf.FileStorageConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;


@EnableConfigurationProperties({
        FileStorageConfig.class
})

@SpringBootApplication(scanBasePackages = {"cz.cvut.kbss.documentmanager.*"}, exclude = {
        UserDetailsServiceAutoConfiguration.class})
@EnableScheduling
@EnableJpaAuditing
public class DocumentManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DocumentManagerApplication.class, args);
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}


package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.Folder;
import java.util.Set;

public interface FolderService {
   Folder findByURI(String uri);

   public Set<Folder> getAllFolderSubfolders(String uri);
   
   public Set<File> getAllFolderFiles(String uri);
   
   public Folder createSubFolder(Folder folder, String parentFolderURI);
      
   Folder update(Folder newFolder, String uri);
   
   void deleteByURI(String uri);
   
}

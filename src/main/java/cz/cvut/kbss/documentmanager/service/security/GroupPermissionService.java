package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.model.security.GroupPermission;

public interface GroupPermissionService {
    
   GroupPermission findByURI(String permUri);

   public GroupPermission update(GroupPermission permission, String permURI);
      
   void deleteByURI(String uri);
   
}

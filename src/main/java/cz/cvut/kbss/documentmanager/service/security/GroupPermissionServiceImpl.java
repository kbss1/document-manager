package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.repository.GroupPermissionDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

@Service
public class GroupPermissionServiceImpl implements GroupPermissionService {
    private final GroupPermissionDao permissionDao;

    public GroupPermissionServiceImpl(GroupPermissionDao permissionDao) {
        this.permissionDao = permissionDao;
    }
    
    @Override
    @Transactional
    public GroupPermission findByURI(String uri) {
        Optional<GroupPermission> perm = permissionDao.findById(uri);
        if (perm.isPresent()) {
            return perm.get();
        } else {
            throw new NotFoundException("Permission with URI '" + uri + "' was not found");
        }
    }
    
    @Override
    @Transactional
    public GroupPermission update(GroupPermission permission, String permURI) {
        if (permission.getPermissionLevel() == null) {
            throw new IncorrectRequestException("Permission level is a required field");
        } else if (permission.getUri() == null) {
            throw new IncorrectRequestException("URI is a required field");
        } else if (!permURI.equals(permission.getUri())) {
            throw new IncorrectRequestException("URIs have to match");
        }
        GroupPermission permToUpdate = findByURI(permURI);
        permToUpdate.setPermissionLevel(permission.getPermissionLevel());
        
        GroupPermission permToUpdateDoc = permToUpdate.getDocument().getGroupPermissionWithUri(permURI);
        permToUpdateDoc.setPermissionLevel(permission.getPermissionLevel());
        
        GroupPermission permToUpdateGroup = permToUpdate.getGroup().getGroupPermissionWithUri(permURI);        
        permToUpdateGroup.setPermissionLevel(permission.getPermissionLevel());
        
        return permToUpdate;
    }

    @Override
    @Transactional
    public void deleteByURI(String uri) {
        if (!permissionDao.existsById(uri)) {
            throw new NotFoundException("Permission with URI '" + uri + "' was not found");
        }
        GroupPermission perm = findByURI(uri);
        perm.getDocument().removeGroupPermissionByUri(uri);
        perm.getGroup().removePermissionByUri(uri);
        permissionDao.deleteById(uri);
        
    }
    
}

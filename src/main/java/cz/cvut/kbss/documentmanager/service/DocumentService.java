package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import java.util.Set;

public interface DocumentService {
    
    Set<Document> getDocumentList();
    
    Document findByURI(String docUri);
    
    Set<UserPermission> getUserPermissionsByDocumentURI(String docUri);
    
    Set<GroupPermission> getGroupPermissionsByDocumentURI(String docUri);

    Document create(Document document);
    
    UserPermission createUserPermission(UserPermission permission, String docUri);
    
    GroupPermission createGroupPermission(GroupPermission permission, String docUri);

    Document update(Document newDocument, String docUri);

    void deleteByURI(String docUri);
    
}

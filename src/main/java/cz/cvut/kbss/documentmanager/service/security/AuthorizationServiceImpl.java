package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.security.DocManUserDetails;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import cz.cvut.kbss.documentmanager.repository.*;
import static cz.cvut.kbss.documentmanager.security.SecurityServiceImpl.getCurrentUserDetails;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.ResolveUri;
import java.util.Optional;
import org.springframework.stereotype.Service;


@Service(value="authorizationService")
public class AuthorizationServiceImpl implements AuthorizationService {

    private final DocumentDao documentDao;
    private final FolderDao folderDao;
    private final FileDao fileDao;
    private final UserPermissionDao userPermissionDao;    
    private final GroupPermissionDao groupPermissionDao;

    public AuthorizationServiceImpl(DocumentDao documentDao, FolderDao folderDao, FileDao fileDao, UserPermissionDao userPermissionDao, GroupPermissionDao groupPermissionDao) {
        this.documentDao = documentDao;
        this.folderDao = folderDao;
        this.fileDao = fileDao;
        this.userPermissionDao = userPermissionDao;
        this.groupPermissionDao = groupPermissionDao;
    }

    public boolean isAuthorizedForDocumentURI(String documentURI, PermissionLevel level) {
        DocManUserDetails currentUser = getCurrentUserDetails();
        Document document = documentDao.findById(documentURI).get();
        PermissionLevel gotPermLevel = document.getPermissionLevelForUser(currentUser.getUserUri());
        return gotPermLevel.isPermitted(level);
    }
    
    @Override
    public boolean isAuthorizedForDocument(String fragment, String namespace, PermissionLevel level) {
        String uri = ResolveUri(fragment, namespace);
        Optional<Document> documentOpt = documentDao.findById(uri);
        if (!documentOpt.isPresent()) {
            throw new NotFoundException("Document with URI '" + uri + "' was not found");
        }
        return isAuthorizedForDocumentURI(documentOpt.get().getUri(), level);
    }

    @Override
    public boolean isAuthorizedForFolder(String fragment, String namespace, PermissionLevel level) {
        String uri = ResolveUri(fragment, namespace);
        Optional<Folder> folderOpt = folderDao.findById(uri);
        if (!folderOpt.isPresent()) {
            throw new NotFoundException("Folder with URI '" + uri + "' was not found");
        }
        Folder folder = folderOpt.get();
        return isAuthorizedForDocumentURI(folder.getDocument().getUri(), level);
    }

    @Override
    public boolean isAuthorizedForFile(String fragment, String namespace, PermissionLevel level) {
        String uri = ResolveUri(fragment, namespace);
        Optional<File> fileOpt = fileDao.findById(uri);
        if (!fileOpt.isPresent()) {
            throw new NotFoundException("File with URI '" + uri + "' was not found");
        }
        File file = fileOpt.get();
        return isAuthorizedForDocumentURI(file.getLocation().getDocument().getUri(), level);
    }
    
    @Override
    public boolean isAuthorizedForUserPermission(String fragment, String namespace, PermissionLevel level) {
        String uri = ResolveUri(fragment, namespace);
        Optional<UserPermission> permOpt = userPermissionDao.findById(ResolveUri(fragment, namespace));
        if (!permOpt.isPresent()) {
            throw new NotFoundException("User permission with URI '" + uri + "' was not found");
        }
        UserPermission perm = permOpt.get();
        return isAuthorizedForDocumentURI(perm.getDocument().getUri(), level);
    }

    @Override
    public boolean isAuthorizedForGroupPermission(String fragment, String namespace, PermissionLevel level) {
        String uri = ResolveUri(fragment, namespace);
        Optional<GroupPermission> permOpt = groupPermissionDao.findById(ResolveUri(fragment, namespace));
        if (!permOpt.isPresent()) {
            throw new NotFoundException("Group permission with URI '" + uri + "' was not found");
        }
        GroupPermission perm = permOpt.get();
        return isAuthorizedForDocumentURI(perm.getDocument().getUri(), level);
    }
    
}

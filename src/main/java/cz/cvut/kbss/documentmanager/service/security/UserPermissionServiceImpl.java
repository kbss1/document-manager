package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import cz.cvut.kbss.documentmanager.repository.UserPermissionDao;

@Service
public class UserPermissionServiceImpl implements UserPermissionService {
    private final UserPermissionDao permissionDao;

    public UserPermissionServiceImpl(UserPermissionDao permissionDao) {
        this.permissionDao = permissionDao;
    }
    
    @Override
    @Transactional
    public UserPermission findByURI(String uri) {
        Optional<UserPermission> perm = permissionDao.findById(uri);
        if (perm.isPresent()) {
            return perm.get();
        } else {
            throw new NotFoundException("Permission with URI '" + uri + "' was not found");
        }
    }
    
    @Override
    @Transactional
    public UserPermission update(UserPermission permission, String permURI) {
        if (permission.getPermissionLevel() == null) {
            throw new IncorrectRequestException("Permission level is a required field");
        } else if (permission.getUri() == null) {
            throw new IncorrectRequestException("URI is a required field");
        } else if (!permURI.equals(permission.getUri())) {
            throw new IncorrectRequestException("URIs have to match");
        }
        UserPermission permToUpdate = findByURI(permURI);
        permToUpdate.setPermissionLevel(permission.getPermissionLevel());
        permToUpdate = permToUpdate.getDocument().getUserPermissionWithUri(permURI);
        permToUpdate.setPermissionLevel(permission.getPermissionLevel());
        return permToUpdate;
    }

    @Override
    @Transactional
    public void deleteByURI(String uri) {
        if (!permissionDao.existsById(uri)) {
            throw new NotFoundException("Permission with URI '" + uri + "' was not found");
        }
        UserPermission perm = findByURI(uri);
        perm.getDocument().removeUserPermissionByUri(uri);
        permissionDao.deleteById(uri);
        
    }
    
}

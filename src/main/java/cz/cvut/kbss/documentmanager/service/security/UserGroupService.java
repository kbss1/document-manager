package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import java.util.Set;

public interface UserGroupService {
    
    UserGroup findByURI(String docUri);
    
    UserGroup create(UserGroup group);
    
    Set<String> getUsers(String groupUri);
    
    void addUser(String groupUri, String userUri);
    
    void removeUser(String groupUri, String userUri);

    UserGroup update(UserGroup newGroup, String groupUri);

    void deleteByURI(String groupUri);
    
}

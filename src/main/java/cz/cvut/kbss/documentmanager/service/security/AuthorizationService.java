package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;

public interface AuthorizationService {
    
   boolean isAuthorizedForDocument(String fragment, String namespace, PermissionLevel level);
   
   boolean isAuthorizedForFolder(String fragment, String namespace, PermissionLevel level);
   
   boolean isAuthorizedForFile(String fragment, String namespace, PermissionLevel level);
   
   boolean isAuthorizedForUserPermission(String fragment, String namespace, PermissionLevel level);
   
   boolean isAuthorizedForGroupPermission(String fragment, String namespace, PermissionLevel level);
   
}

package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.*;
import cz.cvut.kbss.documentmanager.repository.*;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.isUriValid;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import java.util.Set;

import cz.cvut.kbss.documentmanager.repository.UserGroupDao;
import static cz.cvut.kbss.documentmanager.security.SecurityServiceImpl.getCurrentUserDetails;
import cz.cvut.kbss.documentmanager.util.Constants;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import java.util.stream.Collectors;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.generateUserPermissionURI;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.generateGroupPermissionURI;

@Service
public class DocumentServiceImpl implements DocumentService {
    private final DocumentDao documentDao;
    private final FolderDao folderDao;
    private final UserPermissionDao userPermissionDao;
    private final GroupPermissionDao groupPermissionDao;
    private final UserGroupDao userGroupDao;

    public DocumentServiceImpl(DocumentDao documentDao, FolderDao folderDao,
            UserPermissionDao userPermissionDao,
            GroupPermissionDao groupPermissionDao, UserGroupDao userGroupDao) {
        this.documentDao = documentDao;
        this.folderDao = folderDao;
        this.userPermissionDao = userPermissionDao;
        this.groupPermissionDao = groupPermissionDao;
        this.userGroupDao = userGroupDao;
    }

    @Override
    @Transactional
    public Set<Document> getDocumentList() {
        DocManUserDetails user = getCurrentUserDetails();
        Set<Document> ret = documentDao.findAll().stream()
                .filter(doc -> doc.getPermissionLevelForUser(user.getUserUri()).isPermitted(PermissionLevel.READ))
                .collect(Collectors.toSet());
        return ret;
    }    
    
    @Override
    @Transactional
    public Document findByURI(String uri) {
        Optional<Document> document = documentDao.findById(uri);
        if (document.isPresent()) {
            return document.get();
        } else {
            throw new NotFoundException("Document with URI '" + uri + "' was not found");
        }
    }

    @Override
    @Transactional
    public Set<UserPermission> getUserPermissionsByDocumentURI(String uri) {
        Document document = findByURI(uri);
        return document.getUserPermissions();
    }
    
    @Override
    @Transactional
    public Set<GroupPermission> getGroupPermissionsByDocumentURI(String uri) {
        Document document = findByURI(uri);
        return document.getGroupPermissions();
    }

    @Override
    @Transactional
    public Document create(Document document) {
        if (document.getName() == null || document.getName().isEmpty()) {
            throw new IncorrectRequestException("Name is a required field");
        }
        
        if (document.getUri() == null) {
            document.setUri(UriCreationUtils.generateDocumentURI());
        } else if (!isUriValid(document.getUri())) {
            throw new IncorrectRequestException("URI '" + document.getUri() + "' is not valid");
        }
        
        if (documentDao.existsById(document.getUri())) {
            throw new IncorrectRequestException("Document with such URI already exists");
        }
        
        Folder rootFolder;
        if (document.getRootFolder() != null) {
            rootFolder = document.getRootFolder();            
            if (rootFolder.getName() == null || rootFolder.getName().isEmpty()) {
                throw new IncorrectRequestException("Name is a required field");
            }
            
            if (rootFolder.getUri() == null) {
                rootFolder.setUri(UriCreationUtils.generateFolderURI());
            } else if (!isUriValid(rootFolder.getUri())) {
                throw new IncorrectRequestException("URI '" + rootFolder.getUri() + "' is not valid");
            }
            
            if (folderDao.existsById(rootFolder.getUri())) {
                throw new IncorrectRequestException("Folder with such URI already exists");
            }
        } else {
            rootFolder = new Folder();
            rootFolder.setName(document.getName().replace(" ", "_") + "_root");
            rootFolder.setUri(Vocabulary.c_Folder + "/" + document.getFragment() + "_root");
        }
        rootFolder.setIsRoot(true);
        rootFolder.setDocument(document);
        rootFolder.initDates();
        
        document.initDates();
        document.setRootFolder(rootFolder);
        
        DocManUserDetails user = getCurrentUserDetails();
        UserPermission docPerm = new UserPermission();
        docPerm.setPermissionLevel(Constants.MAX_PERM_LEVEL);
        docPerm.setUserURI(user.getUserUri());
        docPerm.setUri(generateUserPermissionURI(document.getFragment(), user.getUserUri()));
        
        UserPermission dbPerm = userPermissionDao.save(docPerm);
        dbPerm.setDocument(document);
        document.addUserPermission(dbPerm);
        return documentDao.save(document);
    }
    
    @Override
    @Transactional
    public UserPermission createUserPermission(UserPermission permission, String docUri) {
        if (permission.getUserURI() == null) {
            throw new IncorrectRequestException("User URI is a required field");
        } else if (permission.getPermissionLevel() == null) {
            throw new IncorrectRequestException("Permission level is a required field");
        } else if (!isUriValid(permission.getUserURI())) {
            throw new IncorrectRequestException("URI '" + permission.getUserURI() + "' is not valid");
        }
        Document doc = findByURI(docUri);
        String newPermUri = generateUserPermissionURI(doc.getFragment(), permission.getUserURI());
        UserPermission testExistsPermission = doc.getUserPermissionWithUri(newPermUri);
        if (testExistsPermission != null) {
            throw new IncorrectRequestException("Permission for given user already exists in given document");
        }
        permission.setUri(newPermUri);
        permission.setDocument(doc);
        UserPermission dbPerm = userPermissionDao.save(permission);
        doc.addUserPermission(dbPerm);
        return dbPerm;
    }
    
    @Override
    @Transactional
    public GroupPermission createGroupPermission(GroupPermission permission, String docUri) {
        if (permission.getPermissionLevel() == null) {
            throw new IncorrectRequestException("Permission level is a required field");
        } else if (permission.getGroup() == null) {
            throw new IncorrectRequestException("Permission needs to be assigned to a group");
        }
        String groupUri = permission.getGroup().getUri();
        Optional<UserGroup> groupOpt = userGroupDao.findById(groupUri);
        if (!groupOpt.isPresent()) {
            throw new NotFoundException("Group with URI '" + groupUri + "' was not found");
        }
        Document doc = findByURI(docUri);
        UserGroup group = groupOpt.get();
        String newPermUri = generateGroupPermissionURI(doc.getFragment(), group.getUri());
        GroupPermission testExistsPermission = group.getGroupPermissionWithUri(newPermUri);
        if (testExistsPermission != null) {
            throw new IncorrectRequestException("Permission for given group already exists in given document");
        }
        permission.setUri(newPermUri);
        permission.setDocument(doc);
        permission.setGroup(group);
        GroupPermission dbPerm = groupPermissionDao.save(permission);
        doc.addGroupPermission(dbPerm);
        group.addPermission(dbPerm);
        return dbPerm;
    }

    @Override
    @Transactional
    public Document update(Document newDocument, String uri) {
        if (newDocument.getName() == null || newDocument.getName().isEmpty()) {
            throw new IncorrectRequestException("Name is a required field");
        } else if (!newDocument.getUri().equals(uri)) {
            throw new IncorrectRequestException("URIs have to match");
        }
        Document document = findByURI(uri);
        document.setName(newDocument.getName());
        document.setDescription(newDocument.getDescription());
        document.updateLastModified();
        return documentDao.save(document);
    }

    @Override
    @Transactional
    public void deleteByURI(String uri) {
        if (!documentDao.existsById(uri)) {
            throw new NotFoundException("Document with URI '" + uri + "' was not found");
        }
        Document doc = findByURI(uri);
        if (doc.getGroupPermissions() != null) {
            for (GroupPermission perm : doc.getGroupPermissions()) {
                perm.getGroup().removePermissionByUri(perm.getUri());
            }
        }
        
        documentDao.deleteById(uri);
    }
    
}

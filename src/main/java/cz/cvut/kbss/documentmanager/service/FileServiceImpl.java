package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.FileMemento;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.repository.FileDao;
import cz.cvut.kbss.documentmanager.repository.FileStorageDao;
import cz.cvut.kbss.documentmanager.repository.FolderDao;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.isUriValid;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import java.util.Set;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileServiceImpl implements FileService {
    private final FileDao fileDao;
    private final FolderDao folderDao;
    private final FileStorageDao fileStorageDao;

    public FileServiceImpl(FileDao fileDao, FolderDao folderDao, FileStorageDao fileStorageDao) {
        this.fileDao = fileDao;
        this.folderDao = folderDao;
        this.fileStorageDao = fileStorageDao;
    }
    
    @Override
    @Transactional
    public File findByURI(String uri) {
        Optional<File> file = fileDao.findById(uri);
        if (file.isPresent()) {
            return file.get();
        } else {
            throw new NotFoundException("File with URI '" + uri + "' was not found");
        }
    }
    
    @Override
    public FileMemento getFileMemento(String uri, long version) {
        return findByURI(uri).getMementoVersion(version);
    }
    
    @Override
    public Set<FileMemento> getAllFileMementos(String uri) {
        return findByURI(uri).getVersions();
    }
    

    @Override
    public Resource getFileContent(String fileURI, Long version) {
        File dbFile = findByURI(fileURI);
        if (version == null) {
            version = dbFile.getVersion();
        }
        return fileStorageDao.loadFileAsResource(Paths.get(dbFile.getStoragePath(version) + "/" + dbFile.getMementoVersion(version).getFileName()));
    }
        
    @Override
    @Transactional
    public File uploadFile(MultipartFile file, String fileURI, String nameOfFile , String folderURI) {
        if (file.isEmpty()) {
            throw new IncorrectRequestException("Empty file submitted");
        } else if (nameOfFile == null || nameOfFile.isEmpty()) {
            throw new IncorrectRequestException("Name is a required field");
        }
        
        if (fileURI == null) {
            fileURI = UriCreationUtils.generateFileURI();
        } else if (!isUriValid(fileURI)) {
            throw new IncorrectRequestException("URI '" + fileURI + "' is not valid");
        }
        
        if (fileDao.existsById(fileURI)) {
            throw new IncorrectRequestException("File with such URI already exists");
        }
        
        Optional<Folder> uploadFolderOpt = folderDao.findById(folderURI);
        if (!uploadFolderOpt.isPresent()) {
            throw new NotFoundException("Folder with URI '" + folderURI + "' was not found");
        }
        Folder uploadFolder = uploadFolderOpt.get();
        
        File newFile = new File();
        newFile.setName(nameOfFile);
        newFile.setFileName(file.getOriginalFilename());
        newFile.setUri(fileURI);
        newFile.setFileSize(file.getSize());
        newFile.setFileType(file.getContentType());
        newFile.setVersion(0L);
        newFile.initDates();
        newFile.setLocation(uploadFolder);
        
        FileMemento memento = new FileMemento();
        memento.setFromFile(newFile);
        memento.generateUriFromFragment(newFile.getFragment());
        newFile.saveMemento(memento);
        
        Path storagePathExistsCheck = newFile.getStoragePath();
        if (fileStorageDao.fileFolderExists(storagePathExistsCheck)) {
            fileStorageDao.deleteFileByFolderPath(storagePathExistsCheck);
        }
        Path storagePath = newFile.getStoragePath(0L);
        fileStorageDao.storeFileInPath(file, storagePath);
        uploadFolder.addFile(newFile);
        folderDao.save(uploadFolder);
        return fileDao.save(newFile);
    }

    @Override
    @Transactional
    public File updateFileInfo(File newFile, String fileURI) {        
        if (newFile.getName() == null || newFile.getName().isEmpty()) {
            throw new IncorrectRequestException("Name is a required field");
        }
        if (newFile.getUri() == null || !newFile.getUri().equals(fileURI)) {
            throw new IncorrectRequestException("URIs have to match");
        }
        File file = findByURI(fileURI);
        file.setName(newFile.getName());
        return fileDao.save(file);
    }

    @Override
    @Transactional
    public File updateFileContent(MultipartFile newFile, String fileURI) {
        if (newFile.isEmpty()) {
            throw new IncorrectRequestException("Empty file submitted");
        }
        File file = findByURI(fileURI);
        file.setFileName(newFile.getOriginalFilename());
        file.setFileType(newFile.getContentType());
        file.setFileSize(newFile.getSize());
        file.incrementVersion();
        file.updateLastModified();
        
        FileMemento newMemento = new FileMemento();
        newMemento.setFromFile(file);
        newMemento.generateUriFromFragment(file.getFragment());
        file.saveMemento(newMemento);
        
        Path storagePath = file.getStoragePath(file.getVersion());
        fileStorageDao.storeFileInPath(newFile, storagePath);
        return fileDao.save(file);
    }
    
    @Override
    @Transactional
    public void deleteByURI(String uri) {
        File file = findByURI(uri);
        file.getLocation().removeFile(file);
        fileDao.deleteById(file.getUri());
        fileStorageDao.deleteFileByFolderPath(file.getStoragePath());
    }
    
    @Override
    @Scheduled(cron = "${maintenance.lonely-files-cleanup}")
    @Transactional
    public void deleteLonelyFiles() {
        List<File> files = new LinkedList(fileDao.findAll());
        List<String> filePaths = new LinkedList();
        //Remove all entities in the databse that are not stored in the file system
        for (File dbFile : files) {
            Path storagePath = dbFile.getStoragePath();
            if (!fileStorageDao.fileFolderExists(storagePath)) {
                fileDao.deleteById(dbFile.getUri());
            }
            filePaths.add(storagePath.toString());
        }
        //Remove all folders in the file system that are not stored in the database
        List<String> folderPaths = fileStorageDao.getStorageFileFolders();
        for (String folderPath : folderPaths) {
            if (!filePaths.contains(folderPath)) {
                fileStorageDao.deleteFileByFolderPath(Paths.get(folderPath));
            }
        }
    }
    
}

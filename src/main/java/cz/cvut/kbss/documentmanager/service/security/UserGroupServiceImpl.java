package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.isUriValid;
import java.util.Optional;
import java.util.Set;
import cz.cvut.kbss.documentmanager.repository.UserGroupDao;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;

@Service
public class UserGroupServiceImpl implements UserGroupService {
    private final UserGroupDao userGroupDao;

    public UserGroupServiceImpl(UserGroupDao userGroupDao) {
        this.userGroupDao = userGroupDao;
    }

    @Override
    @Transactional
    public UserGroup findByURI(String uri) {
        Optional<UserGroup> document = userGroupDao.findById(uri);
        if (document.isPresent()) {
            return document.get();
        } else {
            throw new NotFoundException("Group with URI '" + uri + "' was not found");
        }
    }

    @Override
    @Transactional
    public Set<String> getUsers(String groupURI) {
        UserGroup group = findByURI(groupURI);
        return group.getUsers();
    }

    @Override
    @Transactional
    public void addUser(String groupURI, String userURI) {
        UserGroup group = findByURI(groupURI);
        if (!isUriValid(userURI)) {
            throw new IncorrectRequestException("URI '" + userURI + "' is not valid");
        }
        group.addUser(userURI);
    }

    @Override
    @Transactional
    public void removeUser(String groupURI, String userURI) {
        UserGroup group = findByURI(groupURI);
        if (!group.hasUser(userURI)) {
            throw new NotFoundException("User with URI '" + userURI + "' was not found");
        }
        group.removeUser(userURI);
    }

    @Override
    @Transactional
    public UserGroup create(UserGroup group) {
        if (group.getName() == null || group.getName().isEmpty()) {
            throw new IncorrectRequestException("Name is a required field");
        }
        
        if (group.getUri() == null) {
            group.setUri(UriCreationUtils.generateUserGroupURI());
        } else if (!isUriValid(group.getUri())) {
            throw new IncorrectRequestException("URI '" + group.getUri() + "' is not valid");
        }
        
        if (userGroupDao.existsById(group.getUri())) {
            throw new IncorrectRequestException("Group with such URI already exists");
        }
        return userGroupDao.save(group);
    }
    
    @Override
    @Transactional
    public UserGroup update(UserGroup newGroup, String uri) {
        if (newGroup.getName() == null || newGroup.getName().isEmpty()) {
            throw new IncorrectRequestException("Name is a required field");
        } else if (newGroup.getUri() == null) {
            throw new IncorrectRequestException("URI is a required field");
        } else if (!newGroup.getUri().equals(uri)) {
            throw new IncorrectRequestException("URIs have to match");
        }
        UserGroup group = findByURI(uri);
        group.setName(newGroup.getName());
        return userGroupDao.save(group);
    }

    @Override
    @Transactional
    public void deleteByURI(String uri) {
        if (!userGroupDao.existsById(uri)) {
            throw new NotFoundException("Group with URI '" + uri + "' was not found");
        }
        UserGroup group = findByURI(uri);        
        for (GroupPermission perm : group.getPermissions()) {
            perm.getDocument().removeGroupPermissionByUri(perm.getUri());            
        }
        userGroupDao.deleteById(uri);
    }
    
}

package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.model.security.UserPermission;

public interface UserPermissionService {
   
   UserPermission findByURI(String permUri);
   
   public UserPermission update(UserPermission permission, String permURI);
   
   void deleteByURI(String uri);
   
}
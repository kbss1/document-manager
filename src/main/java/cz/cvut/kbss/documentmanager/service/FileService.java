package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.FileMemento;
import java.util.Set;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    
    File findByURI(String uri);
    
    FileMemento getFileMemento(String uri, long version);
    
    Set<FileMemento> getAllFileMementos(String uri);

    Resource getFileContent(String fileURI, Long version);
    
    File uploadFile(MultipartFile file, String fileURI, String nameOfFile, String folderURI);
    
    File updateFileInfo(File file, String fileURI);
    
    File updateFileContent(MultipartFile newFile, String fileURI);
    
    void deleteByURI(String uri);

    public void deleteLonelyFiles();

}

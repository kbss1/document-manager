package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.repository.FolderDao;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.isUriValid;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import java.util.Set;

@Service
public class FolderServiceImpl implements FolderService {
    private final FolderDao folderDao;

    public FolderServiceImpl(FolderDao folderDao) {
        this.folderDao = folderDao;
    }
    
    @Override
    @Transactional
    public Folder findByURI(String uri) {
        Optional<Folder> folder = folderDao.findById(uri);
        if (folder.isPresent()) {
            return folder.get();
        } else {
            throw new NotFoundException("Folder with URI '" + uri + "' was not found");
        }
    }
    
    @Override
    @Transactional
    public Set<Folder> getAllFolderSubfolders(String uri) {
        Folder folder = findByURI(uri);
        return folder.getFolders();
    }
    
    @Override
    @Transactional
    public Set<File> getAllFolderFiles(String uri) {
        Folder folder = findByURI(uri);
        return folder.getFiles();
    }
    
    @Override
    @Transactional
    public Folder createSubFolder(Folder folder, String parentFolderURI) {
        if (folder.getName() == null || folder.getName().isEmpty()) {
            throw new IncorrectRequestException("Name is a required field");
        }
        
        if (folder.getUri() == null) {
            folder.setUri(UriCreationUtils.generateFolderURI());
        } else if (!isUriValid(folder.getUri())) {
            throw new IncorrectRequestException("URI '" + folder.getUri() + "' is not valid");
        }
        
        if (folderDao.existsById(folder.getUri())) {
            throw new IncorrectRequestException("Folder with such URI already exists");
        }
        
        Folder parentFolder = findByURI(parentFolderURI);
        folder.initDates();
        folder.setParentFolder(parentFolder);
        folder.setDocument(parentFolder.getDocument());
        folder.setIsRoot(false);
        
        parentFolder.addSubfolder(folder);
        folderDao.save(parentFolder);
        return folderDao.save(folder);
    }

    @Override
    @Transactional
    public Folder update(Folder newFolder, String uri) {
        if (newFolder.getName() == null || newFolder.getName().isEmpty()) {
            throw new IncorrectRequestException("Name is a required field");
        } else if (!newFolder.getUri().equals(uri)) {
            throw new IncorrectRequestException("URIs have to match");
        }
        Folder folder = findByURI(uri);
        folder.setName(newFolder.getName());
        folder.updateLastModified();
        folder.setDescription(newFolder.getDescription());
        return folderDao.save(folder);
    }
    
    @Override
    @Transactional
    public void deleteByURI(String uri) {
        Folder folderToDelete = this.findByURI(uri);
        if (folderToDelete.isRoot()) {
            throw new IncorrectRequestException("Root folder cannot be deleted");
        }
        folderToDelete.getParentFolder().removeSubfolder(folderToDelete);
        folderDao.deleteById(uri);
    }

}

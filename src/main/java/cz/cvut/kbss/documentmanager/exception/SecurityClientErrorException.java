package cz.cvut.kbss.documentmanager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class SecurityClientErrorException extends RuntimeException {
    
    public SecurityClientErrorException(String message) {
        super(message);
    }
    
}

package cz.cvut.kbss.documentmanager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class SecurityServerErrorException extends RuntimeException {
    
    public SecurityServerErrorException(String message) {
        super(message);
    }
    
}

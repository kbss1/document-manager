package cz.cvut.kbss.documentmanager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IncorrectRequestException extends RuntimeException {
    
    public IncorrectRequestException(String message) {
        super(message);
    }
    
    public IncorrectRequestException(String message, Throwable cause) {
        super(message, cause);
    }
    
}

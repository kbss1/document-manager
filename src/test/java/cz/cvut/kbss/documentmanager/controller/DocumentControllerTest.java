package cz.cvut.kbss.documentmanager.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.cvut.kbss.documentmanager.environment.Environment;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.TestSecurityInterceptor;
import cz.cvut.kbss.documentmanager.environment.conf.TestRestSecurityConfig;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.*;
import cz.cvut.kbss.documentmanager.service.DocumentService;
import cz.cvut.kbss.documentmanager.service.security.AuthorizationService;
import java.util.HashSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import javax.servlet.Filter;
import java.util.List;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestRestSecurityConfig.class, DocumentControllerTest.Config.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
class DocumentControllerTest extends BaseControllerTestRunner {
    
    private static final String PATH = "/documents";
    
    @Autowired
    private Filter springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext context;
    
    @Autowired
    private DocumentService ds;
    
    @Autowired
    private AuthorizationService as;
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setupObjectMappers();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity(springSecurityFilterChain)).build();
    }
    
    @EnableWebMvc
    @Configuration
    public static class Config implements WebMvcConfigurer {
        @Mock
        TestSecurityInterceptor tsi;
        
        @Mock
        private DocumentService ds;
        
        final private AuthorizationService as;

        @InjectMocks
        private DocumentController dc;

        Config() {
            MockitoAnnotations.initMocks(this);
            as = mock(AuthorizationService.class, (Answer)(InvocationOnMock iom) -> false);
        }
        
        @Bean
        public DocumentService documentService() {
            return ds;
        }

        @Bean
        public DocumentController documentController() {
            return dc;
        }
        
        @Bean
        public AuthorizationService authorizationService() {
            return as;
        }
        
        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(Environment.createJsonLdMessageConverter());
            converters.add(Environment.createDefaultMessageConverter());
            converters.add(Environment.createStringEncodingMessageConverter());
        }
    }
    
    @Test
    void getDocumentListTest() throws Exception {        
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        
        final Document doc1 = Generator.generateDocument();
        final Document doc2 = Generator.generateDocument();
        Set<Document> docList = new HashSet<>();
        docList.add(doc1);
        docList.add(doc2);
        when(ds.getDocumentList()).thenReturn(docList);
        
        final MvcResult mvcResult = mockMvc.perform(get(PATH).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk()).andReturn();
        final List<Document> result = readValue(mvcResult, new TypeReference<List<Document>>() {});
        assertEquals(2, result.size());
        assert(result.contains(doc1));
        assert(result.contains(doc2));
        verify(ds, times(1)).getDocumentList();
    }
    
    @Test
    void getDocumentTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Document doc = Generator.generateDocument();
        when(ds.findByURI(doc.getUri())).thenReturn(doc);
        
        String path = PATH + "/" + doc.getFragment();        
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isForbidden());
        
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk()).andReturn();
        Document result = readValue(mvcResult, Document.class);
        assertEquals(doc.getUri(), result.getUri());
        assertEquals(doc.getName(), result.getName());
        assertEquals(doc.getRootFolder().getUri(), result.getRootFolder().getUri());
        
        when(as.isAuthorizedForDocument(doc.getFragment(), Vocabulary.c_Document, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, Document.class);
        assertEquals(doc.getUri(), result.getUri());
        assertEquals(doc.getName(), result.getName());
        assertEquals(doc.getRootFolder().getUri(), result.getRootFolder().getUri());
        verify(ds, times(2)).findByURI(any());
    }
    
    @Test
    void getDocumentUserPermissionsTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Document doc = Generator.generateDocument();
        final UserPermission perm = Generator.generateUserPermission();
        doc.addUserPermission(perm);
        when(ds.getUserPermissionsByDocumentURI(doc.getUri())).thenReturn(doc.getUserPermissions());
        
        String path = PATH + "/" + doc.getFragment() + "/permissions/user";
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk()).andReturn();
        List<UserPermission> result = readValue(mvcResult, new TypeReference<List<UserPermission>>() {});
        assertEquals(1, result.size());
        assert(result.contains(perm));
        
        when(as.isAuthorizedForDocument(doc.getFragment(), Vocabulary.c_Document, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk()).andReturn();
        
        result = readValue(mvcResult, new TypeReference<List<UserPermission>>() {});
        assertEquals(1, result.size());
        assert(result.contains(perm));
        
        verify(ds, times(2)).getUserPermissionsByDocumentURI(any());
    }
    
    @Test
    void getDocumentGroupPermissionsTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Document doc = Generator.generateDocument();
        final GroupPermission perm = Generator.generateGroupPermission();
        doc.addGroupPermission(perm);
        when(ds.getGroupPermissionsByDocumentURI(doc.getUri())).thenReturn(doc.getGroupPermissions());
        
        String path = PATH + "/" + doc.getFragment() + "/permissions/group";
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk()).andReturn();
        List<GroupPermission> result = readValue(mvcResult, new TypeReference<List<GroupPermission>>() {});
        assertEquals(1, result.size());
        assert(result.contains(perm));
        
        when(as.isAuthorizedForDocument(doc.getFragment(), Vocabulary.c_Document, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk()).andReturn();
        
        result = readValue(mvcResult, new TypeReference<List<GroupPermission>>() {});
        assertEquals(1, result.size());
        assert(result.contains(perm));
        
        verify(ds, times(2)).getGroupPermissionsByDocumentURI(any());
    }
    @Test
    void createDocumentTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        Document doc = Generator.generateDocument();
        
        mockMvc.perform(post(PATH).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(doc))
            .param("namespace", Vocabulary.c_Document)).andExpect(status().isOk());
        mockMvc.perform(post(PATH).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(doc))
            .param("namespace", Vocabulary.c_Document)).andExpect(status().isOk());
        verify(ds, times(2)).create(any());
    }
    
    @Test
    void createUserPermissionTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Document doc = Generator.generateDocument();
        final UserPermission perm = Generator.generateUserPermission();
        
        String path = PATH + "/" + doc.getFragment() + "/permissions/user";
        mockMvc.perform(post(path).with(user(userDetails)).content(toJson(perm))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isForbidden());
        mockMvc.perform(post(path).with(user(adminDetails)).content(toJson(perm))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk());
        
        when(as.isAuthorizedForDocument(doc.getFragment(), Vocabulary.c_Document, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(post(path).with(user(userDetails)).content(toJson(perm))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk());
        
        verify(ds, times(2)).createUserPermission(any(), any());
    }
    
    @Test
    void createGroupPermissionTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Document doc = Generator.generateDocument();
        final GroupPermission perm = Generator.generateGroupPermission();
        
        String path = PATH + "/" + doc.getFragment() + "/permissions/group";
        mockMvc.perform(post(path).with(user(userDetails)).content(toJson(perm))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isForbidden());
        mockMvc.perform(post(path).with(user(adminDetails)).content(toJson(perm))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk());
        
        when(as.isAuthorizedForDocument(doc.getFragment(), Vocabulary.c_Document, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(post(path).with(user(userDetails)).content(toJson(perm))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk());
        
        verify(ds, times(2)).createGroupPermission(any(), any());
    }
    
    @Test
    void updateDocumentTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Document doc = Generator.generateDocument();
        
        String path = PATH + "/" + doc.getFragment();
        mockMvc.perform(put(path).with(user(userDetails)).content(toJson(doc))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isForbidden());
        mockMvc.perform(put(path).with(user(adminDetails)).content(toJson(doc))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk());
        
        when(as.isAuthorizedForDocument(doc.getFragment(), Vocabulary.c_Document, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(put(path).with(user(userDetails)).content(toJson(doc))
            .contentType(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk());
        
        verify(ds, times(2)).update(any(), any());
    }
    
    @Test
    void deleteDocumentTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Document doc = Generator.generateDocument();
        
        String path = PATH + "/" + doc.getFragment();
        mockMvc.perform(delete(path).with(user(userDetails)).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isForbidden());
        mockMvc.perform(delete(path).with(user(adminDetails)).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk());
        
        when(as.isAuthorizedForDocument(doc.getFragment(), Vocabulary.c_Document, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(delete(path).with(user(userDetails)).param("namespace", Vocabulary.c_Document))
            .andExpect(status().isOk());
        
        verify(ds, times(2)).deleteByURI(any());
    }
   
}
package cz.cvut.kbss.documentmanager.controller;

import cz.cvut.kbss.documentmanager.environment.Environment;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.conf.TestRestSecurityConfig;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.*;
import cz.cvut.kbss.documentmanager.service.security.AuthorizationService;
import cz.cvut.kbss.documentmanager.service.security.UserPermissionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import javax.servlet.Filter;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestRestSecurityConfig.class, UserPermissionControllerTest.Config.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
class UserPermissionControllerTest extends BaseControllerTestRunner {
    
    private static final String PATH = "/permissions/user";
    
    @Autowired
    private Filter springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext context;
    
    @Autowired
    private UserPermissionService ups;
    
    @Autowired
    private AuthorizationService as;
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setupObjectMappers();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity(springSecurityFilterChain)).build();
    }
    
    @EnableWebMvc
    @Configuration
    public static class Config implements WebMvcConfigurer {
        @Mock
        private UserPermissionService ups;
        
        final private AuthorizationService as;

        @InjectMocks
        private UserPermissionController upc;

        Config() {
            MockitoAnnotations.initMocks(this);
            as = mock(AuthorizationService.class, (Answer)(InvocationOnMock iom) -> false);
        }
        
        @Bean
        public UserPermissionService userPermissionService() {
            return ups;
        }

        @Bean
        public UserPermissionController userPermissionController() {
            return upc;
        }
        
        @Bean
        public AuthorizationService authorizationService() {
            return as;
        }
        
        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(Environment.createJsonLdMessageConverter());
            converters.add(Environment.createDefaultMessageConverter());
            converters.add(Environment.createStringEncodingMessageConverter());
        }
    }
    
    @Test
    void getUserPermissionTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        UserPermission perm = Generator.generateUserPermission();
        when(ups.findByURI(perm.getUri())).thenReturn(perm);
        
        String path = PATH + "/" + perm.getFragment();
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_UserPermission))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_UserPermission))
            .andExpect(status().isOk()).andReturn();
        UserPermission result = readValue(mvcResult, UserPermission.class);
        assertEquals(perm.getUri(), result.getUri());
        assertEquals(perm.getPermissionLevel(), result.getPermissionLevel());
        
        when(as.isAuthorizedForUserPermission(perm.getFragment(), Vocabulary.c_UserPermission, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_UserPermission))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, UserPermission.class);
        assertEquals(perm.getUri(), result.getUri());
        
        verify(ups, times(2)).findByURI(perm.getUri());
    }
    
    @Test
    void updateUserPermissionTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final UserPermission perm = Generator.generateUserPermission();
        
        String path = PATH + "/" + perm.getFragment();
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(perm))
            .param("namespace", Vocabulary.c_UserPermission)).andExpect(status().isForbidden());
        mockMvc.perform(put(path).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(perm))
            .param("namespace", Vocabulary.c_UserPermission)).andExpect(status().isOk());
        
        when(as.isAuthorizedForUserPermission(perm.getFragment(), Vocabulary.c_UserPermission, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(perm))
            .param("namespace", Vocabulary.c_UserPermission)).andExpect(status().isOk());
        verify(ups, times(2)).update(any(), eq(perm.getUri()));
    }
    
    @Test
    void deleteFolderTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final UserPermission perm = Generator.generateUserPermission();
        
        String path = PATH + "/" + perm.getFragment();
        mockMvc.perform(delete(path).with(user(userDetails))
            .param("namespace", Vocabulary.c_UserPermission)).andExpect(status().isForbidden());
        mockMvc.perform(delete(path).with(user(adminDetails))
            .param("namespace", Vocabulary.c_UserPermission)).andExpect(status().isOk());
        
        when(as.isAuthorizedForUserPermission(perm.getFragment(), Vocabulary.c_UserPermission, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(delete(path).with(user(userDetails))
            .param("namespace", Vocabulary.c_UserPermission)).andExpect(status().isOk());
        verify(ups, times(2)).deleteByURI(perm.getUri());
    }
    
}
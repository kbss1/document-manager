package cz.cvut.kbss.documentmanager.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.kbss.documentmanager.environment.Environment;
import static cz.cvut.kbss.documentmanager.environment.Environment.createDefaultMessageConverter;
import static cz.cvut.kbss.documentmanager.environment.Environment.createJsonLdMessageConverter;
import static cz.cvut.kbss.documentmanager.environment.Environment.createResourceMessageConverter;
import static cz.cvut.kbss.documentmanager.environment.Environment.createStringEncodingMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.accept.ContentNegotiationManager;


public class BaseControllerTestRunner {

    ObjectMapper objectMapper;

    ObjectMapper jsonLdObjectMapper;

    MockMvc mockMvc;

    void setUp(Object controller) {
        setupObjectMappers();
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller)
            .setMessageConverters(createJsonLdMessageConverter(), createDefaultMessageConverter(),
                createStringEncodingMessageConverter(), createResourceMessageConverter())
            .setUseSuffixPatternMatch(false)
            .setContentNegotiationManager(new ContentNegotiationManager()).build();
    }

    void setupObjectMappers() {
        this.objectMapper = Environment.getObjectMapper();
        this.jsonLdObjectMapper = Environment.getJsonLdObjectMapper();
    }

    String toJson(Object object) throws Exception {
        return objectMapper.writeValueAsString(object);
    }

    String toJsonLd(Object object) throws Exception {
        return jsonLdObjectMapper.writeValueAsString(object);
    }

    <T> T readValue(MvcResult result, Class<T> targetType) throws Exception {
        return objectMapper.readValue(result.getResponse().getContentAsByteArray(), targetType);
    }

    <T> T readValue(MvcResult result, TypeReference<T> targetType) throws Exception {
        return objectMapper.readValue(result.getResponse().getContentAsByteArray(), targetType);
    }
    
}

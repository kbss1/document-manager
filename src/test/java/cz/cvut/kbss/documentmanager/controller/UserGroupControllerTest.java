package cz.cvut.kbss.documentmanager.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.cvut.kbss.documentmanager.environment.Environment;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.conf.TestRestSecurityConfig;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.*;
import cz.cvut.kbss.documentmanager.service.security.UserGroupService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import javax.servlet.Filter;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestRestSecurityConfig.class, UserGroupControllerTest.Config.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
class UserGroupControllerTest extends BaseControllerTestRunner {
    
    private static final String PATH = "/groups";
    
    @Autowired
    private Filter springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext context;
    
    @Autowired
    private UserGroupService ugs;
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setupObjectMappers();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity(springSecurityFilterChain)).build();
    }
    
    @EnableWebMvc
    @Configuration
    public static class Config implements WebMvcConfigurer {
        @Mock
        private UserGroupService ugs;

        @InjectMocks
        private UserGroupController usc;

        Config() {
            MockitoAnnotations.initMocks(this);
        }
        
        @Bean
        public UserGroupService userGroupService() {
            return ugs;
        }

        @Bean
        public UserGroupController userGroupController() {
            return usc;
        }
        
        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(Environment.createJsonLdMessageConverter());
            converters.add(Environment.createDefaultMessageConverter());
            converters.add(Environment.createStringEncodingMessageConverter());
        }
    }
    
    @Test
    void getUserGroupTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        UserGroup group = Generator.generateUserGroup();
        when(ugs.findByURI(group.getUri())).thenReturn(group);
        
        String path = PATH + "/" + group.getFragment();
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_UserGroup))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_UserGroup))
            .andExpect(status().isOk()).andReturn();
        UserGroup result = readValue(mvcResult, UserGroup.class);
        assertEquals(group.getUri(), result.getUri());
        assertEquals(group.getName(), result.getName());
        
        verify(ugs, times(1)).findByURI(group.getUri());
    }
    
    @Test
    void getUserGroupUsersTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        UserGroup group = Generator.generateUserGroup();
        String userURI1 = Generator.generateUserURI();
        String userURI2 = Generator.generateUserURI();
        group.addUser(userURI1);
        group.addUser(userURI2);
        when(ugs.getUsers(group.getUri())).thenReturn(group.getUsers());
        
        String path = PATH + "/" + group.getFragment() + "/users";
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_UserGroup))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_UserGroup))
            .andExpect(status().isOk()).andReturn();
        final List<String> result = readValue(mvcResult, new TypeReference<List<String>>() {});
        assertEquals(2, result.size());
        assert(result.contains(userURI1));
        assert(result.contains(userURI2));        
        
        verify(ugs, times(1)).getUsers(group.getUri());
    }
    
    @Test
    void createUserGroupTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final UserGroup group = Generator.generateUserGroup();
        
        String path = PATH + "/" + group.getFragment();
        mockMvc.perform(post(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(group))
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isForbidden());
        mockMvc.perform(post(path).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(group))
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isOk());
        
        verify(ugs, times(1)).create(any());
    }
    
    @Test
    void addUserToUserGroupTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final UserGroup group = Generator.generateUserGroup();
        String userURI = Generator.generateUserURI();
        
        String path = PATH + "/" + group.getFragment() + "/users";
        mockMvc.perform(post(path).with(user(userDetails)).content(userURI)
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isForbidden());
        mockMvc.perform(post(path).with(user(adminDetails)).content(userURI)
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isOk());
        
        verify(ugs, times(1)).addUser(group.getUri(), userURI);
    }
    
    @Test
    void updateUserGroupTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final UserGroup group = Generator.generateUserGroup();
        
        String path = PATH + "/" + group.getFragment();
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(group))
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isForbidden());
        mockMvc.perform(put(path).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(group))
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isOk());
        
        verify(ugs, times(1)).update(group, group.getUri());
    }
    
    @Test
    void deleteUserGroupTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final UserGroup group = Generator.generateUserGroup();
        
        String path = PATH + "/" + group.getFragment();
        mockMvc.perform(delete(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isForbidden());
        mockMvc.perform(delete(path).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isOk());
        
        verify(ugs, times(1)).deleteByURI(group.getUri());
    }
    
    @Test
    void removeUserFromUserGroupTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final UserGroup group = Generator.generateUserGroup();
        String userURI = Generator.generateUserURI();
        
        String path = PATH + "/" + group.getFragment() + "/users";
        mockMvc.perform(delete(path).with(user(userDetails)).content(userURI)
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isForbidden());
        mockMvc.perform(delete(path).with(user(adminDetails)).content(userURI)
            .param("namespace", Vocabulary.c_UserGroup)).andExpect(status().isOk());
        
        verify(ugs, times(1)).removeUser(group.getUri(), userURI);
    }
    
}
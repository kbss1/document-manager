package cz.cvut.kbss.documentmanager.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.cvut.kbss.documentmanager.environment.Environment;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.conf.TestRestSecurityConfig;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.*;
import cz.cvut.kbss.documentmanager.service.FolderService;
import cz.cvut.kbss.documentmanager.service.security.AuthorizationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import javax.servlet.Filter;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestRestSecurityConfig.class, FolderControllerTest.Config.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
class FolderControllerTest extends BaseControllerTestRunner {
    
    private static final String PATH = "/folders";
    
    @Autowired
    private Filter springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext context;
    
    @Autowired
    private FolderService fs;
    
    @Autowired
    private AuthorizationService as;
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setupObjectMappers();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity(springSecurityFilterChain)).build();
    }
    
    @EnableWebMvc
    @Configuration
    public static class Config implements WebMvcConfigurer {
        @Mock
        private FolderService fs;
        
        final private AuthorizationService as;

        @InjectMocks
        private FolderController fc;

        Config() {
            MockitoAnnotations.initMocks(this);
            as = mock(AuthorizationService.class, (Answer)(InvocationOnMock iom) -> false);
        }
        
        @Bean
        public FolderService folderService() {
            return fs;
        }

        @Bean
        public FolderController folderController() {
            return fc;
        }
        
        @Bean
        public AuthorizationService authorizationService() {
            return as;
        }
        
        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(Environment.createJsonLdMessageConverter());
            converters.add(Environment.createDefaultMessageConverter());
            converters.add(Environment.createStringEncodingMessageConverter());
        }
    }
    
    @Test
    void getFolderTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        Folder folder = Generator.generateFolder();
        when(fs.findByURI(folder.getUri())).thenReturn(folder);
        
        String path = PATH + "/" + folder.getFragment();
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isOk()).andReturn();
        Folder result = readValue(mvcResult, Folder.class);
        assertEquals(folder.getUri(), result.getUri());
        assertEquals(folder.getName(), result.getName());
        
        when(as.isAuthorizedForFolder(folder.getFragment(), Vocabulary.c_Folder, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, Folder.class);
        assertEquals(folder.getUri(), result.getUri());
        assertEquals(folder.getName(), result.getName());
        
        verify(fs, times(2)).findByURI(folder.getUri());
    }
    
    @Test
    void getFolderFilesTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Folder folder = Generator.generateFolder();
        final File file1 = Generator.generateDummyFileInfo();
        final File file2 = Generator.generateDummyFileInfo();
        folder.addFile(file1);
        folder.addFile(file2);
        when(fs.getAllFolderFiles(folder.getUri())) .thenReturn(folder.getFiles());
        
        String path = PATH + "/" + folder.getFragment() + "/files";
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isOk()).andReturn();
        List<File> result = readValue(mvcResult, new TypeReference<List<File>>() {});
        assertEquals(2, result.size());
        assert(result.stream().filter(file -> file.getUri().equals(file1.getUri())).findFirst().isPresent());
        assert(result.stream().filter(file -> file.getUri().equals(file2.getUri())).findFirst().isPresent());
        
        when(as.isAuthorizedForFolder(folder.getFragment(), Vocabulary.c_Folder, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, new TypeReference<List<File>>() {});
        assertEquals(2, result.size());
        assert(result.stream().filter(file -> file.getUri().equals(file1.getUri())).findFirst().isPresent());
        assert(result.stream().filter(file -> file.getUri().equals(file2.getUri())).findFirst().isPresent());
        
        verify(fs, times(2)).getAllFolderFiles(folder.getUri());
    }
    
    @Test
    void getFolderSubfoldersTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Folder folder = Generator.generateFolder();
        final Folder subFolder1 = Generator.generateFolder();
        final Folder subFolder2 = Generator.generateFolder();
        folder.addSubfolder(subFolder1);
        folder.addSubfolder(subFolder2);
        when(fs.getAllFolderSubfolders(folder.getUri())) .thenReturn(folder.getFolders());
        
        String path = PATH + "/" + folder.getFragment() + "/subfolders";
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isOk()).andReturn();
        List<Folder> result = readValue(mvcResult, new TypeReference<List<Folder>>() {});
        assertEquals(2, result.size());
        assert(result.stream().filter(subFolder -> subFolder.getUri().equals(subFolder1.getUri())).findFirst().isPresent());
        assert(result.stream().filter(subFolder -> subFolder.getUri().equals(subFolder2.getUri())).findFirst().isPresent());
        
        when(as.isAuthorizedForFolder(folder.getFragment(), Vocabulary.c_Folder, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_Folder))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, new TypeReference<List<Folder>>() {});
        assertEquals(2, result.size());
        assert(result.stream().filter(subFolder -> subFolder.getUri().equals(subFolder1.getUri())).findFirst().isPresent());
        assert(result.stream().filter(subFolder -> subFolder.getUri().equals(subFolder2.getUri())).findFirst().isPresent());
        
        verify(fs, times(2)).getAllFolderSubfolders(folder.getUri());
    }
    
    @Test
    void createSubFolderTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Folder folder = Generator.generateFolder();
        
        String path = PATH + "/" + folder.getFragment() + "/subfolders";
        mockMvc.perform(post(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(folder))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isForbidden());
        mockMvc.perform(post(path).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(folder))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isOk());
        
        when(as.isAuthorizedForFolder(folder.getFragment(), Vocabulary.c_Folder, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(post(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(folder))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isOk());
        verify(fs, times(2)).createSubFolder(any(), eq(folder.getUri()));
    }
    
    @Test
    void updateFolderTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Folder folder = Generator.generateFolder();
        
        String path = PATH + "/" + folder.getFragment();
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(folder))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isForbidden());
        mockMvc.perform(put(path).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(folder))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isOk());
        
        when(as.isAuthorizedForFolder(folder.getFragment(), Vocabulary.c_Folder, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(folder))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isOk());
        verify(fs, times(2)).update(any(), eq(folder.getUri()));
    }
    
    @Test
    void deleteFolderTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final Folder folder = Generator.generateFolder();
        
        String path = PATH + "/" + folder.getFragment();
        mockMvc.perform(delete(path).with(user(userDetails))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isForbidden());
        mockMvc.perform(delete(path).with(user(adminDetails))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isOk());
        
        when(as.isAuthorizedForFolder(folder.getFragment(), Vocabulary.c_Folder, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(delete(path).with(user(userDetails))
            .param("namespace", Vocabulary.c_Folder)).andExpect(status().isOk());
        verify(fs, times(2)).deleteByURI(folder.getUri());
    }
    
}
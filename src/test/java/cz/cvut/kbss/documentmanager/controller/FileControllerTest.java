package cz.cvut.kbss.documentmanager.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.cvut.kbss.documentmanager.environment.Environment;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.conf.TestFileStorageConfig;
import cz.cvut.kbss.documentmanager.environment.conf.TestRestSecurityConfig;
import cz.cvut.kbss.documentmanager.model.*;
import cz.cvut.kbss.documentmanager.model.security.*;
import cz.cvut.kbss.documentmanager.service.FileService;
import cz.cvut.kbss.documentmanager.service.FolderService;
import cz.cvut.kbss.documentmanager.service.security.AuthorizationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import javax.servlet.Filter;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import cz.cvut.kbss.documentmanager.repository.FolderDao;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestRestSecurityConfig.class, FileControllerTest.Config.class, TestFileStorageConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
class FileControllerTest extends BaseControllerTestRunner {
    
    private static final String PATH = "/files";
    
    @Autowired
    private Filter springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext context;
    
    @Autowired
    private FileService fs;
    
    @Autowired
    private FolderService fls;
    
    @Autowired
    private FolderDao fd;
    
    @Autowired
    private AuthorizationService as;
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setupObjectMappers();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity(springSecurityFilterChain)).build();
    }
    
    @EnableWebMvc
    @Configuration
    public static class Config implements WebMvcConfigurer {
        
        @Mock
        private FileService fs;
        
        @Mock
        private FolderService fls;
        
        @Mock
        private FolderDao fd;
        
        final private AuthorizationService as;

        @InjectMocks
        private FileController fc;

        Config() {
            MockitoAnnotations.initMocks(this);
            as = mock(AuthorizationService.class, (Answer)(InvocationOnMock iom) -> false);
        }
        
        @Bean
        public FileService fileService() {
            return fs;
        }
        
        @Bean
        public FolderService folderService() {
            return fls;
        }
        
        @Bean
        public FolderDao folderDao() {
            return fd;
        }

        @Bean
        public FileController fileController() {
            return fc;
        }
        
        @Bean
        public AuthorizationService authorizationService() {
            return as;
        }
        
        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(Environment.createJsonLdMessageConverter());
            converters.add(Environment.createDefaultMessageConverter());
            converters.add(Environment.createStringEncodingMessageConverter());
        }
    }
    
    @Test
    void getFileTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        File file = Generator.generateDummyFileInfo();
        when(fs.findByURI(file.getUri())).thenReturn(file);
        
        String path = PATH + "/" + file.getFragment();
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isOk()).andReturn();
        File result = readValue(mvcResult, File.class);
        assertEquals(file.getUri(), result.getUri());
        assertEquals(file.getName(), result.getName());
        
        when(as.isAuthorizedForFile(file.getFragment(), Vocabulary.c_File, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, File.class);
        assertEquals(file.getUri(), result.getUri());
        assertEquals(file.getName(), result.getName());
        
        verify(fs, times(2)).findByURI(file.getUri());
    }
    
    @Test
    void getFileVersionsTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final File file = Generator.generateDummyFileInfo();
        final FileMemento fm1 = new FileMemento();
        fm1.setFromFile(file);
        fm1.generateUriFromFragment(file.getFragment());
        file.incrementVersion();
        final FileMemento fm2 = new FileMemento();
        fm2.setFromFile(file);
        fm2.generateUriFromFragment(file.getFragment());
        file.saveMemento(fm1);
        file.saveMemento(fm2);        
        when(fs.findByURI(file.getUri())).thenReturn(file);
        
        when(fs.getAllFileMementos(file.getUri())).thenReturn(file.getVersions());
        
        String path = PATH + "/" + file.getFragment() + "/versions";
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isOk()).andReturn();
        List<FileMemento> result = readValue(mvcResult, new TypeReference<List<FileMemento>>() {});
        assertEquals(2, result.size());
        assert(result.stream().filter(fm -> fm.getUri().equals(fm1.getUri())).findFirst().isPresent());
        assert(result.stream().filter(fm -> fm.getUri().equals(fm2.getUri())).findFirst().isPresent());
        
        when(as.isAuthorizedForFile(file.getFragment(), Vocabulary.c_File, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, new TypeReference<List<FileMemento>>() {});
        assertEquals(2, result.size());
        assert(result.stream().filter(fm -> fm.getUri().equals(fm1.getUri())).findFirst().isPresent());
        assert(result.stream().filter(fm -> fm.getUri().equals(fm2.getUri())).findFirst().isPresent());
        
        verify(fs, times(2)).getAllFileMementos(file.getUri());
    }
    
    @Test
    void getFileVersionTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final File file = Generator.generateDummyFileInfo();
        final FileMemento fm1 = new FileMemento();
        fm1.setFromFile(file);
        fm1.generateUriFromFragment(file.getFragment());
        file.incrementVersion();
        final FileMemento fm2 = new FileMemento();
        fm2.setFromFile(file);
        fm2.generateUriFromFragment(file.getFragment());
        file.saveMemento(fm1);
        file.saveMemento(fm2);        
        when(fs.findByURI(file.getUri())).thenReturn(file);
        
        long version = 0L;
        when(fs.getFileMemento(file.getUri(),version)).thenReturn(file.getMementoVersion(version));
        
        String path = PATH + "/" + file.getFragment() + "/versions/" + Long.toString(version);
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isOk()).andReturn();
        FileMemento result = readValue(mvcResult, FileMemento.class);
        assertEquals(fm1.getUri(), result.getUri());
        assertEquals(fm1.getVersion(), result.getVersion());
        
        when(as.isAuthorizedForFile(file.getFragment(), Vocabulary.c_File, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_File))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, FileMemento.class);
        assertEquals(fm1.getUri(), result.getUri());
        assertEquals(fm1.getVersion(), result.getVersion());
        
        verify(fs, times(2)).getFileMemento(file.getUri(), version);
    }
    
    /*
    @Test
    void uploadFileTest() throws Exception {
        Document doc = Generator.generateDocument();
        Folder uploadFolder = doc.getRootFolder();
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        MockMultipartFile upload = new MockMultipartFile("file", "filename.txt", "text/plain", "File content".getBytes());
        
        when(fd.existsById(uploadFolder.getUri())).thenReturn(true);
        when(fls.findByURI(uploadFolder.getUri())).thenReturn(uploadFolder);
        
        String path = "/folders/" + uploadFolder.getFragment() + "/files";
        mockMvc.perform(multipart(path).file(upload)
            .param("namespace", Vocabulary.c_Folder).param("name", "nameOfFile")
            .with(user(userDetails)).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isForbidden());
        mockMvc.perform(multipart(path).file(upload)
            .param("namespace", Vocabulary.c_Folder).param("name", "nameOfFile")
            .with(user(adminDetails)).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());
        
        when(as.isAuthorizedForFolder(uploadFolder.getFragment(), Vocabulary.c_Folder, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(multipart(path).file(upload)
            .param("namespace", Vocabulary.c_Folder).param("name", "nameOfFile")
            .with(user(userDetails)).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());
        
        verify(fs, times(2)).uploadFile(any(),any(),any(),any());
    }
    */ //TODO
    
    @Test
    void updateFileInfoTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final File file = Generator.generateDummyFileInfo();
        
        String path = PATH + "/" + file.getFragment();
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(file))
            .param("namespace", Vocabulary.c_File)).andExpect(status().isForbidden());
        mockMvc.perform(put(path).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(file))
            .param("namespace", Vocabulary.c_File)).andExpect(status().isOk());
        
        when(as.isAuthorizedForFile(file.getFragment(), Vocabulary.c_File, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(file))
            .param("namespace", Vocabulary.c_File)).andExpect(status().isOk());
        
        verify(fs, times(2)).updateFileInfo(any(), any());
    }
    
    @Test
    void deleteFileTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final File file = Generator.generateDummyFileInfo();
        
        String path = PATH + "/" + file.getFragment();
        mockMvc.perform(delete(path).with(user(userDetails))
            .param("namespace", Vocabulary.c_File)).andExpect(status().isForbidden());
        mockMvc.perform(delete(path).with(user(adminDetails))
            .param("namespace", Vocabulary.c_File)).andExpect(status().isOk());
        
        when(as.isAuthorizedForFile(file.getFragment(), Vocabulary.c_File, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(delete(path).with(user(userDetails))
            .param("namespace", Vocabulary.c_File)).andExpect(status().isOk());
        
        verify(fs, times(2)).deleteByURI(any());
    }
    
}
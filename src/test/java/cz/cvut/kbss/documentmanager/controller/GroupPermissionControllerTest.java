package cz.cvut.kbss.documentmanager.controller;

import cz.cvut.kbss.documentmanager.environment.Environment;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.conf.TestRestSecurityConfig;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.*;
import cz.cvut.kbss.documentmanager.service.security.AuthorizationService;
import cz.cvut.kbss.documentmanager.service.security.GroupPermissionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import javax.servlet.Filter;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestRestSecurityConfig.class, GroupPermissionControllerTest.Config.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
class GroupPermissionControllerTest extends BaseControllerTestRunner {
    
    private static final String PATH = "/permissions/group";
    
    @Autowired
    private Filter springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext context;
    
    @Autowired
    private GroupPermissionService gps;
    
    @Autowired
    private AuthorizationService as;
    
    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        super.setupObjectMappers();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity(springSecurityFilterChain)).build();
    }
    
    @EnableWebMvc
    @Configuration
    public static class Config implements WebMvcConfigurer {
        @Mock
        private GroupPermissionService gps;
        
        final private AuthorizationService as;

        @InjectMocks
        private GroupPermissionController gpc;

        Config() {
            MockitoAnnotations.initMocks(this);
            as = mock(AuthorizationService.class, (Answer)(InvocationOnMock iom) -> false);
        }
        
        @Bean
        public GroupPermissionService groupPermissionService() {
            return gps;
        }

        @Bean
        public GroupPermissionController groupPermissionController() {
            return gpc;
        }
        
        @Bean
        public AuthorizationService authorizationService() {
            return as;
        }
        
        @Override
        public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            converters.add(Environment.createJsonLdMessageConverter());
            converters.add(Environment.createDefaultMessageConverter());
            converters.add(Environment.createStringEncodingMessageConverter());
        }
    }
    
    @Test
    void getGroupPermissionTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        GroupPermission perm = Generator.generateGroupPermission();
        when(gps.findByURI(perm.getUri())).thenReturn(perm);
        
        String path = PATH + "/" + perm.getFragment();
        mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_GroupPermission))
            .andExpect(status().isForbidden());
        MvcResult mvcResult = mockMvc.perform(get(path).with(user(adminDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_GroupPermission))
            .andExpect(status().isOk()).andReturn();
        GroupPermission result = readValue(mvcResult, GroupPermission.class);
        assertEquals(perm.getUri(), result.getUri());
        assertEquals(perm.getPermissionLevel(), result.getPermissionLevel());
        
        when(as.isAuthorizedForGroupPermission(perm.getFragment(), Vocabulary.c_GroupPermission, PermissionLevel.READ))
            .thenReturn(true);
        mvcResult = mockMvc.perform(get(path).with(user(userDetails))
            .accept(MediaType.APPLICATION_JSON_VALUE).param("namespace", Vocabulary.c_GroupPermission))
            .andExpect(status().isOk()).andReturn();
        result = readValue(mvcResult, GroupPermission.class);
        assertEquals(perm.getUri(), result.getUri());
        
        verify(gps, times(2)).findByURI(perm.getUri());
    }
    
    @Test
    void updateGroupPermissionTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final GroupPermission perm = Generator.generateGroupPermission();
        
        String path = PATH + "/" + perm.getFragment();
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(perm))
            .param("namespace", Vocabulary.c_GroupPermission)).andExpect(status().isForbidden());
        mockMvc.perform(put(path).with(user(adminDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(perm))
            .param("namespace", Vocabulary.c_GroupPermission)).andExpect(status().isOk());
        
        when(as.isAuthorizedForGroupPermission(perm.getFragment(), Vocabulary.c_GroupPermission, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(put(path).with(user(userDetails))
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(perm))
            .param("namespace", Vocabulary.c_GroupPermission)).andExpect(status().isOk());
        verify(gps, times(2)).update(any(), eq(perm.getUri()));
    }
    
    @Test
    void deleteFolderTest() throws Exception {
        DocManUserDetails userDetails = new DocManUserDetails(Generator.generateUserAccount());
        DocManUserDetails adminDetails = new DocManUserDetails(Generator.generateAdminUserAccount());
        final GroupPermission perm = Generator.generateGroupPermission();
        
        String path = PATH + "/" + perm.getFragment();
        mockMvc.perform(delete(path).with(user(userDetails))
            .param("namespace", Vocabulary.c_GroupPermission)).andExpect(status().isForbidden());
        mockMvc.perform(delete(path).with(user(adminDetails))
            .param("namespace", Vocabulary.c_GroupPermission)).andExpect(status().isOk());
        
        when(as.isAuthorizedForGroupPermission(perm.getFragment(), Vocabulary.c_GroupPermission, PermissionLevel.SECURITY))
            .thenReturn(true);
        mockMvc.perform(delete(path).with(user(userDetails))
            .param("namespace", Vocabulary.c_GroupPermission)).andExpect(status().isOk());
        verify(gps, times(2)).deleteByURI(perm.getUri());
    }
    
}
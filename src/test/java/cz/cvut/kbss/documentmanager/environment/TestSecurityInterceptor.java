package cz.cvut.kbss.documentmanager.environment;

import cz.cvut.kbss.documentmanager.security.*;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TestSecurityInterceptor extends HandlerInterceptorAdapter {
    private final SecurityService securityService;

    public TestSecurityInterceptor(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        return securityService.isTokenValid(request);
    }
    
}

package cz.cvut.kbss.documentmanager.environment;

import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;
import cz.cvut.kbss.documentmanager.model.security.UserAccount;
import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import cz.cvut.kbss.documentmanager.util.Constants;
import java.util.HashSet;
import java.util.Random;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.generateUserPermissionURI;
import static cz.cvut.kbss.documentmanager.util.UriCreationUtils.generateGroupPermissionURI;
import java.io.IOException;
import java.io.InputStream;
import org.springframework.web.multipart.MultipartFile;

public class Generator {
    
    private static final Random RAND = new Random();

    public static String getRandomString() {
        return Long.toString(RAND.nextLong(), 36);
    }
    
    public static Document generateDocumentWithoutUris() {
        final Document document = new Document();
        final Folder rootFolder = new Folder();
        
        document.setName("Build test document name " + getRandomString());
        document.setDescription("Build test document description " + getRandomString());
        document.initDates();
        document.setRootFolder(rootFolder);
        document.setGroupPermissions(new HashSet<>());
        document.setUserPermissions(new HashSet<>());
        
        rootFolder.setName("Build test root folder name");
        rootFolder.setDescription("Build test root folder description");
        rootFolder.initDates();
        rootFolder.setIsRoot(true);
        rootFolder.setFolders(new HashSet<>());
        rootFolder.setFiles(new HashSet<>());        
        rootFolder.setDocument(document);
        
        return document;
    }
    
    public static Document generateDocument() {
        final Document document = new Document();
        final Folder rootFolder = generateFolder();
        
        document.setName("Build test document name " + getRandomString());
        document.setDescription("Build test document description " + getRandomString());
        document.setUri(Vocabulary.c_Document + "/BuildTestDoc_" + getRandomString());
        document.initDates();
        document.setRootFolder(rootFolder);
        document.setGroupPermissions(new HashSet<>());
        document.setUserPermissions(new HashSet<>());
        
        rootFolder.setDocument(document);
        rootFolder.setIsRoot(true);
        
        return document;
    }
    
    public static Folder generateFolder() {
        final Folder folder = new Folder();
        folder.setName("Build test root folder name");
        folder.setDescription("Build test root folder description");
        folder.setUri(Vocabulary.c_Folder + "/BuildTestFolder_" + getRandomString());
        folder.initDates();
        folder.setIsRoot(false);
        folder.setFolders(new HashSet<>());
        folder.setFiles(new HashSet<>());
        return folder;
    }
    
    public static File generateDummyFileInfo() {
        final File file = new File();
        file.setName("Build test dummy name");
        file.setUri(Vocabulary.c_File + "/BuildTestDummyFile_" + getRandomString());
        file.initDates();
        file.setFileName("Build test dummy file name");
        file.setFileSize(42L);
        file.setVersion(0L);
        return file;
    }
    
    public static UserPermission generateUserPermission() {
        final UserPermission userPerm = new UserPermission();
        userPerm.setUri(Vocabulary.c_UserPermission + "/BuildTestUserPermission_" + getRandomString());
        userPerm.setPermissionLevel(PermissionLevel.NONE);
        return userPerm;
    }
    
    public static UserPermission generateUserPermission(Document document, String userURI) {
        final UserPermission userPerm = generateUserPermission();
        userPerm.setUri(generateUserPermissionURI(document.getFragment(), userURI));
        userPerm.setUserURI(userURI);
        return userPerm;
    }    
    
    public static GroupPermission generateGroupPermission() {
        final GroupPermission grouPerm = new GroupPermission();
        grouPerm.setUri(Vocabulary.c_GroupPermission + "/BuildTestGroupPermission_" + getRandomString());
        grouPerm.setPermissionLevel(PermissionLevel.NONE);
        return grouPerm;
    }
    
    public static GroupPermission generateGroupPermission(Document doc, UserGroup group) {
        final GroupPermission grouPerm = generateGroupPermission();
        grouPerm.setUri(generateGroupPermissionURI(doc.getFragment(), group.getUri()));
        grouPerm.setGroup(group);
        return grouPerm;
    }
    
    public static UserGroup generateUserGroup() {
        final UserGroup group = new UserGroup();
        group.setUri(Vocabulary.c_UserGroup + "/BuildTestUserGroup_" + getRandomString());
        group.setName("Build test group");
        group.setPermissions(new HashSet<>());
        group.setUsers(new HashSet<>());
        return group;
    }
    
    public static String generateUserURI() {
        return Vocabulary.BASE + "/User/BuildTestUser_" + getRandomString();
    }
    
    public static UserAccount generateUserAccount() {
        UserAccount user = new UserAccount();
        user.firstName = "TestBuildUserFirstName_" + getRandomString();
        user.lastName = "TestBuildUserLastName_" + getRandomString();
        user.username = "TestBuildUserUsername_" + getRandomString();
        user.uri = generateUserURI();
        user.types = new HashSet<>();
        return user;
    }
    
    public static UserAccount generateAdminUserAccount() {
        UserAccount user = generateUserAccount();
        user.types.add(Constants.ADMIN_ROLE);
        return user;
    }
    
    public static String generateFileURI() {
        return Vocabulary.c_File + "/BuildTestFile_" + getRandomString();
    }
    
}

package cz.cvut.kbss.documentmanager.environment.conf;

import cz.cvut.kbss.documentmanager.conf.FileStorageConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestFileStorageConfig {

    @Bean
    public FileStorageConfig fsp() {
        final FileStorageConfig fsp = new FileStorageConfig();
        fsp.setUploadDir(System.getProperty("java.io.tmpdir") + "/semDocManFiles");
        return fsp;
    }
    
}

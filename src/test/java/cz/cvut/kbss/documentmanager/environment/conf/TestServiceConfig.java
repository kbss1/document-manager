package cz.cvut.kbss.documentmanager.environment.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"cz.cvut.kbss.documentmanager.service", "cz.cvut.kbss.documentmanager.repository"})
public class TestServiceConfig {
}

package cz.cvut.kbss.documentmanager.environment;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.mock.env.MockEnvironment;

public class PropertyMockingApplicationContextInitializer
        implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        MockEnvironment mockEnvironment = new MockEnvironment();
        mockEnvironment.setActiveProfiles("sem");
        configurableApplicationContext.setEnvironment(mockEnvironment);
    }
}

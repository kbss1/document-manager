package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.service.*;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.PropertyMockingApplicationContextInitializer;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.security.AuthenticationToken;
import cz.cvut.kbss.documentmanager.model.security.DocManUserDetails;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;
import cz.cvut.kbss.documentmanager.model.security.UserAccount;
import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = PropertyMockingApplicationContextInitializer.class)
public class GroupPermissionServiceTest extends BaseServiceTestRunner {

    @Autowired
    private DocumentService ds;
    
    @Autowired
    private GroupPermissionService gps;
    
    @Autowired
    private UserGroupService ugs;
    
    DocManUserDetails userDetails;
    
    Authentication authentication;
    
    SecurityContext securityContext;
    
    Document doc;
    
    UserGroup group;
    
    PermissionLevel level;
    
    @BeforeEach
    void setUP() {
        UserAccount user = Generator.generateUserAccount();
        userDetails = new DocManUserDetails(user);
        authentication = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        securityContext = new SecurityContextImpl(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        Document docEntity = Generator.generateDocument();
        ds.create(docEntity);        
        
        UserGroup groupEntity = Generator.generateUserGroup();
        ugs.create(groupEntity);
        
        GroupPermission perm = Generator.generateGroupPermission();
        level = PermissionLevel.WRITE;
        perm.setPermissionLevel(level);
        perm.setGroup(groupEntity);
        ds.createGroupPermission(perm, docEntity.getUri());
        
        doc = ds.findByURI(docEntity.getUri());
        group = ugs.findByURI(groupEntity.getUri());
    }
    
    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }
    
    @Test
    void groupPermissionIsPersisted() {
        GroupPermission perm = gps.findByURI(doc.getGroupPermissions().stream().findFirst().get().getUri());
        
        assertEquals(level, perm.getPermissionLevel());
        assertEquals(doc.getUri(), perm.getDocument().getUri());
        assertEquals(group.getUri(), perm.getGroup().getUri());
    }
    
    @Test
    void groupPermissionIsPersistedInDocument() {
        GroupPermission perm = doc.getGroupPermissions().stream().findFirst().get();
        
        assertEquals(level, perm.getPermissionLevel());
        assertEquals(doc.getUri(), perm.getDocument().getUri());
        assertEquals(group.getUri(), perm.getGroup().getUri());
    }
    
    @Test
    void groupPermissionIsPersistedInUserGroup() {
        GroupPermission perm = group.getPermissions().stream().findFirst().get();
        
        assertEquals(level, perm.getPermissionLevel());
        assertEquals(doc.getUri(), perm.getDocument().getUri());
        assertEquals(group.getUri(), perm.getGroup().getUri());
    }
    
    @Test
    void permissionIsUpdated() {
        GroupPermission perm = doc.getGroupPermissions().stream().findFirst().get();
        PermissionLevel newLevel = PermissionLevel.READ;
        perm.setPermissionLevel(newLevel);
        gps.update(perm, perm.getUri());
                
        doc = ds.findByURI(perm.getDocument().getUri());
        perm = doc.getGroupPermissions().stream().findFirst().get();        
        assertEquals(newLevel, perm.getPermissionLevel());
        
        group = ugs.findByURI(perm.getGroup().getUri());
        perm = group.getPermissions().stream().findFirst().get();        
        assertEquals(newLevel, perm.getPermissionLevel());        
    }    
    
    @Test
    void throwsExceptionOnUpdateWithMissingUri() {
        GroupPermission perm = doc.getGroupPermissions().stream().findFirst().get();
        perm.setUri(null);
        assertThrows(IncorrectRequestException.class, () -> gps.update(perm, perm.getUri()));
    }
    
    @Test
    void throwsExceptionOnUpdateWithMissingLevel() {
        GroupPermission perm = doc.getGroupPermissions().stream().findFirst().get();
        perm.setPermissionLevel(null);
        assertThrows(IncorrectRequestException.class, () -> gps.update(perm, perm.getUri()));
    }
    
    @Test
    void throwsExceptionOnUpdateWithdifferentUris() {
        GroupPermission perm = doc.getGroupPermissions().stream().findFirst().get();
        String oldUri = perm.getUri();
        perm.setUri(UriCreationUtils.generateUserPermissionURI("otherFragment", "differentUserUri"));
        assertThrows(IncorrectRequestException.class, () -> gps.update(perm, oldUri));
    }
    
    @Test
    void userPermissionIsDeleted() {
        GroupPermission perm = doc.getGroupPermissions().stream().findFirst().get();
        gps.deleteByURI(perm.getUri());
        
        Document retDoc = ds.findByURI(doc.getUri());
        assertNull(retDoc.getGroupPermissionWithUri(perm.getUri()));
        
        UserGroup retGroup = ugs.findByURI(group.getUri());
        assertNull(retGroup.getGroupPermissionWithUri(perm.getUri()));
        
        assertThrows(NotFoundException.class, () -> gps.findByURI(perm.getUri()));
    }
    
    @Test
    void throwsExceptionOnDeletionWithUnknownUri() {
        GroupPermission perm = doc.getGroupPermissions().stream().findFirst().get();
        assertThrows(NotFoundException.class, () -> gps.deleteByURI(perm.getUri() + Generator.getRandomString()));
    }
    
    @Test
    void permissionIsDeletedOnUserGroupDeletion() {
        GroupPermission perm = group.getPermissions().stream().findFirst().get();
        ugs.deleteByURI(group.getUri());
        assertThrows(NotFoundException.class, () -> gps.findByURI(perm.getUri()));
        doc = ds.findByURI(doc.getUri());
        assertEquals(0, doc.getGroupPermissions().size());
    }
    
    @Test
    void permissionIsDeletedOnDocumentDeletion() {
        GroupPermission perm = group.getPermissions().stream().findFirst().get();
        ds.deleteByURI(doc.getUri());
        assertThrows(NotFoundException.class, () -> gps.findByURI(perm.getUri()));
        group = ugs.findByURI(group.getUri());
        assertEquals(0, group.getPermissions().size());
    }
    
    
}
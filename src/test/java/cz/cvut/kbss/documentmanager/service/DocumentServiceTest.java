package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.PropertyMockingApplicationContextInitializer;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.security.AuthenticationToken;
import cz.cvut.kbss.documentmanager.model.security.DocManUserDetails;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;
import cz.cvut.kbss.documentmanager.model.security.UserAccount;
import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import cz.cvut.kbss.documentmanager.service.security.UserGroupService;
import cz.cvut.kbss.documentmanager.util.Constants;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = PropertyMockingApplicationContextInitializer.class)
public class DocumentServiceTest extends BaseServiceTestRunner {

    @Autowired
    private DocumentService ds;
    
    @Autowired
    private UserGroupService ugs;
    
    DocManUserDetails userDetails;
    
    Authentication authentication;
    
    SecurityContext securityContext;
    
    @BeforeEach
    void setUP() {
        UserAccount user = Generator.generateUserAccount();
        userDetails = new DocManUserDetails(user);
        authentication = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        securityContext = new SecurityContextImpl(authentication);
        SecurityContextHolder.setContext(securityContext);
    }
    
    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }
    
    @Test
    void rootFolderIsGenerated() {
        Document doc = Generator.generateDocument();
        doc.setRootFolder(null);
        Document dbDoc = ds.create(doc);
        assert(UriCreationUtils.isUriValid(dbDoc.getUri()));
        Set<Document> docs = ds.getDocumentList();
        assertEquals(1,docs.size());
        assert(docs.contains(doc));
    }
    
    @Test
    void documentAndFolderArePersisted() {
        Document doc = Generator.generateDocument();
        ds.create(doc);
        Document retDoc = ds.findByURI(doc.getUri());
        assertNotNull(retDoc);
        assertEquals(retDoc.getUri(), doc.getUri());
        assertEquals(retDoc.getName(), doc.getName());
        assertEquals(retDoc.getDescription(), doc.getDescription());
        
        Folder folder = doc.getRootFolder();
        Folder retFolder = retDoc.getRootFolder();
        assertEquals(retFolder.getDocument(), retDoc);
        assertEquals(retDoc.getRootFolder(), retFolder);
        
        assertEquals(retFolder.getUri(), folder.getUri());
        assertEquals(retFolder.getName(), folder.getName());
        assertEquals(retFolder.getDescription(), folder.getDescription());
    }
    
    @Test
    void generatesUriForDocumentAndRootFolder() {
        Document doc = Generator.generateDocumentWithoutUris();
        ds.create(doc);
        Document retDoc = ds.findByURI(doc.getUri());
        assert(UriCreationUtils.isUriValid(retDoc.getUri()));
        assert(UriCreationUtils.isUriValid(retDoc.getRootFolder().getUri()));
    }
    
    @Test
    void assignsUserPermissionToDocumentOnCreation() {
        Document doc = Generator.generateDocumentWithoutUris();
        ds.create(doc);
        assertNotNull(doc);
        assertEquals(doc.getUserPermissions().size(), 1);
        UserPermission perm = doc.getUserPermissions().stream().findFirst().get();
        assertEquals(perm.getUserURI(), userDetails.getUserUri());
        assertEquals(perm.getPermissionLevel(), Constants.MAX_PERM_LEVEL);
    }
    
    @Test
    void throwsExceptionForInvalidUri() {
        Document doc = Generator.generateDocument();
        doc.setUri("I am an invalid URI");
        assertThrows(IncorrectRequestException.class, () -> ds.create(doc));
    }
    
    @Test
    void throwsExceptionForInvalidRootFolderUri() {
        Document doc = Generator.generateDocument();
        doc.getRootFolder().setUri("I am an invalid URI");
        assertThrows(IncorrectRequestException.class, () -> ds.create(doc));
    }
    
    @Test
    void throwsExceptionForAlreadyExistingDocumentAndFolder() {
        Document doc = Generator.generateDocument();
        ds.create(doc);
        String docURI = doc.getUri();
        String folderURI = doc.getRootFolder().getUri();
        doc.setUri(docURI + "_I_am_different");
        assertThrows(IncorrectRequestException.class, () -> ds.create(doc));
        doc.setUri(docURI);
        doc.getRootFolder().setUri(folderURI + "_I_am_different");
        assertThrows(IncorrectRequestException.class, () -> ds.create(doc));
        doc.setUri(docURI + "_I_am_different");
        ds.create(doc);
    }
    
    @Test
    void throwsExceptionForMissingNames() {
        Document doc = Generator.generateDocument();
        doc.setName(null);
        assertThrows(IncorrectRequestException.class, () -> ds.create(doc));
        doc.setName("Doc name");
        doc.getRootFolder().setName(null);
        assertThrows(IncorrectRequestException.class, () -> ds.create(doc));
        doc.getRootFolder().setName("Folder name");
        ds.create(doc);
    }
    
    @Test
    void createUserPermissionTest() {
        Document doc = Generator.generateDocument();
        String userURI = Generator.generateUserURI();
        UserPermission perm = Generator.generateUserPermission(doc, userURI);
        ds.create(doc);
        ds.createUserPermission(perm, doc.getUri());
        Set<UserPermission> dbPerms = ds.getUserPermissionsByDocumentURI(doc.getUri());
        assert(dbPerms.contains(perm));
    }
    
    @Test
    void createUserPermissionTestThrowsExceptionOnAlreadyExistingUser() {
        Document doc = Generator.generateDocument();
        String userURI = Generator.generateUserURI();
        UserPermission perm = Generator.generateUserPermission(doc, userURI);
        ds.create(doc);
        ds.createUserPermission(perm, doc.getUri());
        assertThrows(IncorrectRequestException.class, () -> ds.createUserPermission(perm, doc.getUri()));
    }
    
    @Test
    void createUserPermissionTestThrowsExpcetionOnMissingUserURI() {
        Document doc = Generator.generateDocument();
        String userURI = Generator.generateUserURI();
        UserPermission perm = new UserPermission();
        perm.setPermissionLevel(PermissionLevel.NONE);
        ds.create(doc);
        assertThrows(IncorrectRequestException.class, () -> ds.createUserPermission(perm, doc.getUri()));
    }
    
    @Test
    void createUserPermissionTestThrowsExpcetionOnMissingPermLevel() {
        Document doc = Generator.generateDocument();
        String userURI = Generator.generateUserURI();
        UserPermission perm = Generator.generateUserPermission(doc, userURI);
        perm.setPermissionLevel(null);
        ds.create(doc);
        assertThrows(IncorrectRequestException.class, () -> ds.createUserPermission(perm, doc.getUri()));
    }
    
    @Test
    void createUserPermissionTestThrowsExpcetionOnInvalidUri() {
        Document doc = Generator.generateDocument();
        String userURI = Generator.generateUserURI();
        UserPermission perm = Generator.generateUserPermission(doc, userURI);
        perm.setUserURI("I am an invalid URI");
        ds.create(doc);
        assertThrows(IncorrectRequestException.class, () -> ds.createUserPermission(perm, doc.getUri()));
    }
    
    @Test
    void createGroupPermissionTest() {
        Document doc = Generator.generateDocument();
        ds.create(doc);
        UserGroup group = Generator.generateUserGroup();
        ugs.create(group);
        GroupPermission perm = Generator.generateGroupPermission(doc, group);        
        ds.createGroupPermission(perm, doc.getUri());
        Set<GroupPermission> dbPerms = ds.getGroupPermissionsByDocumentURI(doc.getUri());
        assert(dbPerms.contains(perm));
    }
    
    @Test
    void createGroupPermissionTestThrowsExceptionOnExistingPermission() {
        Document doc = Generator.generateDocument();
        ds.create(doc);
        UserGroup group = Generator.generateUserGroup();
        ugs.create(group);
        GroupPermission perm = Generator.generateGroupPermission(doc, group);        
        ds.createGroupPermission(perm, doc.getUri());
        assertThrows(IncorrectRequestException.class, () -> ds.createGroupPermission(perm, doc.getUri()));
    }
    
    @Test
    void createGroupPermissionTestThrowsExpcetionOnMissingPermLevel() {
        UserGroup group = Generator.generateUserGroup();
        ugs.create(group);        
        Document doc = Generator.generateDocument();
        ds.create(doc);
        
        GroupPermission perm = Generator.generateGroupPermission(doc, group);
        perm.setPermissionLevel(null);        
        assertThrows(IncorrectRequestException.class, () -> ds.createGroupPermission(perm, doc.getUri()));
    }
    
    @Test
    void createGroupPermissionTestThrowsExpcetionOnMissingUserGroup() {
        UserGroup group = Generator.generateUserGroup();
        ugs.create(group);        
        Document doc = Generator.generateDocument();
        ds.create(doc);
        
        GroupPermission perm = new GroupPermission();
        perm.setPermissionLevel(PermissionLevel.NONE);
        assertThrows(IncorrectRequestException.class, () -> ds.createGroupPermission(perm, doc.getUri()));
    }
    
    @Test
    void createGroupPermissionTestThrowsExpcetionOnNonExistingGroup() {
        UserGroup group = Generator.generateUserGroup();
        ugs.create(group);        
        Document doc = Generator.generateDocument();
        ds.create(doc);
        
        GroupPermission perm = Generator.generateGroupPermission(doc, group);
        perm.getGroup().setUri(group.getUri() + "_");
        assertThrows(NotFoundException.class, () -> ds.createGroupPermission(perm, doc.getUri()));
    }
    
    @Test
    void updatesDocumentInformation() {        
        Document doc = Generator.generateDocument();
        ds.create(doc);
        
        String newName = "New name";
        String newDescription = "New description";
        doc.setDescription(newDescription);
        doc.setName(newName);
        ds.update(doc, doc.getUri());        
        
        Document retDoc = ds.findByURI(doc.getUri());
        assertEquals(retDoc.getDescription(), newDescription);
        assertEquals(retDoc.getName(), newName);
    }
    
    @Test
    void throwsExceptionForMissingNameUriOnUpdate() {
        Document doc = Generator.generateDocument();        
        ds.create(doc);
        doc.setName(null);
        assertThrows(IncorrectRequestException.class, () -> ds.update(doc, doc.getUri() + "_"));
        doc.setName("");
        assertThrows(IncorrectRequestException.class, () -> ds.update(doc, doc.getUri() + "_"));
    }
    
    @Test
    void throwsExceptionForNonMatchingUriOnUpdate() {
        Document doc = Generator.generateDocument();
        ds.create(doc);
        assertThrows(IncorrectRequestException.class, () -> ds.update(doc, doc.getUri() + "_"));
    }
    
    @Test
    void throwsExceptionForNonExistingDocumentURI() {
        Document doc = Generator.generateDocument();
        assertThrows(NotFoundException.class, () -> ds.update(doc, doc.getUri()));
    }
    
    @Test
    void documentIsRemoved() {
        Document doc = Generator.generateDocument();
        ds.create(doc);
        ds.deleteByURI(doc.getUri());
        assertThrows(NotFoundException.class, () -> ds.findByURI(doc.getUri()));
        assertThrows(NotFoundException.class, () -> ds.deleteByURI(doc.getUri()));

    }
    
}
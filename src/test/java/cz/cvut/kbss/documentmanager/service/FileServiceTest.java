package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.PropertyMockingApplicationContextInitializer;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.FileMemento;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.AuthenticationToken;
import cz.cvut.kbss.documentmanager.model.security.DocManUserDetails;
import cz.cvut.kbss.documentmanager.model.security.UserAccount;
import cz.cvut.kbss.documentmanager.repository.FileStorageDao;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import java.io.IOException;
import java.nio.file.Path;
import org.apache.commons.io.IOUtils;
import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = PropertyMockingApplicationContextInitializer.class)
public class FileServiceTest extends BaseServiceTestRunner {

    @Autowired
    private DocumentService ds;
    
    @Autowired
    private FolderService fls;
    
    @Autowired
    private FileService fs;    
    @Autowired
    private FileStorageDao fsd;
    
    DocManUserDetails userDetails;
    
    Authentication authentication;
    
    SecurityContext securityContext;
    
    Folder root;
    
    MockMultipartFile mockFile;
    
    String mockFileName = "mockFile.txt";
    String mockFileContent = "mock file content";

    public FileServiceTest() {
        mockFile = new MockMultipartFile("file", mockFileName, "plain/text", mockFileContent.getBytes());
    }
    
    @BeforeEach
    void setUP() {
        UserAccount user = Generator.generateUserAccount();
        userDetails = new DocManUserDetails(user);
        authentication = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        securityContext = new SecurityContextImpl(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        Document doc = Generator.generateDocument();
        ds.create(doc);
        root = ds.findByURI(doc.getUri()).getRootFolder();
    }
    
    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }
    
    @Test
    void fileInformationIsCreated() {        
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        File dbFile = fs.findByURI(fileURI);
        assertEquals(dbFile.getLocation().getUri(), root.getUri());
        assertEquals(dbFile.getName(), dbName);
        assertEquals(dbFile.getFileName(), mockFile.getOriginalFilename());
        Long fileSize = mockFile.getSize();
        assertEquals(fileSize, dbFile.getFileSize());
        Long startingVersion = 0L;
        assertEquals(dbFile.getVersion(), startingVersion);
        assertEquals(1, dbFile.getVersions().size());
        FileMemento memento = dbFile.getVersions().stream().findFirst().get();
        assertEquals(memento.getFileName(), mockFileName);
        assertEquals(memento.getVersion(), startingVersion);
    }
    
    
    @Test
    void uriIsGeneratedIfMissing() {        
        String dbName = "Mock File";
        fs.uploadFile(mockFile, null, dbName, root.getUri());
        root = fls.findByURI(root.getUri());
        File dbFileInfo = root.getFiles().stream().findFirst().get();
        assert(UriCreationUtils.isUriValid(dbFileInfo.getUri()));        
    }
    
    @Test
    void getFileMementoTest() {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        assertEquals(1, fs.getAllFileMementos(fileURI).size());
        FileMemento dbFm = fs.getFileMemento(fileURI, 0);
        assertEquals(mockFileName, dbFm.getFileName());
        Long version = 0L;
        assertEquals(dbFm.getVersion(), version);
    }
    
    @Test
    void getFileContentTest() throws IOException {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        Resource ret = fs.getFileContent(fileURI, null);
        String fileContent = IOUtils.toString(ret.getInputStream(), "UTF-8");
        assertEquals(mockFileContent, fileContent);        
    }
    
    @Test
    void throwsExceptionOnInvalidEmptyFile() {
        final String dbName = "Mock File";
        String fileURI = Generator.generateFileURI();
        MockMultipartFile emptyMockFile = new MockMultipartFile(mockFileName, "".getBytes());        
        assertThrows(IncorrectRequestException.class, () -> fs.uploadFile(emptyMockFile, fileURI, dbName, root.getUri()));
    }
    
    @Test
    void throwsExceptionOnInvalidUri() {
        final String dbName = "Mock File";
        String fileURI = "Invalid URI";
        assertThrows(IncorrectRequestException.class, () -> fs.uploadFile(mockFile, fileURI, dbName, root.getUri()));
    }
    
    @Test
    void throwsExceptionOnNonExistingFolder() {
        String randomFolderURI = Vocabulary.c_Folder + "/" + Generator.getRandomString();
        assertThrows(NotFoundException.class, () -> fs.uploadFile(mockFile, Generator.generateFileURI(), "nameOfTheFile", randomFolderURI));
    }
    
    @Test
    void throwsExceptionOnMissingName() {
        assertThrows(IncorrectRequestException.class, () -> fs.uploadFile(mockFile, Generator.generateFileURI(), null, root.getUri()));
        assertThrows(IncorrectRequestException.class, () -> fs.uploadFile(mockFile, Generator.generateFileURI(), "", root.getUri()));
    }
    
    @Test
    void throwsExceptionOnAlreadyExistingFileUri() {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        assertThrows(IncorrectRequestException.class, () -> fs.uploadFile(mockFile, fileURI, dbName, root.getUri()));
    }
    
    @Test
    void updatesFileInfo() {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        File dbFile = fs.findByURI(fileURI);
        String newName = "New mock file";
        dbFile.setName(newName);
        fs.updateFileInfo(dbFile, fileURI);
        dbFile = fs.findByURI(fileURI);
        assertEquals(newName, dbFile.getName());
    }
    
    @Test
    void throwsExceptionOnMissingNameOnFileInfoUpdate() {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        File dbFile = fs.findByURI(fileURI);
        dbFile.setName(null);
        assertThrows(IncorrectRequestException.class, () -> fs.updateFileInfo(dbFile, fileURI));
    }
    
    @Test
    void throwsExceptionOnNonMatchingUriOnFileInfoUpdate() {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        File dbFile = fs.findByURI(fileURI);
        dbFile.setUri(null);
        assertThrows(IncorrectRequestException.class, () -> fs.updateFileInfo(dbFile, fileURI));
    }
    
    @Test
    void mementoIsCreatedOnContentUpdate() {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        MockMultipartFile secondMockFile = new MockMultipartFile("secondMockFile.txt", "second mock file content".getBytes());
        fs.updateFileContent(secondMockFile, fileURI);
        File dbFile = fs.findByURI(fileURI);
        Long fileVersion = 1L;
        assertEquals(fileVersion, dbFile.getVersion());
        assertEquals(2, dbFile.getVersions().size());
        assertEquals(dbFile.getFileName(), secondMockFile.getOriginalFilename());
        Long fileSize = secondMockFile.getSize();
        assertEquals(fileSize, dbFile.getFileSize());
        
        FileMemento secondMemento = dbFile.getVersions().stream().filter(memento -> memento.getVersion() == 1).findFirst().get();
        assertEquals(secondMemento.getFileName(), secondMockFile.getOriginalFilename());
        assertEquals(secondMemento.getFileSize(), fileSize);
        assertEquals(fileVersion, secondMemento.getVersion());
    }
    
    @Test
    void fileIsDeleted() {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        fs.deleteByURI(fileURI);
        assertThrows(NotFoundException.class, () -> fs.findByURI(fileURI));
    }
    
    @Test
    void thorsExceptionOnNonExistingFileURIOnDelete() {
        String fileURI = Generator.generateFileURI();
        String dbName = "Mock File";
        fs.uploadFile(mockFile, fileURI, dbName, root.getUri());
        fs.deleteByURI(fileURI);
        assertThrows(NotFoundException.class, () -> fs.findByURI(fileURI));
    }
    
    
    @Test
    void fileCleanupTest() {
        File file = Generator.generateDummyFileInfo();
        Path filePath = file.getStoragePath();
        fs.uploadFile(mockFile, file.getUri(), file.getName(), root.getUri());
        assert(fsd.fileFolderExists(filePath));
        fs.deleteLonelyFiles();
        assert(fsd.fileFolderExists(filePath));
        fsd.deleteFileByFolderPath(filePath);
        fs.deleteLonelyFiles();
        assert(!fsd.fileFolderExists(filePath));
        assertThrows(NotFoundException.class, () -> fs.findByURI(file.getUri()));
    }
    
}

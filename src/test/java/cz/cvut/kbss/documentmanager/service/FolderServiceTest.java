package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.PropertyMockingApplicationContextInitializer;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.AuthenticationToken;
import cz.cvut.kbss.documentmanager.model.security.DocManUserDetails;
import cz.cvut.kbss.documentmanager.model.security.UserAccount;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = PropertyMockingApplicationContextInitializer.class)
public class FolderServiceTest extends BaseServiceTestRunner {
    
    @Autowired
    private DocumentService ds;
    
    @Autowired
    private FolderService fs;
    
    DocManUserDetails userDetails;
    
    Authentication authentication;
    
    SecurityContext securityContext;
    
    Folder root;
    
    @BeforeEach
    void setUP() {
        UserAccount user = Generator.generateUserAccount();
        userDetails = new DocManUserDetails(user);
        authentication = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        securityContext = new SecurityContextImpl(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        Document doc = Generator.generateDocument();
        ds.create(doc);
        root = ds.findByURI(doc.getUri()).getRootFolder();
    }
    
    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }
    
    @Test
    void generatesUriForSubfolder() {
        Folder folder = Generator.generateFolder();
        fs.createSubFolder(folder, root.getUri());
        Folder retFolder = fs.findByURI(folder.getUri());
        assert(UriCreationUtils.isUriValid(retFolder.getUri()));
    }
    
    @Test
    void folderIsFound() {
        assertEquals(fs.findByURI(root.getUri()), root);
    }
    
    @Test
    void subfolderIsAdded() {
        Folder subFolder = Generator.generateFolder();
        fs.createSubFolder(subFolder, root.getUri());
        Folder retSubFolder = fs.findByURI(subFolder.getUri());
        assertEquals(subFolder.getUri(), retSubFolder.getUri());
        assertEquals(subFolder.getDescription(), retSubFolder.getDescription());
        assertEquals(subFolder.getName(), retSubFolder.getName());
        assertEquals(subFolder.getDocument(), retSubFolder.getDocument());
        assertEquals(subFolder.getParentFolder(), retSubFolder.getParentFolder());
    }
    
    @Test
    void getAllSubfoldersTest() {
        Folder subFolder = Generator.generateFolder();
        fs.createSubFolder(subFolder, root.getUri());
        Set<Folder> subs = fs.getAllFolderSubfolders(root.getUri());
        assertEquals(1, subs.size());
        assert(subs.contains(subFolder));
    }
    
    @Test
    void subfolderIsDeleted() {
        Folder subFolder = Generator.generateFolder();
        fs.createSubFolder(subFolder, root.getUri());
        Folder dbRoot = fs.findByURI(root.getUri());
        assertEquals(1, dbRoot.getFolders().size());
        fs.deleteByURI(subFolder.getUri());
        dbRoot = fs.findByURI(root.getUri());
        assertEquals(0, dbRoot.getFolders().size());
        assertThrows(NotFoundException.class, () -> fs.findByURI(subFolder.getUri()));
    }
    
    @Test
    void throwsExceptionForInvalidSubFolderUri() {
        Folder subFolder = Generator.generateFolder();
        subFolder.setUri("I am an invalid URI");
        assertThrows(IncorrectRequestException.class, () -> fs.createSubFolder(subFolder, root.getUri()));
    }
    
    @Test
    void throwsExceptionOnAlreadyExistingFolder() {
        Folder subFolder = Generator.generateFolder();
        fs.createSubFolder(subFolder, root.getUri());
        assertThrows(IncorrectRequestException.class, () -> fs.createSubFolder(subFolder, root.getUri()));
    }
    
    @Test
    void throwsExceptionOnMissingName() {
        Folder subFolder = Generator.generateFolder();
        subFolder.setName(null);
        assertThrows(IncorrectRequestException.class, () -> fs.createSubFolder(subFolder, root.getUri()));
    }
    
    @Test
    void updatesFolder() {
        Folder subFolder = Generator.generateFolder();
        String newName = "New name";
        String newDescription = "New description";
        fs.createSubFolder(subFolder, root.getUri());
        subFolder.setName(newName);
        subFolder.setDescription(newDescription);
        fs.update(subFolder, subFolder.getUri());
        Folder dbFolder = fs.findByURI(subFolder.getUri());
        assertEquals(subFolder.getName(), dbFolder.getName());
        assertEquals(subFolder.getDescription(), dbFolder.getDescription());
    }

    @Test
    void throwsExceptionOnVariedUriOnFolderUpdate() {
        Folder subFolder = Generator.generateFolder();
        String newName = "New name";
        String newDescription = "New description";
        fs.createSubFolder(subFolder, root.getUri());
        subFolder.setName(newName);
        subFolder.setDescription(newDescription);
        String originalURI = subFolder.getUri();
        subFolder.setUri(Vocabulary.c_Folder + "newURI");        
        assertThrows(IncorrectRequestException.class, () -> fs.update(subFolder, originalURI));
    }
    
    @Test
    void throwsExceptionOnMissingNameonFolderUpdate() {
        Folder subFolder = Generator.generateFolder();
        fs.createSubFolder(subFolder, root.getUri());
        subFolder.setName("");
        assertThrows(IncorrectRequestException.class, () -> fs.update(subFolder, subFolder.getUri()));
    }
    
    @Test
    void subFolderIsRemoved() {
        Folder subFolder = Generator.generateFolder();
        fs.createSubFolder(subFolder, root.getUri());
        assertNotNull(fs.findByURI(subFolder.getUri()));
        fs.deleteByURI(subFolder.getUri());
        assertThrows(NotFoundException.class, () -> fs.findByURI(subFolder.getUri()));
    }
    
    @Test
    void throwsExceptionOnNonExistingFolderOnRemove() {
        assertThrows(NotFoundException.class, () -> fs.deleteByURI(Vocabulary.c_Folder + "/" + Generator.getRandomString()));
    }
    
    @Test
    void rootFolderCannotBeRemoved() {
        assertThrows(IncorrectRequestException.class, () -> fs.deleteByURI(root.getUri()));
    }
    
}

package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.service.*;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.PropertyMockingApplicationContextInitializer;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.File;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.model.Vocabulary;
import cz.cvut.kbss.documentmanager.model.security.AuthenticationToken;
import cz.cvut.kbss.documentmanager.model.security.DocManUserDetails;
import cz.cvut.kbss.documentmanager.model.security.GroupPermission;
import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;
import cz.cvut.kbss.documentmanager.model.security.UserAccount;
import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import java.util.HashSet;
import java.util.Set;
import org.assertj.core.util.Arrays;
import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = PropertyMockingApplicationContextInitializer.class)
public class AuthorizationServiceTest extends BaseServiceTestRunner {

    final MockMultipartFile mockFile;
    
    final Set<PermissionLevel> testedLevels;
    
    @Autowired
    private AuthorizationService as;
    
    @Autowired
    private DocumentService ds;
    
    @Autowired
    private FileService fs;
    
    @Autowired
    private UserGroupService ugs;
    
    @Autowired
    private UserPermissionService ups;

    @Autowired
    private GroupPermissionService gps;
    
    DocManUserDetails userDetails;
    
    Authentication authentication;
    
    SecurityContext securityContext;
    
    Document doc;
    
    public AuthorizationServiceTest() {
        mockFile = new MockMultipartFile("mockFile.txt", "mock file content".getBytes());
        testedLevels = new HashSet(Arrays.asList(PermissionLevel.values()));
        testedLevels.remove(PermissionLevel.NONE);
    }
    
    @BeforeEach
    void setUP() {
        UserAccount author = Generator.generateUserAccount();
        userDetails = new DocManUserDetails(author);
        authentication = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        securityContext = new SecurityContextImpl(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        doc = ds.create(Generator.generateDocument());
        fs.uploadFile(mockFile, Generator.generateFileURI(), "Test file", doc.getRootFolder().getUri());        
        UserGroup group = ugs.create(Generator.generateUserGroup());
        ugs.addUser(group.getUri(), userDetails.getUserUri());
    }
    
    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }
    
    void assertHasAccess(Document doc, Set<PermissionLevel> permissions, boolean hasAccess) {
        Folder root = doc.getRootFolder();
        File fileInfo = null;
        if (!root.getFiles().isEmpty()) {
            fileInfo = root.getFiles().stream().findFirst().get();
        }
        GroupPermission groupPerm = null;
        if (!doc.getGroupPermissions().isEmpty()) {
            groupPerm = doc.getGroupPermissions().stream().findFirst().get();
        }
        UserPermission userPerm = null;
        if (!doc.getUserPermissions().isEmpty()) {
            userPerm = doc.getUserPermissions().stream().findFirst().get();
        }
        
        for (PermissionLevel perm : permissions) {
            assertEquals(hasAccess, as.isAuthorizedForDocument(doc.getFragment(), Vocabulary.c_Document, perm));
            assertEquals(hasAccess, as.isAuthorizedForFolder(root.getFragment(), Vocabulary.c_Folder, perm));
            if (fileInfo != null) {
                assertEquals(hasAccess, as.isAuthorizedForFile(fileInfo.getFragment(), Vocabulary.c_File, perm));
            }
            if (userPerm != null) {
                assertEquals(hasAccess, as.isAuthorizedForUserPermission(userPerm.getFragment(), Vocabulary.c_UserPermission, perm));
            }
            if (groupPerm != null) { 
                assertEquals(hasAccess, as.isAuthorizedForGroupPermission(groupPerm.getFragment(), Vocabulary.c_GroupPermission, perm));
            }
        }
    }
    
    @Test
    void documentAuthorHasFullAccessToEntireDocument() {
        Set<PermissionLevel> levels = new HashSet(Arrays.asList(PermissionLevel.values()));
        levels.remove(PermissionLevel.NONE);
        assertHasAccess(doc, levels, true);
    }
    
    @Test
    void throwsExceptionForNonExistingDocument() {
        assertThrows(NotFoundException.class, () ->
            as.isAuthorizedForDocument(Generator.getRandomString(),
                Vocabulary.c_Document, PermissionLevel.READ));
    }
    
    @Test
    void throwsExceptionForNonExistingFolder() {
        assertThrows(NotFoundException.class, () ->
            as.isAuthorizedForFolder(Generator.getRandomString(),
                Vocabulary.c_Folder, PermissionLevel.READ));
    }
    
    @Test
    void throwsExceptionForNonExistingFile() {
        assertThrows(NotFoundException.class, () ->
            as.isAuthorizedForFile(Generator.getRandomString(),
                Vocabulary.c_File, PermissionLevel.READ));
    }
    
    @Test
    void throwsExceptionForNonExistingUserPermission() {
        assertThrows(NotFoundException.class, () ->
            as.isAuthorizedForUserPermission(Generator.getRandomString(),
                Vocabulary.c_UserPermission, PermissionLevel.READ));
    }
    
    @Test
    void throwsExceptionForNonExistingGroupPermission() {
        assertThrows(NotFoundException.class, () ->
            as.isAuthorizedForGroupPermission(Generator.getRandomString(),
                Vocabulary.c_GroupPermission, PermissionLevel.READ));
    }
    
    @Test
    void userAccessToDocumentWithUserPermissionTest() {
        SecurityContextHolder.clearContext();
        UserAccount newUser = Generator.generateUserAccount();
        userDetails = new DocManUserDetails(newUser);
        authentication = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        securityContext = new SecurityContextImpl(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        Set<PermissionLevel> hasAccessLevels = new HashSet();
        Set<PermissionLevel> hasNoAccessLevels = new HashSet(Arrays.asList(PermissionLevel.values()));
        hasAccessLevels.add(PermissionLevel.NONE);
        hasNoAccessLevels.remove(PermissionLevel.NONE);
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        UserPermission userPerm = Generator.generateUserPermission(doc, newUser.uri);
        userPerm.setPermissionLevel(PermissionLevel.NONE);
        ds.createUserPermission(userPerm, doc.getUri());
        doc = ds.findByURI(doc.getUri());
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        userPerm.setPermissionLevel(PermissionLevel.READ);
        ups.update(userPerm, userPerm.getUri());
        doc = ds.findByURI(doc.getUri());
        hasAccessLevels.add(PermissionLevel.READ);
        hasNoAccessLevels.remove(PermissionLevel.READ);
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        userPerm.setPermissionLevel(PermissionLevel.WRITE);
        ups.update(userPerm, userPerm.getUri());
        doc = ds.findByURI(doc.getUri());        
        hasAccessLevels.add(PermissionLevel.WRITE);
        hasNoAccessLevels.remove(PermissionLevel.WRITE);        
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        userPerm.setPermissionLevel(PermissionLevel.SECURITY);
        ups.update(userPerm, userPerm.getUri());
        doc = ds.findByURI(doc.getUri());        
        hasAccessLevels.add(PermissionLevel.SECURITY);        
        assertHasAccess(doc, hasAccessLevels, true);
    }
    
    @Test
    void userAccessToDocumentWithGroupPermissionTest() {
        SecurityContextHolder.clearContext();
        UserAccount newUser = Generator.generateUserAccount();
        userDetails = new DocManUserDetails(newUser);
        authentication = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        securityContext = new SecurityContextImpl(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        Set<PermissionLevel> hasAccessLevels = new HashSet();
        Set<PermissionLevel> hasNoAccessLevels = new HashSet(Arrays.asList(PermissionLevel.values()));
        hasAccessLevels.add(PermissionLevel.NONE);
        hasNoAccessLevels.remove(PermissionLevel.NONE);
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        UserGroup group = Generator.generateUserGroup();        
        ugs.create(group);
        ugs.addUser(group.getUri(), newUser.uri);
        GroupPermission groupPerm = Generator.generateGroupPermission(doc, group);
        groupPerm.setPermissionLevel(PermissionLevel.NONE);
        groupPerm = ds.createGroupPermission(groupPerm, doc.getUri());
        doc = ds.findByURI(doc.getUri());
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        groupPerm.setPermissionLevel(PermissionLevel.READ);
        gps.update(groupPerm, groupPerm.getUri());
        doc = ds.findByURI(doc.getUri());
        hasAccessLevels.add(PermissionLevel.READ);
        hasNoAccessLevels.remove(PermissionLevel.READ);
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        groupPerm.setPermissionLevel(PermissionLevel.WRITE);
        gps.update(groupPerm, groupPerm.getUri());
        doc = ds.findByURI(doc.getUri());
        hasAccessLevels.add(PermissionLevel.WRITE);
        hasNoAccessLevels.remove(PermissionLevel.WRITE);
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        groupPerm.setPermissionLevel(PermissionLevel.SECURITY);
        gps.update(groupPerm, groupPerm.getUri());
        doc = ds.findByURI(doc.getUri());
        hasAccessLevels.add(PermissionLevel.SECURITY);
        assertHasAccess(doc, hasAccessLevels, true);        
    }
    
    @Test
    void permissionPriorityTest() {
        UserGroup group = Generator.generateUserGroup();
        ugs.create(group);
        ugs.addUser(group.getUri(), userDetails.getUserUri());
        GroupPermission groupPerm = Generator.generateGroupPermission(doc, group);
        groupPerm.setPermissionLevel(PermissionLevel.WRITE);
        ds.createGroupPermission(groupPerm, doc.getUri());
        
        Set<PermissionLevel> hasAccessLevels = new HashSet(Arrays.asList(PermissionLevel.values()));
        Set<PermissionLevel> hasNoAccessLevels = new HashSet();
        assertHasAccess(doc, hasAccessLevels, true);
        
        UserPermission authorPerm = doc.getUserPermissions().stream().findFirst().get();
        authorPerm.setPermissionLevel(PermissionLevel.READ);
        ups.update(authorPerm, authorPerm.getUri());
        hasAccessLevels.remove(PermissionLevel.SECURITY);
        hasAccessLevels.remove(PermissionLevel.WRITE);
        hasNoAccessLevels.add(PermissionLevel.SECURITY);
        hasNoAccessLevels.add(PermissionLevel.WRITE);
        
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        ups.deleteByURI(authorPerm.getUri());
        hasAccessLevels.add(PermissionLevel.WRITE);
        hasNoAccessLevels.remove(PermissionLevel.WRITE);
        doc = ds.findByURI(doc.getUri());
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
    }
    
    @Test
    void multipleGroupPermissionsPriorityTest() {
        UserGroup group1 = Generator.generateUserGroup();
        ugs.create(group1);
        ugs.addUser(group1.getUri(), userDetails.getUserUri());
        UserGroup group2 = Generator.generateUserGroup();
        ugs.create(group2);
        ugs.addUser(group2.getUri(), userDetails.getUserUri());
        
        ups.deleteByURI(doc.getUserPermissions().stream().findFirst().get().getUri());
        
        GroupPermission groupPerm1 = Generator.generateGroupPermission(doc, group1);
        groupPerm1.setPermissionLevel(PermissionLevel.READ);
        groupPerm1 = ds.createGroupPermission(groupPerm1, doc.getUri());
        
        GroupPermission groupPerm2 = Generator.generateGroupPermission(doc, group2);
        groupPerm2.setPermissionLevel(PermissionLevel.WRITE);
        groupPerm2 = ds.createGroupPermission(groupPerm2, doc.getUri());
        
        Set<PermissionLevel> hasAccessLevels = new HashSet(Arrays.asList(PermissionLevel.values()));
        Set<PermissionLevel> hasNoAccessLevels = new HashSet();
        hasAccessLevels.remove(PermissionLevel.SECURITY);
        hasNoAccessLevels.add(PermissionLevel.SECURITY);
        doc = ds.findByURI(doc.getUri());        
        assertHasAccess(doc, hasAccessLevels, true);
        assertHasAccess(doc, hasNoAccessLevels, false);
        
        groupPerm1.setPermissionLevel(PermissionLevel.SECURITY);
        gps.update(groupPerm1, groupPerm1.getUri());
        doc = ds.findByURI(doc.getUri());
        hasAccessLevels.add(PermissionLevel.SECURITY);
        assertHasAccess(doc, hasAccessLevels, true);
    }
    
    
}
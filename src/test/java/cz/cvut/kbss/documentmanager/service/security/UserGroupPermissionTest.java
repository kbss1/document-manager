package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.service.*;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.PropertyMockingApplicationContextInitializer;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.security.UserGroup;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = PropertyMockingApplicationContextInitializer.class)
public class UserGroupPermissionTest extends BaseServiceTestRunner {

    @Autowired
    private UserGroupService ugs;
    
    UserGroup groupEntity;
    
    String addedUser;
    
    @BeforeEach
    void setUP() {        
        addedUser = Generator.generateUserURI();
        groupEntity = Generator.generateUserGroup();
        groupEntity.addUser(addedUser);
        ugs.create(groupEntity);
    }
    
    @Test
    void userGroupIsFound() {
        UserGroup dbGroup = ugs.findByURI(groupEntity.getUri());
        assertEquals(groupEntity.getUri(), dbGroup.getUri());
        assertEquals(groupEntity.getName(), dbGroup.getName());
        assertEquals(1, dbGroup.getUsers().size());
        assertEquals(addedUser, dbGroup.getUsers().stream().findFirst().get());
    }
    
    @Test
    void generatesUriWhenMissing() {
        groupEntity = Generator.generateUserGroup();
        groupEntity.setUri(null);
        ugs.create(groupEntity);
        UserGroup dbGroup = ugs.findByURI(groupEntity.getUri());
        assert(UriCreationUtils.isUriValid(dbGroup.getUri()));
    }
    
    
    @Test
    void throwsExceptionOnNonExistingUserGroupOnFind() {
        UserGroup newGroup = Generator.generateUserGroup();
        assertThrows(NotFoundException.class, () -> ugs.findByURI(newGroup.getUri()));
    }
    
    @Test
    void throwsExceptionOnMissingNameOnCreate() {
        UserGroup group = Generator.generateUserGroup();
        group.setName("");
        assertThrows(IncorrectRequestException.class,() -> ugs.create(group));
    }
    
    @Test
    void throwsExceptionOnInvalidUriOnCreate() {
        UserGroup group = Generator.generateUserGroup();
        group.setUri("Invalid_uri");
        assertThrows(IncorrectRequestException.class,() -> ugs.create(group));
    }
    
    @Test
    void uriIsCreatedWhenMissing() {
        UserGroup group = Generator.generateUserGroup();
        group.setUri(null);
        UserGroup dbGroup = ugs.create(group);
        assertNotNull(dbGroup.getUri());
        assert(UriCreationUtils.isUriValid(dbGroup.getUri()));
    }
    
    @Test
    void throwsExceptionOnAlreadyExistingUserGroup() {
        assertThrows(IncorrectRequestException.class,() -> ugs.create(groupEntity));
    }
    
    @Test
    void getsUsersFromUserGroup() {
        Set<String> users = ugs.getUsers(groupEntity.getUri());
        assertEquals(1, users.size());
        assertEquals(addedUser, users.stream().findFirst().get());
    }
    
    @Test
    void throwsExceptionOnNonExistingUserGroupOnGetUsers() {
        UserGroup newGroup = Generator.generateUserGroup();
        assertThrows(NotFoundException.class, () -> ugs.getUsers(newGroup.getUri()));
    }
    
    @Test
    void userIsAdded() {
        String secondUser = Generator.generateUserURI();
        ugs.addUser(groupEntity.getUri(), secondUser);
        Set<String> users = ugs.getUsers(groupEntity.getUri());
        assertEquals(2, users.size());
        assert(users.contains(secondUser));
        assert(users.contains(addedUser));
    }
    
    @Test
    void throwsExceptionOnNonValidUserUriOnAddUser() {
        String secondUser = "Invalid_uri";
        assertThrows(IncorrectRequestException.class, () -> ugs.addUser(groupEntity.getUri(), secondUser));
    }
    
    @Test
    void throwsExceptionOnNonExistingUserGroupOnAddUser() {
        assertThrows(NotFoundException.class, () -> ugs.addUser(groupEntity.getUri() + "_", addedUser));
    }
    
    @Test
    void userIsRemoved() {
        ugs.removeUser(groupEntity.getUri(), addedUser);
        Set<String> users = ugs.getUsers(groupEntity.getUri());
        assertEquals(0, users.size());
    }
    
    @Test
    void throwsExceptionOnNonExistingUserOnRemoveUser() {
        String secondUser = Generator.generateUserURI();
        assertThrows(NotFoundException.class, () -> ugs.removeUser(groupEntity.getUri(), secondUser));
    }
    
    @Test
    void throwsExceptionOnNonExistingGroupOnRemoveUser() {
        assertThrows(NotFoundException.class, () -> ugs.removeUser(groupEntity.getUri() + "_", addedUser));
    }
    
    @Test
    void groupIsUpdated() {
        String newName = "Kaja";
        groupEntity.setName(newName);
        ugs.update(groupEntity, groupEntity.getUri());
        UserGroup dbGroup = ugs.findByURI(groupEntity.getUri());
        assertEquals(newName, dbGroup.getName());
    }
    
    @Test
    void throwsExceptionOnMissingNameOnUpdate() {
        groupEntity.setName(null);
        assertThrows(IncorrectRequestException.class, () -> ugs.update(groupEntity, groupEntity.getUri()));
        groupEntity.setName("");
        assertThrows(IncorrectRequestException.class, () -> ugs.update(groupEntity, groupEntity.getUri()));
    }
    
    @Test
    void throwsExceptionOnMissingUriUpdate() {
        groupEntity.setUri(null);
        assertThrows(IncorrectRequestException.class, () -> ugs.update(groupEntity, groupEntity.getUri()));
    }
    
    @Test
    void throwsExceptionOnNonMatchingUriOnUpdate() {
        assertThrows(IncorrectRequestException.class, () -> ugs.update(groupEntity, groupEntity.getUri() + "_"));
    }
    
    @Test
    void userGroupIsDeleted() {
        ugs.deleteByURI(groupEntity.getUri());
        assertThrows(NotFoundException.class, () -> ugs.findByURI(groupEntity.getUri()));
    }
    
    @Test
    void throwsExceptionOnNonExistingUserGroupOnRemove() {
        assertThrows(NotFoundException.class, () -> ugs.deleteByURI(groupEntity.getUri() + "_"));
    }
    
}
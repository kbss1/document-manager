package cz.cvut.kbss.documentmanager.service.security;

import cz.cvut.kbss.documentmanager.service.*;
import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.PropertyMockingApplicationContextInitializer;
import cz.cvut.kbss.documentmanager.exception.IncorrectRequestException;
import cz.cvut.kbss.documentmanager.exception.NotFoundException;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.security.AuthenticationToken;
import cz.cvut.kbss.documentmanager.model.security.DocManUserDetails;
import cz.cvut.kbss.documentmanager.model.security.PermissionLevel;
import cz.cvut.kbss.documentmanager.model.security.UserAccount;
import cz.cvut.kbss.documentmanager.model.security.UserPermission;
import cz.cvut.kbss.documentmanager.util.UriCreationUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = PropertyMockingApplicationContextInitializer.class)
public class UserPermissionServiceTest extends BaseServiceTestRunner {

    @Autowired
    private DocumentService ds;
    
    @Autowired
    private UserPermissionService ups;
    
    DocManUserDetails userDetails;
    
    Authentication authentication;
    
    SecurityContext securityContext;
    
    Document doc;
    
    UserPermission perm;
    
    @BeforeEach
    void setUP() {
        UserAccount user = Generator.generateUserAccount();
        userDetails = new DocManUserDetails(user);
        authentication = new AuthenticationToken(userDetails.getAuthorities(), userDetails);
        securityContext = new SecurityContextImpl(authentication);
        SecurityContextHolder.setContext(securityContext);
        
        Document doc = Generator.generateDocument();
        ds.create(doc);
        this.doc = ds.findByURI(doc.getUri());
        perm = this.doc.getUserPermissions().stream().findFirst().get();
    }
    
    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }
    
    @Test
    void permissionLevelIsUpdated() {
        perm.setPermissionLevel(PermissionLevel.WRITE);
        ups.update(perm, perm.getUri());        
        UserPermission retPerm = ups.findByURI(perm.getUri());        
        assertEquals(retPerm.getPermissionLevel(), perm.getPermissionLevel());
        Document retDoc = ds.findByURI(doc.getUri());        
        assertEquals(1,retDoc.getUserPermissions().size());
        UserPermission retDocPerm = retDoc.getUserPermissions().stream().findFirst().get();
        assertEquals(retDocPerm.getPermissionLevel(), perm.getPermissionLevel());
    }
    
    @Test
    void newUserPermissionIsCreatedAndFound() {
        String userURI = Generator.generateUserURI();
        UserPermission newPerm = Generator.generateUserPermission(doc, userURI);
        
        ds.createUserPermission(newPerm, doc.getUri());
        
        UserPermission retPerm = ups.findByURI(newPerm.getUri());
        assertEquals(retPerm.getUri(), newPerm.getUri());
        assertEquals(retPerm.getUserURI(), newPerm.getUserURI());
        assertEquals(retPerm.getPermissionLevel(), newPerm.getPermissionLevel());
    }
    
    @Test
    void throwsExceptionOnUpdateWithMissingUri() {
        perm.setUri(null);
        assertThrows(IncorrectRequestException.class, () -> ups.update(perm, perm.getUri()));
    }
    
    @Test
    void throwsExceptionOnUpdateWithMissingPermissionLevel() {
        perm.setPermissionLevel(null);
        assertThrows(IncorrectRequestException.class, () -> ups.update(perm, perm.getUri()));
    }
    
    @Test
    void throwsExceptionOnUpdateWithdifferentUris() {
        UserPermission perm = doc.getUserPermissions().stream().findFirst().get();
        String oldUri = perm.getUri();
        perm.setUri(UriCreationUtils.generateUserPermissionURI("otherFragment", "differentUserUri"));
        assertThrows(IncorrectRequestException.class, () -> ups.update(perm, oldUri));
    }
    
    @Test
    void userPermissionIsDeleted() {
        ups.deleteByURI(perm.getUri());
        Document retDoc = ds.findByURI(doc.getUri());
        assertNull(retDoc.getUserPermissionWithUri(perm.getUri()));
        assertThrows(NotFoundException.class, () -> ups.findByURI(perm.getUri()));
    }
    
    @Test
    void throwsExceptionOnDeletionWithUnknownUri() {        
        assertThrows(NotFoundException.class, () -> ups.deleteByURI(perm.getUri() + Generator.getRandomString()));
    }
    
    
}
package cz.cvut.kbss.documentmanager.service;

import cz.cvut.kbss.documentmanager.environment.conf.TestFileStorageConfig;
import cz.cvut.kbss.documentmanager.environment.conf.TestPersistenceConfig;
import cz.cvut.kbss.documentmanager.environment.conf.TestServiceConfig;
import cz.cvut.kbss.documentmanager.environment.TransactionalTestRunner;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@ExtendWith(SpringExtension.class)
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableTransactionManagement
@ContextConfiguration(classes = {TestFileStorageConfig.class, TestPersistenceConfig.class, TestServiceConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BaseServiceTestRunner extends TransactionalTestRunner {
}

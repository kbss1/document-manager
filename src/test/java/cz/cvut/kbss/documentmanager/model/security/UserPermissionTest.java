package cz.cvut.kbss.documentmanager.model.security;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.model.Document;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.jupiter.api.Test;

public class UserPermissionTest {
    
    @Test
    void userPermissionIsAddedToDocument() {
        final Document document = Generator.generateDocument();
        final UserPermission perm = Generator.generateUserPermission();
        
        perm.setDocument(document);
        document.addUserPermission(perm);
        
        assertNotNull(document.getUserPermissionWithUri(perm.getUri()));
        assertEquals(document.getUserPermissionWithUri(perm.getUri()), perm);
    }
    
    @Test
    void userPermissionIsDeletedFromDocument() {
        final Document document = Generator.generateDocument();
        final UserPermission perm = Generator.generateUserPermission();
        
        perm.setDocument(document);
        document.addUserPermission(perm);
        
        document.removeUserPermissionByUri(perm.getUri());
        assertNull(document.getUserPermissionWithUri(perm.getUri()));
    }
    
}
package cz.cvut.kbss.documentmanager.model.security;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.model.Document;
import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

public class DocumentPermissionLevelTest {
    
    @Test
    void returnsZeroLevelForUnknownUser() {
        final Document document = Generator.generateDocument();
        final String userURI = Generator.generateUserURI();
        final PermissionLevel expectedLevel = PermissionLevel.NONE;
        assertEquals(expectedLevel, document.getPermissionLevelForUser(userURI));
    }
        
    @Test
    void userPermissionLevelIsUsed() {
        final Document document = Generator.generateDocument();
        final String userURI = Generator.generateUserURI();
        final UserPermission perm = Generator.generateUserPermission(document, userURI);
        final PermissionLevel permLevel = PermissionLevel.WRITE;
        
        perm.setPermissionLevel(permLevel);
        perm.setUserURI(userURI);        
        document.addUserPermission(perm);
        perm.setDocument(document);
        
        assertEquals(permLevel, document.getPermissionLevelForUser(userURI));
    }
    
    @Test
    void groupPermissionLevelIsUsed() {
        final Document document = Generator.generateDocument();
        final UserGroup group = Generator.generateUserGroup();
        final String userURI = Generator.generateUserURI();
        
        final GroupPermission groupPerm = Generator.generateGroupPermission(document, group);
        final PermissionLevel permLevel = PermissionLevel.SECURITY;
        
        groupPerm.setPermissionLevel(permLevel);
        groupPerm.setGroup(group);        
        group.addUser(userURI);
        group.addPermission(groupPerm);        
        document.addGroupPermission(groupPerm);
        
        assertEquals(permLevel, document.getPermissionLevelForUser(userURI));
    }
    
    @Test
    void userPermissionLevelIsUsedOverGroupPermission() {
        final Document document = Generator.generateDocument();        
        final UserGroup group = Generator.generateUserGroup();
        final String userURI = Generator.generateUserURI();
        
        final UserPermission userPerm = Generator.generateUserPermission(document, userURI);
        final GroupPermission groupPerm = Generator.generateGroupPermission(document, group);
        PermissionLevel userPermLevel = PermissionLevel.READ;
        PermissionLevel groupPermLevel = PermissionLevel.SECURITY;
        
        userPerm.setPermissionLevel(userPermLevel);
        userPerm.setUserURI(userURI);
        userPerm.setDocument(document);        
        groupPerm.setPermissionLevel(groupPermLevel);
        groupPerm.setGroup(group);        
        group.addUser(userURI);
        group.addPermission(groupPerm);        
        document.addUserPermission(userPerm);
        document.addGroupPermission(groupPerm);
        
        assertEquals(userPermLevel, document.getPermissionLevelForUser(userURI));
        
        userPermLevel = PermissionLevel.SECURITY;
        groupPermLevel = PermissionLevel.READ;
        userPerm.setPermissionLevel(userPermLevel);
        groupPerm.setPermissionLevel(groupPermLevel);
        
        assertEquals(userPermLevel, document.getPermissionLevelForUser(userURI));        
        document.removeUserPermissionByUri(userPerm.getUri());
        assertEquals(groupPermLevel, document.getPermissionLevelForUser(userURI));
    }
    
    @Test
    void groupPermissionWithHighestLevelIsUsed() {
        final Document document = Generator.generateDocument();
        
        final UserGroup group1 = Generator.generateUserGroup();
        final UserGroup group2 = Generator.generateUserGroup();
        final String userURI = Generator.generateUserURI();
        group1.addUser(userURI);
        group2.addUser(userURI);
        
        final GroupPermission groupPerm1 = Generator.generateGroupPermission(document, group1);
        final GroupPermission groupPerm2 = Generator.generateGroupPermission(document, group2);
        PermissionLevel groupPermLevel1 = PermissionLevel.READ;
        PermissionLevel groupPermLevel2 = PermissionLevel.WRITE;
        groupPerm1.setPermissionLevel(groupPermLevel1);
        groupPerm2.setPermissionLevel(groupPermLevel2);
        
        groupPerm1.setGroup(group1);
        groupPerm2.setGroup(group2);
        group1.addPermission(groupPerm1);
        group2.addPermission(groupPerm2);        
        document.addGroupPermission(groupPerm1);
        document.addGroupPermission(groupPerm2);
        
        assertEquals(groupPermLevel2, document.getPermissionLevelForUser(userURI));
        
        PermissionLevel newGroupPermLevel1 = PermissionLevel.SECURITY;
        groupPerm1.setPermissionLevel(newGroupPermLevel1);
        assertEquals(newGroupPermLevel1, document.getPermissionLevelForUser(userURI));
        
    }
    
}
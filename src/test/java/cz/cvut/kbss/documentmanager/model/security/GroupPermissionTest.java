package cz.cvut.kbss.documentmanager.model.security;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.model.Document;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.jupiter.api.Test;

public class GroupPermissionTest {
    
    @Test
    void groupPermissionIsAddedToDocument() {
        final Document document = Generator.generateDocument();
        final GroupPermission perm = Generator.generateGroupPermission();
        
        document.addGroupPermission(perm);
        perm.setDocument(document);
        
        assertNotNull(document.getGroupPermissionWithUri(perm.getUri()));
        assertEquals(document.getGroupPermissionWithUri(perm.getUri()), perm);
    }
    
    @Test
    void groupPermissionIsAddedToGroup() {
        final GroupPermission perm = Generator.generateGroupPermission();
        final UserGroup group = Generator.generateUserGroup();
        
        perm.setGroup(group);
        group.addPermission(perm);
        
        assertNotNull(group.getGroupPermissionWithUri(perm.getUri()));
        assertEquals(group.getGroupPermissionWithUri(perm.getUri()), perm);
    }
    
    @Test
    void groupPermissionIsDeletedFromDocument() {
        final Document document = Generator.generateDocument();
        final GroupPermission perm = Generator.generateGroupPermission();
        
        perm.setDocument(document);
        document.addGroupPermission(perm);
        
        document.removeGroupPermissionByUri(perm.getUri());
        assertNull(document.getGroupPermissionWithUri(perm.getUri()));
    }
    
    @Test
    void groupPermissionIsDeletedUserGroup() {
        final GroupPermission perm = Generator.generateGroupPermission();
        final UserGroup group = Generator.generateUserGroup();
        
        perm.setGroup(group);
        group.addPermission(perm);
        
        group.removePermissionByUri(perm.getUri());
        assertNull(group.getGroupPermissionWithUri(perm.getUri()));
    }
    
}
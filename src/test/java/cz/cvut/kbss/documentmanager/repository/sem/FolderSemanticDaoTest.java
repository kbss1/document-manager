package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.model.Folder;
import cz.cvut.kbss.documentmanager.repository.DocumentDao;
import cz.cvut.kbss.documentmanager.repository.FolderDao;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class FolderSemanticDaoTest extends BaseDaoTestRunner {

    @Autowired
    private DocumentDao documentDao;
    
    @Autowired
    private FolderDao folderDao;
    
    @Test
    public void findByIdFindsInstanceWithSpecifiedKey() {
        final Folder folder = Generator.generateFolder();
        executeInTransaction(() -> folderDao.save(folder));
        

        final Folder result = folderDao.findById(folder.getUri()).get();
        assertNotNull(result);
        assertEquals(folder.getUri(), result.getUri());
    }
    
    @Test
    public void findByIdReturnsNullForUnknownKey() {
        final Optional<Folder> res = folderDao.findById("https://test.cz/Folder/BUILD_TEST_DOC_" + Long.toString(System.currentTimeMillis(), 36));
        assert(!res.isPresent());
    }
    
    @Test
    public void FolderIsDeleted() {
        final Folder folder = Generator.generateFolder();
        executeInTransaction(() -> folderDao.save(folder));
        executeInTransaction(() -> folderDao.deleteById(folder.getUri()));

        final Optional<Folder> result = folderDao.findById(folder.getUri());
        assert(!result.isPresent());
    }
    
    @Test
    public void SubfolderPersistenceContextTest() {
        final Document document = Generator.generateDocument();
        final Folder subFolder = Generator.generateFolder();
        executeInTransaction(() -> documentDao.save(document));
        document.getRootFolder().addSubfolder(subFolder);
        subFolder.setParentFolder(document.getRootFolder());
        executeInTransaction(() -> folderDao.save(subFolder));
        
        document.getRootFolder().removeSubfolder(subFolder);
        executeInTransaction(() -> folderDao.deleteById(subFolder.getUri()));
        
        document.getRootFolder().addSubfolder(subFolder);
        executeInTransaction(() -> folderDao.save(subFolder));
    }
    
    @Test
    public void SubfolderCorrectlyPersisted() {
        final Document document = Generator.generateDocument();
        final Folder subFolder = Generator.generateFolder();
        subFolder.setParentFolder(document.getRootFolder());
        document.getRootFolder().addSubfolder(subFolder);
        executeInTransaction(() -> documentDao.save(document));
        
        final Folder dbFolder = folderDao.findById(document.getRootFolder().getUri()).get();
        Set<Folder> dbFolderSubfolders = dbFolder.getFolders();        
        assertEquals(dbFolderSubfolders.size(), 1);
        Iterator<Folder> it = dbFolderSubfolders.iterator();
        Folder dbSubFolder = it.next();
        assertEquals(dbSubFolder.getUri(), subFolder.getUri());
        assertEquals(dbSubFolder.getParentFolder().getUri(), document.getRootFolder().getUri());
    }

}

package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.model.Document;
import cz.cvut.kbss.documentmanager.repository.DocumentDao;
import java.util.Optional;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class DocumentSemanticDaoTest extends BaseDaoTestRunner {

    @Autowired
    private DocumentDao documentDao;
    
    @Test
    public void findByIdFindsInstanceWithSpecifiedKey() {
        final Document document = Generator.generateDocument();
        executeInTransaction(() -> documentDao.save(document));

        Document result = documentDao.findById(document.getUri()).get();
        assertNotNull(result);
        assertEquals(document.getUri(), result.getUri());
    }
    
    @Test
    public void findByIdReturnsNullForUnknownKey() {
        final Optional<Document> res = documentDao.findById("https://test.cz/Document/TEST_DOC_" + Long.toString(System.currentTimeMillis(), 36));
        assert(!res.isPresent());
    }

    @Test
    public void folderIsPersistedWithDocument() {
        final Document document = Generator.generateDocument();
        executeInTransaction(() -> documentDao.save(document));

        final Document result = documentDao.findById(document.getUri()).get();
        assertNotNull(result.getRootFolder());
        assertEquals(result.getRootFolder().isRoot(), true);
    }
    
    @Test
    public void documentIsDeleted() {
        final Document document = Generator.generateDocument();
        executeInTransaction(() -> documentDao.save(document));
        executeInTransaction(() -> documentDao.deleteById(document.getUri()));

        final Optional<Document> result = documentDao.findById(document.getUri());
        assert(!result.isPresent());
    }

}

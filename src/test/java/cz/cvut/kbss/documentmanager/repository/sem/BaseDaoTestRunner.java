package cz.cvut.kbss.documentmanager.repository.sem;

import cz.cvut.kbss.documentmanager.environment.conf.TestFileStorageConfig;
import cz.cvut.kbss.documentmanager.environment.conf.TestPersistenceConfig;
import cz.cvut.kbss.documentmanager.environment.TestUtils;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestPersistenceConfig.class, TestFileStorageConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles(profiles = "sem")
public abstract class BaseDaoTestRunner {

    @Autowired
    private PlatformTransactionManager txManager;
    
    public void executeInTransaction(Runnable procedure) {
        TestUtils.executeInTransaction(txManager, procedure);
    }
}

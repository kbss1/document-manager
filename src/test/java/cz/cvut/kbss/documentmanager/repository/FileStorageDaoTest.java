package cz.cvut.kbss.documentmanager.repository;

import cz.cvut.kbss.documentmanager.environment.Generator;
import cz.cvut.kbss.documentmanager.environment.conf.TestFileStorageConfig;
import cz.cvut.kbss.documentmanager.exception.FileStorageException;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.io.IOUtils;
import static org.junit.Assert.assertEquals;
import org.springframework.core.io.Resource;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FileStorageDaoImpl.class, TestFileStorageConfig.class})
public class FileStorageDaoTest {

    @Autowired
    private FileStorageDao fsd;
    
    @Test
    public void fileIsLoaded() throws IOException {
        String fileName = "textfile.txt";
        String content = "File content";
        MockMultipartFile file = new MockMultipartFile(fileName, content.getBytes());        
        Path path = Paths.get("Path_" + Generator.getRandomString() + "/" + fileName);
        fsd.storeFileInPath(file, path);
        Resource fileBack = fsd.loadFileAsResource(path);
        assertEquals(fileName, fileBack.getFilename());
        StringWriter writer = new StringWriter();
        IOUtils.copy(fileBack.getInputStream(), writer, "UTF-8");
        assertEquals(content, writer.toString());
    }
    
    @Test
    public void fileIsFound() {
        String fileName = "textfile.txt";
        String content = "File content";
        MockMultipartFile file = new MockMultipartFile(fileName, content.getBytes());        
        String folderPath = "Path_" + Generator.getRandomString();
        Path path = Paths.get(folderPath + "/" + fileName);
        
        fsd.storeFileInPath(file, path);
        assert(fsd.fileFolderExists(path));
        assert(fsd.getStorageFileFolders().stream().filter(gotPath -> gotPath.equals(folderPath)).findAny().isPresent());
    }
    
    @Test
    public void fileIsDeleted() {
        String fileName = "textfile.txt";
        String content = "File content";
        MockMultipartFile file = new MockMultipartFile(fileName, content.getBytes());        
        String folderPath = "Path_" + Generator.getRandomString();
        Path path = Paths.get(folderPath + "/" + fileName);        
        fsd.storeFileInPath(file, path);
        fsd.deleteFileByFolderPath(Paths.get(folderPath));
        assert(!fsd.fileFolderExists(path));
        assertThrows(FileStorageException.class, () -> fsd.loadFileAsResource(path));
        assertThrows(FileStorageException.class, () -> fsd.deleteFileByFolderPath(Paths.get(folderPath)));
    }
    
}